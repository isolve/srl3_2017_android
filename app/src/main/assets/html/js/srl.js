/**
 * srl.js
 * (C) 2014 Isolve AB
 */
var elementIDContainingNode = function(node) {
	while (node != null) {
    	if (node.nodeType == 1) {
    		id = node.getAttribute('id');
    		if (id != null) {
    			if (id.match(/^e[0-9]{1,6}/)) {
    				return id;
    			}
    		}
    	}
    	node = node.parentNode;
	}
	return null;
}

var serializeMarksInElementWithID = function(ID) {
    var e = $('#'+ID);
	var m = e.find('.mark');
    var a = '';
    if (m.length > 0) {
        var r = rangy.createRange();
        m.each(function(i) {
            r.selectNode(this);
            var bm = r.getBookmark(e[0]);
               a += (i>0?'|':'') + bm.start + ':' + bm.end;
        });
    }
    return a;
}

var deserializeMarksInElementWithID = function(ID, s) {
    var e = $('#'+ID);
    if (e.length == 1) {
        var r = rangy.createRange();
        r.selectNodeContents(e[0]);
        var bm = r.getBookmark(e[0]);
        a = s.split('|');
        var l = a.length;
        for (var i = 0; i < l; i++) {
            o = a[i].split(':');
            bm.start = parseInt(o[0]);
            bm.end = parseInt(o[1]);
            r.moveToBookmark(bm);
            cssApplier.applyToRange(r);
        }
    }
}

var getElementMarkserialArrayStringFromSelection = function() {
	var sel = document.getSelection();
	var eA = elementIDContainingNode(sel.anchorNode);
	var eF = elementIDContainingNode(sel.focusNode);
	var idA = parseInt(eA.substring(1));
	var idF = parseInt(eF.substring(1));
	var i = Math.min(idA,idF);
	var m = Math.max(idA,idF);
	var result = "";
	cssApplier.toggleSelection();
	sel.empty();
	var count = 0;
	if (i > 0 && m > 0) {
		for (;i<=m && count<100;i++) {
			var serial = serializeMarksInElementWithID("e" + i);
			result+=(count>0?'/':'') + i + '#' + serial;
			count++;
		}
	}
	return result;
}
