package utils;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import se.isolve.srl3.R;

public class NotesDialogFragment extends DialogFragment {
	/* The activity that creates an instance of this dialog fragment must
	     * implement this interface in order to receive event callbacks.
	     * Each method passes the DialogFragment in case the host needs to query it. */
		Button mButton;  
		EditText mEditText;  
		public NotesDialogListener mListener;
		NotesDialogFragment mDialogFragment;
		private String mText = null, type;
		private boolean isSingleLine = false;
	
	    public interface NotesDialogListener {
	        public void onNotesReadyClick(String arg);
	    }

		/* PWA: 2016-08-19: Changed to default empty constructor
	    public NotesDialogFragment(String text, String typeOfDialogue, boolean singleLine) {
	    	mText = text;
	    	type = typeOfDialogue;
	    	isSingleLine = singleLine;
	    }*/

		public NotesDialogFragment() {}

		public void SetNotesDialogFragmentArgs(String text, String typeOfDialogue, boolean singleLine) {
			mText = text;
			type = typeOfDialogue;
			isSingleLine = singleLine;
		}
	    // Use this instance of the interface to deliver action events
	    
	    
	    /*// Override the Fragment.onAttach() method to instantiate the NoticeDialogListener
	    @Override
	    public void onAttach(Activity activity) {
	        super.onAttach(activity);
	        // Verify that the host activity implements the callback interface
	        try {
	            // Instantiate the NoticeDialogListener so we can send events to the host
	            mListener = (NotesDialogListener) activity;
	        } catch (ClassCastException e) {
	            // The activity doesn't implement the interface, throw exception
	            throw new ClassCastException(activity.toString()
	                    + " must implement NotesDialogListener");
	        }
	    }*/
	    
	    @Override
	    public Dialog onCreateDialog(Bundle savedInstanceState) {
	    	final Dialog dialog = new Dialog(getActivity());  
	    	  dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);  
	    	  dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,  
	    	    WindowManager.LayoutParams.FLAG_FULLSCREEN);  
	    	  dialog.setContentView(R.layout.notes_dialog_fragment);  
	    	  //dialog.getWindow().setBackgroundDrawable(  
	    	  //  new ColorDrawable(Color.TRANSPARENT));  
	    	  dialog.show();  
	    	  mButton = (Button) dialog.findViewById(R.id.button1);  
	    	  mEditText = (EditText) dialog.findViewById(R.id.notes_dialog_edit_text);  
	    	  if (type != null) { 
	    		  mButton.setText(type);
	    	  }
	    	  if (mText != null) {
	    		  mEditText.setText(mText);
	    	  }
	    	  
	    	  mEditText.setSingleLine(isSingleLine);

				//HERE

				Handler go = new Handler();
				go.postDelayed(new Runnable() {
				@Override
				public void run() {
					try {
						mEditText.requestFocus();
						InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
						imm.showSoftInput(mEditText, InputMethodManager.SHOW_IMPLICIT);
					} catch (Exception e) {
						Log.d("GATillActivity", "Unable to request keyboard focus");
					}
				}
				}, 100);

	    	  
	    	  mButton.setOnClickListener(new OnClickListener() {  
	    	  
	    	   @Override  
	    	   public void onClick(View v) {  
	    	    mListener.onNotesReadyClick(mEditText.getText().toString());  
	    	    dismiss();  
	    	   }  
	    	  });  
	    	  return dialog;  
	    }
	    
	    public void showDialog(){
	        android.app.FragmentTransaction ft = getFragmentManager().beginTransaction();
	        /*Fragment f = getSupportFragmentManager().findFragmentByTag("dialog");

	        if(f != null){
	            ft.remove(f);
	        }*/

			// PWA: 2016-08-19: Changed to use default empty contructor
			mDialogFragment = new NotesDialogFragment();
			mDialogFragment.SetNotesDialogFragmentArgs(mText, type, isSingleLine);

	        mDialogFragment.setCancelable(false);
	        mDialogFragment.show(ft, "dialog");
	        //dialogfragment.setStyle(DialogFragment.STYLE_NO_TITLE, android.R.style.Theme_Holo_Light_Dialog);

	    }
	    
}
