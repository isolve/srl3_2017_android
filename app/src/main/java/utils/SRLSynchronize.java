package utils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import se.isolve.srl3.SRL3ApplicationClass;
import se.isolve.srl3.database.BookDB;
import se.isolve.srl3.reader.BookmarkHistoryItem;
import se.isolve.srl3.reader.BookmarkHistoryNotesManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

public class SRLSynchronize {
	
	public BookmarkHistoryNotesManager bookmarkHistory;
	
	private String username;
	private String password;
	private String yearCode;
	private String udid;
	private String loginUrlString;
//	NSURLSessionConfiguration *syncSessionConf;
//	NSURLSession *syncSession;
	JSONArray notesToSyncArray;
	JSONArray notesAndBookmarksToSyncArray;
//	AppDelegate *delegate;
	private String timestampName;
	private boolean setSyncForSettings, isSyncing = false;
	private long syncTimestamp;
	
	private static SRLSynchronize instance;
	
	private SRLNotesSyncListener syncListener;
	
	public interface SRLNotesSyncListener {
		public void syncDone();
	}
	
	//Singleton = private constructor
    private SRLSynchronize(Context c) {
        load(c);
    }

    public static void initSyncSession (Context c) {
        if (instance == null) {
            Log.d("SRLSynchronize", "+++++++++++Singleton initialized+++++++++++++++");
            instance = new SRLSynchronize(c);
        }
    }

    public static SRLSynchronize getInstance() {
        return instance;
    }
    
    public static void destroyInstance () {
    	instance = null;
    }
    
    private void load (Context context) {
    	SharedPreferences settings = context.getSharedPreferences("LOGIN",Context.MODE_PRIVATE);
    	
    	bookmarkHistory = BookmarkHistoryNotesManager.getInstance();
    	username = settings.getString("username", null);
    	password = settings.getString("password", null);
    	udid = settings.getString("udid", null);
    	yearCode = "18";

    	String usernameEn = username;
    	String udidEn = udid;
    	
    	try {
    		usernameEn = URLEncoder.encode(usernameEn,"UTF-8");
    		udidEn = URLEncoder.encode(udidEn,"UTF-8");
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}

		// PWA: 2017-12-19: Updated service and year
		loginUrlString = "https://isolve-srl3-service.appspot.com/post_notes?username="+usernameEn+"&year="+yearCode+"&udid="+udidEn+"&timestamp="+syncTimestamp;

    	timestampName = username + "_timestamp";
    }

	// PWA: 2017-01-15: Updated version to handle Bookmarks as well as Notes

	private JSONArray getNotesAndBookmarks () {
		JSONArray resArr = new JSONArray ();
		ArrayList<BookmarkHistoryItem> notesArr = bookmarkHistory.getNotes();
		for (BookmarkHistoryItem item : notesArr) {
			if (item.getNotSynced()) {
				JSONObject obj = new JSONObject();
				try {
					obj.put("id", item.ID);
					obj.put("timestamp", SRLUtils.timeIntervalSince1970ForDateString(item.getDate()));
					obj.put("text", item.text);
				} catch (JSONException e1) {
					e1.printStackTrace();
				} catch (ParseException e) {
					e.printStackTrace();
				}
				resArr.put(obj);
			}
		}
		ArrayList<BookmarkHistoryItem> bookmarksArr = bookmarkHistory.getBookmarks();
		for (BookmarkHistoryItem item : bookmarksArr) {
			if (item.getNotSynced()) {
				JSONObject obj = new JSONObject();
				try {
					obj.put("id", item.ID + 1000000); // PWA: Bookmarks are marked with ID + 1000000
					obj.put("timestamp", SRLUtils.timeIntervalSince1970ForDateString(item.getDate()));
					obj.put("text", item.text);
				} catch (JSONException e1) {
					e1.printStackTrace();
				} catch (ParseException e) {
					e.printStackTrace();
				}
				resArr.put(obj);
			}
		}
		return resArr;
	}

	public void sendNewNotesAndBookmarks(final Context context) {
		if (!sessionIsInitialized()) return;
		if	(isSyncing) return;
		notesAndBookmarksToSyncArray = getNotesAndBookmarks();
		if (notesAndBookmarksToSyncArray.length() > 0) {
			isSyncing = true;
			AsyncHttpClient client = new AsyncHttpClient();
			String notesStr = null;
			try {
				notesStr = URLEncoder.encode(notesAndBookmarksToSyncArray.toString(), "UTF-8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			if (notesStr != null) {
				client.addHeader("Content-Length", Integer.toString(("notes="+notesStr).getBytes().length));
				try {
					client.post(context, loginUrlString, new StringEntity("notes="+notesStr.toString()), "application/x-www-form-urlencoded", new AsyncHttpResponseHandler () {
						@Override
						public void onFailure (int arg0, Header[] arg1, byte[] arg2, Throwable arg3) {
							isSyncing = false;
							Log.d("SRLSynchronize", arg3.getMessage());
						}

						@Override
						public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
							String response = null;
							String responseCode = null;
							isSyncing = false;
							try {
								response = new String(arg2, "UTF-8");
								responseCode = Character.toString(response.charAt(0)) + Character.toString(response.charAt(1));
							} catch (UnsupportedEncodingException e) {
								e.printStackTrace();
							}
							if (response.startsWith("1")) {
								ArrayList<BookmarkHistoryItem> syncedNotes = new ArrayList<BookmarkHistoryItem>();
								ArrayList<BookmarkHistoryItem> syncedBookmarks = new ArrayList<BookmarkHistoryItem>();
								try {
									for (int i = 0; i < notesAndBookmarksToSyncArray.length(); i++) {
										JSONObject obj = notesAndBookmarksToSyncArray.getJSONObject(i);
										BookmarkHistoryItem item = BookmarkHistoryItem.getItem(obj.getInt("id"), 0, SRLUtils.getCurrentTimeStamp(), null, obj.getString("text"), false);
										if (item.ID > 1000000) {
											item.ID -= 1000000;
											syncedBookmarks.add(item);
										} else {
											syncedNotes.add(item);
										}
									}
								} catch (JSONException e) {
									e.printStackTrace();
								}
								bookmarkHistory.setNotesToSynced(syncedNotes);
								bookmarkHistory.setBookmarksToSynced(syncedBookmarks);
								getNewNotesAndBookmarks(context);
							} else if (response.startsWith("-3")) {
								Log.e("SRLSynchronize", "Problem med sync tjänsten.");
							}
						}
					});
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
			}
		} else {
			Log.d("Sync", "Getting new notes because there are no local ones.");
			getNewNotesAndBookmarks(context);
		}
	}

	public void getNewNotesAndBookmarks (final Context context) {
		if (!sessionIsInitialized()) return;
		String usernameEn = username;
		String udidEn = udid;
		String passwordEn = password;
		try {
			usernameEn = URLEncoder.encode(usernameEn,"UTF-8");
			udidEn = URLEncoder.encode(udidEn,"UTF-8");
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}

		String getUrlString = "https://isolve-srl3-service.appspot.com/get_notes?username="+usernameEn+"&year="+yearCode+"&udid="+udidEn+"&timestamp="+getSyncTimestamp(context);
		Log.d("Sync", "getNewNotesAndBookmarks:"+getUrlString);
		AsyncHttpClient client = new AsyncHttpClient();
		client.get(getUrlString, new AsyncHttpResponseHandler () {
			@Override
			public void onFailure (int arg0, Header[] arg1, byte[] arg2, Throwable arg3) {
				// Toast.makeText(context, arg3.getMessage(), Toast.LENGTH_LONG).show();
			}

			@Override
			public void onSuccess (int arg0, Header[] arg1, byte[] arg2) {
				String response = null;
				String responseCode = null;
				try {
					response = new String(arg2, "UTF-8");
					responseCode = Character.toString(response.charAt(0)) + Character.toString(response.charAt(1));
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
				JSONArray arr = null;
				try {
					Log.d("Sync", "got response - "+response);

					arr = new JSONArray(response);
					for (int i = 0; i < arr.length(); i++) {
						JSONObject obj = arr.getJSONObject(i);
						BookmarkHistoryItem item = BookmarkHistoryItem.getItem(obj.getInt("id"), 0, SRLUtils.getCurrentTimeStamp(), null, obj.getString("text"), false);
						if (item.ID > 1000000) {
							item.ID -= 1000000;
							bookmarkHistory.addBookmarkToArray(item, false);
						} else {
							bookmarkHistory.addNoteToArray(item, false);
						}
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
				bookmarkHistory.writeToFile(context);
				setSyncTimestamp(context);
				if (syncListener != null) {
					syncListener.syncDone();
				}
			}
		});
	}

	public void get2017NotesAndBookmarks (final Context context) {
		if (!sessionIsInitialized()) return;
		String usernameEn = username;
		String udidEn = udid;
		String passwordEn = password;
		String yearCode172 = "17-2";
		try {
			usernameEn = URLEncoder.encode(usernameEn,"UTF-8");
			udidEn = URLEncoder.encode(udidEn,"UTF-8");
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}
		String getUrlString = "https://isolve-srl3-service.appspot.com/get_notes?username="+usernameEn+"&year="+yearCode172+"&udid="+udidEn+"&timestamp=0";
		Log.d("Sync", "get2017NotesAndBookmarks:"+getUrlString);
		AsyncHttpClient client = new AsyncHttpClient();
		client.get(getUrlString, new AsyncHttpResponseHandler () {
			@Override
			public void onFailure (int arg0, Header[] arg1, byte[] arg2, Throwable arg3) {
				// Toast.makeText(context, arg3.getMessage(), Toast.LENGTH_LONG).show();
			}

			@Override
			public void onSuccess (int arg0, Header[] arg1, byte[] arg2) {
				String response = null;
				String responseCode = null;
				try {
					response = new String(arg2, "UTF-8");
					responseCode = Character.toString(response.charAt(0)) + Character.toString(response.charAt(1));
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
				JSONArray arr = null;
				try {
					Log.d("Sync", "got response - "+response);
					BookDB bookDB = new BookDB(SRL3ApplicationClass.getContext());
					bookDB.createDatabase();
					arr = new JSONArray(response);
					for (int i = 0; i < arr.length(); i++) {
						JSONObject obj = arr.getJSONObject(i);
						BookmarkHistoryItem item = BookmarkHistoryItem.getItem(obj.getInt("id"), 0, SRLUtils.getCurrentTimeStamp(), null, obj.getString("text"), false);
						if (item.ID > 1000000) {
							item.ID -= 1000000;
							int newId = bookDB.idForOldLawbookId(item.getID());
							if (newId > 0) {
								item.setID(newId);
								bookmarkHistory.addBookmarkToArray(item, false);
							}
							bookmarkHistory.addBookmarkToArray(item, false);
						} else {
							int newId = bookDB.idForOldLawbookId(item.getID());
							if (newId > 0) {
								item.setID(newId);
								bookmarkHistory.addNoteToArray(item, false);
							}
						}
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
				bookmarkHistory.writeToFile(context);
				setSyncTimestamp(context);
				if (syncListener != null) {
					syncListener.syncDone();
				}
			}
		});
	}

	// END: Updated version to handle Bookmarks as well as Notes

	/*
	private JSONArray getNotes () {
		ArrayList<BookmarkHistoryItem> notesArr = bookmarkHistory.getNotes();
		JSONArray resArr = new JSONArray ();
		
		for (BookmarkHistoryItem item : notesArr) {
			if (item.getNotSynced()) {
				JSONObject obj = new JSONObject();
				try {
					obj.put("id", item.ID);
					obj.put("timestamp", SRLUtils.timeIntervalSince1970ForDateString(item.getDate()));
					obj.put("text", item.text);
				} catch (JSONException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				resArr.put(obj);
			}
		}
		
		return resArr;
	}
    
    public void sendNewNotes (final Context context) {
    	if (!sessionIsInitialized()) return;
    	if	(isSyncing) return;
    	//        if ([sender isKindOfClass:[SRLSettingsController class]]) {
    	//            self.setSyncForSettings = YES;
    	//            self.syncController = (SRLSettingsController *)sender;
    	//        }
    	//        else {
    	//            self.setSyncForSettings = NO;
    	//        }

    	notesToSyncArray = getNotes();

    	if (notesToSyncArray.length() > 0) {
    		isSyncing = true;
    		AsyncHttpClient client = new AsyncHttpClient();
    		String notesStr = null;
    		try {
    			notesStr = URLEncoder.encode(notesToSyncArray.toString(), "UTF-8");
    		} catch (UnsupportedEncodingException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}

    		if (notesStr != null) {
    			//client.addHeader("Content-Type", "application/x-www-form-urlencoded");                                  
    			// client.addHeader("Content-Length", Integer.toString(("notes="+notesToSyncArray.toString()).getBytes().length));
    			client.addHeader("Content-Length", Integer.toString(("notes="+notesStr).getBytes().length));

    			try {
    				//client.post(context, loginUrlString, new StringEntity("notes="+notesToSyncArray.toString()), "application/x-www-form-urlencoded", new AsyncHttpResponseHandler () {
    				client.post(context, loginUrlString, new StringEntity("notes="+notesStr.toString()), "application/x-www-form-urlencoded", new AsyncHttpResponseHandler () {

						@Override
    					public void onFailure (int arg0, Header[] arg1, byte[] arg2,
    							Throwable arg3) {
    						isSyncing = false;
    						Log.d("SRLSynchronize", arg3.getMessage());
    						// Toast.makeText(context, arg3.getMessage(), Toast.LENGTH_LONG).show();
    					}

    					@Override
    					public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
    						// TODO Auto-generated method stub
    						String response = null;
    						String responseCode = null;
    						isSyncing = false;
    						try {
    							response = new String(arg2, "UTF-8");
    							responseCode = Character.toString(response.charAt(0)) + Character.toString(response.charAt(1));
    						} catch (UnsupportedEncodingException e) {
    							// TODO Auto-generated catch block
    							e.printStackTrace();
    						}

    						// Toast.makeText(context, "Note post response" + response, Toast.LENGTH_LONG).show();

    						if (response.startsWith("1")) {
    							ArrayList<BookmarkHistoryItem> syncedItems = new ArrayList<BookmarkHistoryItem>();
    							try {
    								for (int i = 0; i < notesToSyncArray.length(); i++) {
    									JSONObject obj = notesToSyncArray.getJSONObject(i);
    									BookmarkHistoryItem item = BookmarkHistoryItem.getItem(obj.getInt("id"),
    											0,
    											SRLUtils.getCurrentTimeStamp(), 
    											null, 
    											obj.getString("text"),
    											false);
    									syncedItems.add(item);
    								}
    							} catch (JSONException e) {
    								// TODO Auto-generated catch block
    								e.printStackTrace();
    							}

    							bookmarkHistory.setNotesToSynced(syncedItems);

    							for (BookmarkHistoryItem item: syncedItems) {
    								if (item.text.length() < 1 || (item.text == null)) {
    									bookmarkHistory.deleteNote(item);
    								}
    							}

    							// Get new notes after sync.
    							getNewNotes(context);

    						} else if (response.startsWith("-3")) {
    							Log.e("SRLSynchronize", "Problem med sync tjänsten.");
    						}
    					}

    				});
    			} catch (UnsupportedEncodingException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}
    		}
    	} else {
    		Log.d("Sync", "Getting new notes because there are no local ones.");
    		getNewNotes(context);
    	}
    }

    
    public void getNewNotes (final Context context) {
        if (!sessionIsInitialized()) return;
        
        String usernameEn = username;
    	String udidEn = udid;
    	String passwordEn = password;
        
        try {
        	usernameEn = URLEncoder.encode(usernameEn,"UTF-8");
        	udidEn = URLEncoder.encode(udidEn,"UTF-8");
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}

		// PWA: 2015-12-15: Updated service and year
        String getUrlString = "https://isolve-srl3-service.appspot.com/get_notes?username="+usernameEn+"&year="+yearCode+"&udid="+udidEn+"&timestamp="+getSyncTimestamp(context);
        
        Log.d("Sync", "getting notes for url - "+getUrlString);
        
        AsyncHttpClient client = new AsyncHttpClient();
        
        client.get(getUrlString, new AsyncHttpResponseHandler () {

			@Override
			public void onFailure (int arg0, Header[] arg1, byte[] arg2,
					Throwable arg3) {
				Toast.makeText(context, arg3.getMessage(), Toast.LENGTH_LONG).show();
			}
			
			@Override
			public void onSuccess (int arg0, Header[] arg1, byte[] arg2) {
				// TODO Auto-generated method stub
				String response = null;
				String responseCode = null;
				try {
					response = new String(arg2, "UTF-8");
					responseCode = Character.toString(response.charAt(0)) + Character.toString(response.charAt(1));
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				JSONArray arr = null;
				try {
					
					Log.d("Sync", "got response - "+response);
					
					arr = new JSONArray(response);
					for (int i = 0; i < arr.length(); i++) {
						JSONObject obj = arr.getJSONObject(i);
						BookmarkHistoryItem item = BookmarkHistoryItem.getItem(obj.getInt("id"),
													0,
													SRLUtils.getCurrentTimeStamp(), 
													null, 
													obj.getString("text"),
													false);
						bookmarkHistory.addNoteToArray(item, false);
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
				bookmarkHistory.writeToFile(context);
				setSyncTimestamp(context);
				
				if (syncListener != null) {
					syncListener.syncDone();
				}
				
				if (setSyncForSettings) {
					//
				}
			}
        });
    }
	*/

    public void setSRLNotesSyncListener (SRLNotesSyncListener listener) {
    	this.syncListener = listener;
    }
    
    public SRLNotesSyncListener getSyncListener() {
    	return this.syncListener;
    }
    
    public void removeSRLNotesSyncListener () {
    	this.syncListener = null;
    }
    
    public boolean sessionIsInitialized () {
    	if (username == null || udid == null) {
    		return false;
    	}
        return (username.length() > 0 && udid.length() > 0);
    }
    
    public long getSyncTimestamp (Context context) {
    	SharedPreferences settings = context.getSharedPreferences("LOGIN",Context.MODE_PRIVATE);
    	long timestamp = settings.getLong(timestampName, 0);
        return timestamp;
    }

    public void setSyncTimestamp (Context context) {
    	SharedPreferences settings = context.getSharedPreferences("LOGIN",0);
    	Editor editor = settings.edit();
		editor.putLong(timestampName, SRLUtils.timeIntervalSince1970());
		editor.commit();
    }
    
    public static void getLatestSyncedNotes (Context c) {
    	if (SRLUtils.isUserLoggedIn(c)) {
    		SRLSynchronize.initSyncSession(c);
        	SRLSynchronize.getInstance().getNewNotesAndBookmarks(c);
    	}
    }
    
    public static void sendLatestNotes (Context c) {
    	if (SRLUtils.isUserLoggedIn(c)) {
    		SRLSynchronize.getInstance().sendNewNotesAndBookmarks(c);
        	if (BookmarkHistoryNotesManager.getInstance() != null) {
        		BookmarkHistoryNotesManager.getInstance().writeToFile(c);
        	}
    	}
    }
    
    public void checkSyncTimestamp(Context c) {
    	long timestampNow = System.currentTimeMillis()/1000;
    	if	(timestampNow - getSyncTimestamp(c) > 10000) {
    		sendNewNotesAndBookmarks(c);
    	}
    	
    }
    
}
