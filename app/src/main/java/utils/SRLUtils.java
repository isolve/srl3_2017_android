package utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.util.Log;

public class SRLUtils {

	public static boolean isTablet(Context context) {
	    return (context.getResources().getConfiguration().screenLayout
	            & Configuration.SCREENLAYOUT_SIZE_MASK)
	            >= Configuration.SCREENLAYOUT_SIZE_LARGE;
	}
	
	/**
	 * 
	 * @return yyyy-MM-dd HH:mm:ss formate date as string
	 */
	public static String getDateStringFromTimeStamp(long timeStamp){
	    try {
	    	timeStamp = (timeStamp*1000);
	        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
	        Date date = new Date();
	        if (timeStamp >= 0) {
	        	Log.d("SRLUtils", "Generating date from ts - "+timeStamp);
	        	date = new Date(timeStamp);
	        }
	        String dateStr = dateFormat.format(date); // Find todays date

	        return dateStr;
	    } catch (Exception e) {
	        e.printStackTrace();

	        return null;
	    }
	}
	
	public static String getCurrentTimeStamp(){
	    return SRLUtils.getDateStringFromTimeStamp(-1);
	}
	
	public static long timeIntervalSince1970ForDateString (String dateStr) throws ParseException {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
		if (dateStr != null) {
			Date date = (Date)formatter.parse(dateStr);
			return date.getTime()/1000;
		}
		return 0;
	}
	
	public static long timeIntervalSince1970 () {
		// Number of seconds passed since 1970.
		return System.currentTimeMillis()/1000;
	}
	
	public static boolean isUserLoggedIn(Context c) {
		SharedPreferences settings = c.getSharedPreferences("LOGIN",Context.MODE_PRIVATE);
		String loggedInUser = settings.getString("username", null);
		String loggedInPass = settings.getString("password", null);
		String loggedInUdid = settings.getString("udid", null);
		if (loggedInUser != null && loggedInPass != null && loggedInUdid != null) {
			return true;
		}
		else {
			return false;
		}
	}
}
