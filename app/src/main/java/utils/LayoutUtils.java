package utils;

import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;

public class LayoutUtils {
	public static void setHeight (View view, int height) {
		ViewGroup.LayoutParams params = view.getLayoutParams();
		params.height = height;
		view.requestLayout();
	}
	
	public static void setLayout (View view, int x, int y, int height, int width) {
		RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) view.getLayoutParams();
		
		if (lp == null) {
			lp =new RelativeLayout.LayoutParams
	                ((int)LayoutParams.WRAP_CONTENT,(int)LayoutParams.WRAP_CONTENT);
		}
		
		if (x > -1) {
			lp.leftMargin = x;
		} else {
			lp.leftMargin = view.getLeft();
		}
		
		if (y > -1) {
			lp.topMargin = y;
		} else {
			lp.topMargin = view.getTop();
		}
		
		if (height > -1) {
			lp.height = height;
		} else {
			lp.height = view.getHeight();
		}

		if (width > -1) {
			lp.width = width;
		} else {
			lp.height = view.getHeight();
		}
		
		view.setLayoutParams(lp);
	}
}
