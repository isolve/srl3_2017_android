package utils;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.ConnectivityManager;
import android.provider.Settings.Secure;
import android.util.Log;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import cz.msebera.android.httpclient.Header;
import org.json.JSONArray;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Date;

import se.isolve.srl3.R;
import se.isolve.srl3.SRL3ApplicationClass;
import se.isolve.srl3.reader.BookmarkHistoryNotesManager;

public class LoginHandler {
	
	public interface LoginCallBack {
		public void onLoginQueryCompleted (String arg);
	}
	
	public BookmarkHistoryNotesManager bookmarkHistory;
	
	private String username;
	private String password;
	private String yearCode;
	private String udid;
	private String loginUrlString;
//	NSURLSessionConfiguration *syncSessionConf;
//	NSURLSession *syncSession;
	JSONArray notesToSyncArray;
//	AppDelegate *delegate;
	private String timestampName;
	private String loggedInUser, loggedInPass, loggedInUdid;
	private LoginCallBack listener;
	
	private static LoginHandler instance;
	
	//Singleton = private constructor
    private LoginHandler(Context c) {
        
    }
    
    public void setLoginCallbackListener(LoginCallBack listener){
        this.listener=listener;
    }

    public static void initInstance (Context c) {
        if (instance == null) {
            Log.d("LoginHandler", "+++++++++++Singleton initialized+++++++++++++++");
            instance = new LoginHandler(c);
        }
    }

    public static LoginHandler getInstance() {
        return instance;
    }
    
    public static void destroyInstance () {
    	instance = null;
    }
    
    public boolean hasInternetConnection() {
    	final ConnectivityManager connMgr = (ConnectivityManager)
    	SRL3ApplicationClass.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);

    	final android.net.NetworkInfo wifi = connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
    	final android.net.NetworkInfo mobile = connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

    	if (wifi.isAvailable() && wifi.isConnected()) {
    		return true;
    	} 
    	else if(mobile != null) {
    		return mobile.isConnected();
    	}
    	else {
    		return false;
    	}
    }
    
    public void setTimeStamp() {
		Date date = new Date();
		long timestamp = date.getTime();
		SharedPreferences setTS = SRL3ApplicationClass.getContext().getSharedPreferences("LOGIN",Context.MODE_PRIVATE);
		Editor edit = setTS.edit();
		edit.putLong("timestamp", timestamp);
		edit.commit();
	}
	
	public long getTimestamp() {
		SharedPreferences getTS = SRL3ApplicationClass.getContext().getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
		return getTS.getLong("timestamp", 0);
	}
	
	public void setIsLoggedIn() {
		SharedPreferences settings = SRL3ApplicationClass.getContext().getSharedPreferences("LOGIN",Context.MODE_PRIVATE);
		Editor editor = settings.edit();
		editor.putString("username", loggedInUser);
		editor.putString("password", loggedInPass);
		editor.putString("udid", loggedInUdid);
		editor.commit();
	}
	
	public void checkLogin(String epost, String losenord) {
		
		AsyncHttpClient client = new AsyncHttpClient();

		if (!isUserLoggedIn()) {
			loggedInUser = epost;
			loggedInPass = losenord;
			loggedInUdid = Secure.getString(SRL3ApplicationClass.getContext().getContentResolver(), Secure.ANDROID_ID);
		}
		loggedInUser = loggedInUser.replace(" ", "");

		String urlString = "https://isolve-srl3-service.appspot.com/login";
		RequestParams requestParams = new RequestParams();
		requestParams.add("username", loggedInUser);
		requestParams.add("pw", loggedInPass);
		// requestParams.add("year", "16");
		requestParams.add("year", "18");
		requestParams.add("udid", loggedInUdid);

		client.get(urlString, requestParams, new AsyncHttpResponseHandler() {

		    @Override
		    public void onStart() {
		        // called before request is started
		    }

			@Override
		    public void onRetry(int retryNo) {
		        // called when request is retried
			}

			@Override
			public void onFailure(int arg0, Header[] arg1, byte[] arg2,
					Throwable arg3) {
				// Toast.makeText(getApplicationContext(), arg3.getMessage(), Toast.LENGTH_LONG).show();
				Toast.makeText(SRL3ApplicationClass.getContext(), R.string.connection_to_server_error, Toast.LENGTH_LONG).show();
				listener.onLoginQueryCompleted(SRL3ApplicationClass.getContext().getResources().getString(R.string.connection_to_server_error));
			}

			@Override
			public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {

				String response = null;
				String responseCode = null;
				try {
					response = new String(arg2, "UTF-8");
					responseCode = Character.toString(response.charAt(0)) + Character.toString(response.charAt(1));
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				listener.onLoginQueryCompleted(responseCode);

				Log.d("SplashScreen", "Login response - "+response);

				//listener.onLoginQueryCompleted(response);

				/*if (Character.toString(response.charAt(0)).equalsIgnoreCase("1")) {
					loginOK();
				}
				else if (responseCode.equalsIgnoreCase("-1")) {
					Toast.makeText(SRL3ApplicationClass.getContext(), "Felaktig e-post eller lösenord", Toast.LENGTH_LONG).show();
				}
				else if (responseCode.equalsIgnoreCase("-2")) {
					Toast.makeText(SRL3ApplicationClass.getContext(), "Kontot saknar behörighet till denna lagbok", Toast.LENGTH_LONG).show();
				}
				else if (responseCode.equalsIgnoreCase("-3")) {
					Toast.makeText(SRL3ApplicationClass.getContext(), "Problem med inloggningstjänsten. Vänligen prova igen senare.", Toast.LENGTH_LONG).show();
				}
				else if (responseCode.equalsIgnoreCase("-4")) {
					Toast.makeText(SRL3ApplicationClass.getContext(), "För många öppna sessioner. Vänligen logga ut från en aktiv session.", Toast.LENGTH_LONG).show();
				}*/

			}
		});

	}
	
	public boolean checkSessionTimestamp() {
		long timestamp = LoginHandler.getInstance().getTimestamp();
		Date date = new Date();
		long timestampNow = date.getTime();
		if (timestamp > 0 && timestampNow - timestamp < 86400000) {
			return true;
		}
		else {
			return false;
		}
		
	}

	public boolean isUserLoggedIn() {
		SharedPreferences settings = SRL3ApplicationClass.getContext().getSharedPreferences("LOGIN",Context.MODE_PRIVATE);
		loggedInUser = settings.getString("username", null);
		loggedInPass = settings.getString("password", null);
		loggedInUdid = settings.getString("udid", null);
		if (loggedInUser != null && loggedInPass != null && loggedInUdid != null) {
			return true;
		}
		else {
			return false;
		}
	}
	
	public void logout() {
		SharedPreferences pref = SRL3ApplicationClass.getContext().getSharedPreferences("LOGIN", 0);
		Editor editor = pref.edit();
		
		String loggedInUserEn = loggedInUser;
		String loggedInPassEn = loggedInPass;
		String loggedInUdidEn = loggedInUdid;
		
		try {
			loggedInUserEn = URLEncoder.encode(loggedInUserEn,"UTF-8");
			loggedInPassEn = URLEncoder.encode(loggedInPassEn,"UTF-8");
			loggedInUdidEn = URLEncoder.encode(loggedInUdidEn,"UTF-8");
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		}

		// PWA: 2017-02-04: Updated service and year
		String urlString = "https://isolve-srl3-service.appspot.com/logout?username="+loggedInUserEn+"&pw=" + loggedInPassEn + "&year=17" + "&udid="+loggedInUdidEn;

        AsyncHttpClient client = new AsyncHttpClient();
        
        client.get(urlString, new AsyncHttpResponseHandler () {
        	
        	@Override
			public void onFailure (int arg0, Header[] arg1, byte[] arg2,
					Throwable arg3) {
				Log.d("","logout failed");
			}
			
			@Override
			public void onSuccess (int arg0, Header[] arg1, byte[] arg2) {
				Log.d("","logout succeeded");
			}
        });
        
		// Destroy instance of BookmarkHistoryNotesManager each time a user logs out. A new one will be loaded based on which user logs in again.
		BookmarkHistoryNotesManager.destroyInstance();
		SRLSynchronize.destroyInstance();
			
			editor.remove("username");
			editor.remove("password");
			editor.remove("udid");
			editor.remove("timestamp");
			editor.commit();
	}
    
}