package se.isolve.lib.pagebased;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.View;

public class SRLViewPager extends ViewPager {

    public SRLViewPager(Context context) {
        super(context);
    }

    public SRLViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
    
    @Override
    protected boolean canScroll(View v, boolean checkV, int dx, int x, int y) {
        if (v instanceof CsWebView) {
            return ((CsWebView) v).canScrollHorizontally(-dx);
        } else {
            return super.canScroll(v, checkV, dx, x, y);
        }
    }
}
