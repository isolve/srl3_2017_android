package se.isolve.lib.pagebased;

import org.json.JSONArray;
import org.json.JSONException;

import se.isolve.srl3.R;
import se.isolve.srl3.reader.BookmarkHistoryItem;
import se.isolve.srl3.reader.BookmarkHistoryNotesManager;
import utils.SRLUtils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.CountDownTimer;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.ActionMode;
import android.view.ActionMode.Callback;
import android.view.Display;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.ViewParent;
import android.view.WindowManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

public class CsWebView extends WebView implements OnLongClickListener {
	
	public interface AnchorIDCallBack {
		public void onAnchorIDFetchCompleted (String arg);
	}
	
	protected Context mContext;
	private SurfaceViewCallback mSurfaceViewCallback;
	public boolean mScrolling = false;
	public boolean mScrollingY = false;
	private float mScrollDiffY = 0;
	private float mLastTouchY = 0;
	private float mScrollDiffX = 0;
	private float mLastTouchX = 0;
	public static boolean isVisible = true;
	public boolean loadingContent = false;
	public int ID = 0, article = 0, endID = 0, startID = 0, tocID = 0;
	private String addedNote;
	private boolean mDisplayBlocked;
	private String anchorIdForNote = null;

	private OnScrollChangedCallback mOnScrollChangedCallback;
	private ContextMenuListener contextMenuListener;
	private JavaScriptCallback mJSCallback;
	
	private AnchorIDCallBack listener;
	
	private CountDownTimer mUpdateTitleTimer;
	
	public interface ContextMenuListener {
		public void onContextItemSelected(ActionMode mode, MenuItem item);


	}
	
	public CsWebView(Context context) {
		super(context);

		mContext = context;
		setup(context);
	}

	public CsWebView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);

		mContext = context;
		setup(context);
	}

	public CsWebView(Context context, AttributeSet attrs) {
		super(context, attrs);

		mContext = context;
		setup(context);

	}

	public void setContextMenuListener (ContextMenuListener listener) {
		this.contextMenuListener = listener;
	}
	
	public void setCallback(SurfaceViewCallback repaintCallback) {
		mSurfaceViewCallback = repaintCallback;
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		// TODO Auto-generated method stub
		if (!isVisible) {
			return true;
		}
		
		float xPoint = getDensityIndependentValue(event.getX(), mContext)
				/ getDensityIndependentValue(getScale(), mContext);
		float yPoint = getDensityIndependentValue(event.getY(), mContext)
				/ getDensityIndependentValue(getScale(), mContext);
		if (event.getAction() == MotionEvent.ACTION_DOWN) {
			//Log.d("CSW", "Action down.");
			
			// Needed to check when the title view is touched.
			mSurfaceViewCallback.touchedPoint(this, event.getRawX(), event.getRawY());
			
			mLastTouchX = xPoint;
			mLastTouchY = yPoint;
		} else if (event.getAction() == MotionEvent.ACTION_UP) {
			if (!mScrolling && !CurlView.isVisible) {
				CurlView.isVisible = true;
			}
			mScrollDiffX = 0;
			mScrollDiffY = 0;
			mScrolling = false;
			mScrollingY = false;
		} else if (event.getAction() == MotionEvent.ACTION_MOVE) {
			mScrollDiffX += (xPoint - mLastTouchX);
			mScrollDiffY += (yPoint - mLastTouchY);

			mLastTouchX = xPoint;
			mLastTouchY = yPoint;
			
			float width = mScrollDiffX;
			// Only account for legitimate movement.
			mScrolling = (Math.abs(mScrollDiffX) > 10 || Math.abs(mScrollDiffY) > 10);
			if (Math.abs(mScrollDiffX) > 10) {

				if (Math.abs(mScrollDiffX) > Math.abs(mScrollDiffY)) {
					if (CurlView.isVisible) {
						if (!mScrollingY) {
//							isVisible = false;
//							CurlView.isTransparent = false;
//							mSurfaceViewCallback.showOnTop(true, width);
//							mSurfaceViewCallback.sendEvent(event);
//							mScrollDiffX = 0;
//							mScrollDiffY = 0;
//							mScrolling = false;
//							// return true;
//							getParent().
//							return false;
						}
					}

				} else {
					mScrollingY = true;
				}
			}
		}
		return super.onTouchEvent(event);
	}

	// Handler handler = new Handler();
	
	protected void setup(Context context) {
		mDisplayBlocked = false;
		setOnLongClickListener(this);
		getSettings().setJavaScriptEnabled(true);
		getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
		addJavascriptInterface(new CsJavaScriptHandler(), "Android");
		getSettings().setPluginState(WebSettings.PluginState.ON);
		// setDrawingCacheEnabled(true);
		//setVerticalScrollBarEnabled(false);
		setHorizontalScrollBarEnabled(false);
		
		WebSettings settings = this.getSettings();
		int fontSize = 70 + getFontSize();
        settings.setTextZoom(fontSize);
		
		setFocusable(false);
		setFocusableInTouchMode(false);

		setWebViewClient (new WebViewClient () {
			
			@Override
			public void onPageStarted (WebView view, String url, Bitmap favicon) {
				super.onPageStarted(view, url, favicon);
				loadingContent = true;
			}
			
			public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
				super.onReceivedError(view, errorCode, description, failingUrl);
				loadingContent = false;
			}
			
			@Override
			public void onPageFinished(WebView view, String url) {
				// TODO Auto-generated method stub
				super.onPageFinished(view, url);
				//Log.d("CSWV", "On page finished for - " + view.getId());
				
				loadingContent = false;
				
				if (view.getTag().toString().equals("about:blank")) {
					//Log.d("CSWV", "On page finished - real page");
					mSurfaceViewCallback.realPage(view);
				} else if (view.getTag().toString().equals("initialPage")) {
					//Log.d("CSWV", "On page finished - run script");
					mSurfaceViewCallback.runScript(view);
				    ////Log.d("weburl","backpage:"+ view.getUrl());
				}
				
//				mSurfaceViewCallback.pageFinishedLoading(CsWebView.this);
			}
		});

	}

	public float getDensityIndependentValue(float val, Context ctx) {

		// Get display from context
		Display display = ((WindowManager) ctx
				.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();

		// Calculate min bound based on metrics
		DisplayMetrics metrics = new DisplayMetrics();
		display.getMetrics(metrics);

		return val / (metrics.densityDpi / 160f);

		// return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_PX, val,
		// metrics);

	}

	@Override
	public boolean canScrollHorizontally (int dir) {
		super.canScrollHorizontally(dir);
		return false;
	}
	
	public interface SurfaceViewCallback {
		public void showOnTop(boolean isTop, float width);

		public void sendEvent(MotionEvent me);

		public void hideOnTop(boolean isHide);

		public void realPage(WebView web);

		public void runScript(WebView web);
		
//		public void pageFinishedLoading(CsWebView web);
		
		public void updateTOCTitle(String id);
		
		public void touchedPoint(CsWebView web, float x, float y);
		
		public Activity getActivity();
	}

	@Override
	public boolean onLongClick(View v) {
		// TODO Auto-generated method stub
		if (!CurlView.isTransparent) {
			return true;
		}
		if (!mScrolling && CurlView.isVisible && isVisible) {
			CurlView.isVisible = false;
			mScrolling = true;
			// mSurfaceViewCallback.hideOnTop(true);
		}
		return false;
	}
	
	CustomActionModeCallback mCallback;

	ActionMode mMode;






	@Override
    public ActionMode startActionMode(Callback callback) {
        ViewParent parent = getParent();
        if (parent == null) {
            return null;
        }
		mCallback = new CustomActionModeCallback();
		return parent.startActionModeForChild(this, mCallback);
	}


	@Override
	public ActionMode startActionMode(Callback callback, int type) {
		if (type == ActionMode.TYPE_FLOATING) {
			ViewParent parent = getParent();
			if (parent == null) {
				return null;
			}
			mCallback = new CustomActionModeCallback();
			return parent.startActionModeForChild(this, mCallback);
		}
		return startActionModeForChild(this, mCallback);
	}


	
	private class CustomActionModeCallback implements ActionMode.Callback {
        
        @Override
        public void onDestroyActionMode(ActionMode mode) {
        	Log.d("CSWV", "clearFocus called");
            clearFocus(); // This is the new code to remove the text highlight
             mMode = null;
        }

		@Override
		public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
			// TODO Auto-generated method stub
			contextMenuListener.onContextItemSelected(mode, item);
			return true;
		}

		@Override
		public boolean onCreateActionMode(ActionMode mode, Menu menu) {
			// TODO Auto-generated method stub
			MenuInflater inflater = new MenuInflater(mContext);
			inflater.inflate(R.menu.pages_action_menu, menu);
			return true;
		}


		@Override
		public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
			// TODO Auto-generated method stub
			return false;
		}
    }
	
	public void updateWebViewTitle() {
        Log.d("CSW", "updateWebViewTitle");
		this.loadUrl("javascript:var cutoff = $(window).scrollTop();" +
		        "var id = '';" +
		        "$('#content').children().each(function()" +
		        "{" +
		        "if ($(this).offset().top>cutoff)" +
		        "{" +
		        "id = $(this).attr('id');" +
		        "return false;" +
		        "}" +
		        "});" +   
		        "Android.updateWebViewTitle(id, "+this.getId()+");");
	}
	
	@Override
    protected void onScrollChanged(final int x, final int y, final int oldX, final int oldY)
    {
        super.onScrollChanged(x, y, oldX, oldY);
        if(mOnScrollChangedCallback != null) mOnScrollChangedCallback.onScroll(this, x, y);
        // PWA: use a timer to update title only at end of scrolling        
		if (mUpdateTitleTimer != null) {
			mUpdateTitleTimer.cancel();
		}
		mUpdateTitleTimer = new CountDownTimer(500, 500) {
			public void onTick(long millisUntilFinished) {
			}
			public void onFinish() {
				updateWebViewTitle();
				mUpdateTitleTimer = null;
			}
		}.start();
		
        /*
        int diffY = Math.abs((y-oldY));
        // Limit the velocity at which the title is updated. Hence the number of requests is also reduced.
        if (diffY > 0 && diffY < 2) {
        	//Log.d("CSW", "Webview scrolling slowly");
        	// Update the current top ID position.
          
          this.loadUrl("javascript:var cutoff = $(window).scrollTop();" +
    		"var id = '';" +
    		"$('#content').children().each(function()" +
    		"{" +
    		"if ($(this).offset().top>cutoff)" +
    		"{" +
    		"id = $(this).attr('id');" +
    		"return false;" +
    		"}" +
    		"});" +   
    		"Android.updateWebViewTitle(id, "+this.getId()+");"); //, "+this+"
        }
        */     
    }

    public OnScrollChangedCallback getOnScrollChangedCallback()
    {
        return mOnScrollChangedCallback;
    }

    public void setOnScrollChangedCallback(final OnScrollChangedCallback onScrollChangedCallback)
    {
        mOnScrollChangedCallback = onScrollChangedCallback;
    }

    /**
     * Impliment in the activity/fragment/view that you want to listen to the webview
     */
    public static interface OnScrollChangedCallback
    {
        public void onScroll(CsWebView webView, int l, int t);
    }
    
    public void setTextSizeDifference (int textZoomDifference) {
    	if (textZoomDifference > 0) {
    		increaseTextSize(textZoomDifference);
    	} else {
    		decreaseTextSize(Math.abs(textZoomDifference));
    	}
    }
    
    public void increaseTextSize (int textZoomDifference) {
    	WebSettings settings = getSettings();
    	settings.setTextZoom(settings.getTextZoom() + textZoomDifference);
    }
    
    public void decreaseTextSize (int textZoomDifference) {
    	WebSettings settings = getSettings();
    	settings.setTextZoom(settings.getTextZoom() - textZoomDifference);
    }
	
	private int getFontSize() {
		SharedPreferences pref = mContext.getSharedPreferences("SRLPref", 0);
		Log.d("FontSize" + pref.getInt("fontSize", 10),"");
		int fontSize = pref.getInt("fontSize", 30);
		return pref.getInt("fontSize", 30);
	}
    
    public class CsJavaScriptHandler {
        public CsJavaScriptHandler()  {
        }
        
        @JavascriptInterface
        public void receiveElementMarkserialArrayString(String str) {
        	Log.d("CSW", "receiveElementMarkserialArrayString:" + str);
        	String[] elems = str.split("/");
        	for (int i = 0; i < elems.length; i++) {
        		String[] elemParts = elems[i].split("#");
        		if (elemParts.length == 2) {
        			int ID = Integer.parseInt(elemParts[0]);
        			String serial = elemParts[1];
            		BookmarkHistoryItem item = BookmarkHistoryItem.getItem(ID, 0, null, null, serial, true);
            		BookmarkHistoryNotesManager.getInstance().insertMarking(item);
        		}
        	}
        }
        
        @JavascriptInterface
        public void severalIdCallback(String serie, String ids) throws JSONException {
        	JSONArray serieArr = new JSONArray(serie);
        	JSONArray idsArr = new JSONArray(ids);
        	for(int i = 0; i < serie.length(); i++) {
                String serializedMarks = serieArr.getString(i);
            		int parsedID = Integer.parseInt(idsArr.getString(i));
            		BookmarkHistoryItem item = BookmarkHistoryItem.getItem(parsedID, 0, null, null, serializedMarks, true);
            		BookmarkHistoryNotesManager.getInstance().insertMarking(item);
        	}
        	
        	Log.d("serie: " + serie,"ids: " + ids);
        }
        
        @JavascriptInterface 
        public void anchorIDForBookmark (String ID) {
        	Log.d("JSCallback", "Adding bookmark - "+ID);
        	srlBookmark(ID);
        }
        
        @JavascriptInterface 
        public void anchorIDForNote (String ID) {
        	srlNote(ID);
        }
        
        @JavascriptInterface 
        public void anchorIDForShare (String ID) {
        	
        }
        
        @JavascriptInterface
        public void returnAnchorIDForNote (String ID) {
        	fetchedNoteAnchorID(ID);
        }
        
        @JavascriptInterface
        public void anchorIDForMark (String ID) {
        	srlMark(ID);
        }
        
        @JavascriptInterface 
        public void log (String s) {
        	Log.d("JS", s);
        }
        
        @JavascriptInterface
        public void updateWebViewTitle (String idString, int webViewID) {
        	////Log.d("Pages - CSW", "Current top ID - "+idString);
//        	CsWebView web = webViewForId(webViewID);   
        	mJSCallback.updateTitle(idString, webViewID);
        }
    }
    
    public void loadJavaScriptUrl (String url) {
    	this.loadUrl(url);
    }

    //public void activateJavaScriptInterface (JavaScriptCallback cb) {
    //	addJavascriptInterface(new CsJavaScriptHandler(this), "JSHandler");
    //	setJavaScriptCallback(cb);
    //}

    public void setJavaScriptCallback (final JavaScriptCallback callback)
    {
    	mJSCallback = callback;
    }

    public enum AnchorIDFor {
    	BOOKMARK, NOTE, SHARE, MARK, RETURNID;
    }

    public static interface JavaScriptCallback
    {
    	public void updateTitle (String idString, int webViewID);
    	//    public void onJSRequestCompleted(CsWebView webView);
    }
    
    public void selectionAnchorWithinID (AnchorIDFor anchorIDFor) {
    	String returnFunction = null;
    	if (anchorIDFor == AnchorIDFor.BOOKMARK) {
    		returnFunction = "anchorIDForBookmark";
    	} else if (anchorIDFor == AnchorIDFor.NOTE) {
    		returnFunction = "anchorIDForNote";
    	} else if (anchorIDFor == AnchorIDFor.MARK) {
    		returnFunction = "anchorIDForMark";
    	} else if (anchorIDFor == AnchorIDFor.RETURNID) {
    		returnFunction = "returnAnchorIDForNote";
    	}
    	else {
    		returnFunction = "anchorIDForShare";
    	}
    	/*if (anchorIDFor == AnchorIDFor.MARK) {
    		String url = "javascript:" +
        			"var sel=document.getSelection(); var Id = elementIDContainingNode(sel.anchorNode); var IdFo = elementIDContainingNode(sel.focusNode);" + 
        			"cssApplier.toggleSelection();" +
        			"sel.empty();" +  // Androidspecific - clear selection
        			"var seri = serializeMarksInElementWithID(Id);" +
        			"Android.blockDisplay(seri, Id, IdFo);";
        	Log.d("CSW", "Loading url - "+url);
        	this.loadUrl(url);
    	}*/
	
    	
    	if (anchorIDFor == AnchorIDFor.MARK) {
    		String url = "javascript:var str = getElementMarkserialArrayStringFromSelection();Android.receiveElementMarkserialArrayString(str);";
        	Log.d("CSW", "Loading url - "+url);
        	this.loadUrl(url);
    	}
    		
    	else {
    	String url = "javascript:" +
    			"var sel=document.getSelection();var Id = elementIDContainingNode(sel.anchorNode);sel.empty();Android."+returnFunction+"(Id);";
//    	//Log.d("CSW", "Loading url - "+url);
    	this.loadUrl(url);
    	}
    }

//    public String selectionFocusWithinID () {
//        return [self stringByEvaluatingJavaScriptFromString:@"sel=document.getSelection();elementIDContainingNode(sel.focusNode);"];
//    }
    
    public void addMarkText() {
    	this.selectionAnchorWithinID(AnchorIDFor.MARK);
    }
        
    public void addBookmark () {
    	Log.d("CSW", "Should add bookmark.");
    	this.selectionAnchorWithinID(AnchorIDFor.BOOKMARK);
    }
    
    public void getSelectionID() {
    	this.selectionAnchorWithinID(AnchorIDFor.RETURNID);
    }
    
    public void addNote (String note) {
    	this.addedNote = note;
    	this.selectionAnchorWithinID(AnchorIDFor.NOTE);
    }

    public void srlBookmark (String anchorID) {
        Log.d("CSW","srlBookmark: "+anchorID);
        int ID = Integer.parseInt(anchorID.substring(1));
        if (ID == 0){
        	Log.d("CSW","srlBookmark ID == 0: "+ID);
        	return;
        } 
        Log.d("CSW","srlBookmark ID: "+ID);
        
        
        
        BookmarkHistoryItem item = BookmarkHistoryItem.getItem(ID, 0, null, null, null, true);
        BookmarkHistoryNotesManager.getInstance().insertBookmark(item);
		Toast.makeText(mContext, "Bokmärke skapat!", Toast.LENGTH_LONG).show();
    }
    
    public void srlNote (String anchorID) {
    	Log.d("CSW","srlNote: "+anchorID);
        int ID = Integer.parseInt(anchorID.substring(1));
        if (ID == 0){
        	Log.d("CSW","srlNote ID == 0: "+ID);
        	return;
        } 
        
        Log.d("CSW","srlNote ID: "+ID);
        BookmarkHistoryItem item = BookmarkHistoryItem.getItem(ID, 0, SRLUtils.getCurrentTimeStamp(), null, addedNote, true);
        BookmarkHistoryNotesManager.getInstance().insertNote(item);
    }
    
    public void srlMark (String anchorID) {
    	int ID = Integer.parseInt(anchorID.substring(1));
    	if (ID == 0) {
    		return;
    	}    	
  }

//    public void srlNote:(id)menuController {
//        NSString * anchorID = [self selectionAnchorWithinID];
//        NSLog(@"srlNote:%@", anchorID);
//        NSUInteger ID = [[anchorID stringByReplacingCharactersInRange:NSMakeRange(0, 1) withString:@""] integerValue];
//        if (ID == 0) return;
//        if (self.delegate && [self.delegate isKindOfClass:[DataViewController class]]) {
//            DataViewController * dvc = (DataViewController *)self.delegate;
//            dvc.ID = ID;
//            [dvc performSelector:@selector(showNoteEditorController) withObject:nil afterDelay:0.0];
//        }
//        self.userInteractionEnabled = NO;
//        self.userInteractionEnabled = YES;
//    }
//
//    public void srlShare:(id)menuController {
//        NSString * anchorID = [self selectionAnchorWithinID];
//        NSLog(@"srlShare:%@", anchorID);
//    }
    
    public String getAnchorIdForNotes() {
    	return anchorIdForNote;
    }

    private void fetchedNoteAnchorID(String ids) {
    	listener.onAnchorIDFetchCompleted(ids);
    }
    
    @Override
    public boolean dispatchTouchEvent(MotionEvent pEvent) {
        if (!mDisplayBlocked) {
            return super.dispatchTouchEvent(pEvent);
        }
        return mDisplayBlocked;
    }
    
    public void setAnchorIDCallbackListener(AnchorIDCallBack listener){
        this.listener=listener;
    }
    
}
