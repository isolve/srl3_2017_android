package se.isolve.introview;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import se.isolve.srl3.R;

// import com.actionbarsherlock.app.SherlockFragment;

public class IntroFragment extends Fragment {
    public static final String TAG_ITEM = "INTROVIEW";
    
    TextView mTitleTextView;
    TextView mIntroTextView;
    ImageView mImageView;
    ImageView mBackgroundImageView;
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.intro_view, container, false);
        mBackgroundImageView = (ImageView) rootView.findViewById(R.id.introBackgroundImageView);
        mImageView = (ImageView) rootView.findViewById(R.id.introImageView);
        mTitleTextView = (TextView) rootView.findViewById(R.id.titleTextView);
        mIntroTextView = (TextView) rootView.findViewById(R.id.introTextView);
        IntroPage intro_page = (IntroPage) getArguments().get(TAG_ITEM);
        mTitleTextView.setText(intro_page.title);
        mIntroTextView.setText(intro_page.text);
        mImageView.setImageResource(intro_page.imgResource);
        mBackgroundImageView.setImageResource(intro_page.bgResource);
        return rootView;
    }
}
