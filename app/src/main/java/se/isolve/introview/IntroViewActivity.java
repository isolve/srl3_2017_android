package se.isolve.introview;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.Window;
import android.view.WindowManager;
import se.isolve.srl3.ChooserPageScreen;
import se.isolve.srl3.R;

public class IntroViewActivity extends FragmentActivity implements IntroView.OnSkipClickListener {
	IntroView introView;
	static final public String INTRO_KEY = "se.nj.srl3_2018.intro";
	static final public String INTRO_VERSION_KEY = "se.nj.srl3_2018.intro.version";
	static final public String INTRO_VERSION = "1.0.0";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		setContentView(R.layout.intro_demo_view);
		
		introView = (IntroView) getSupportFragmentManager().findFragmentById(R.id.introview);
		ArrayList<IntroPage> introPages = new ArrayList<IntroPage>();
		introPages.add(new IntroPage("Uppdaterad", "Innehållet i appen motsvarar den tryckta versionen av lagboken 2018", R.drawable.intro_image0, R.drawable.intro_bg));
		introPages.add(new IntroPage("Bläddring", "Bläddra för att hitta till rätt ställe i Lagboken. Använd pilarna för att navigera mellan kapitel.", R.drawable.intro_image1, R.drawable.intro_bg));
		introPages.add(new IntroPage("Gå till", "Med Gå till navigerar du lätt i Lagboken. Skriv i textrutan så visas resultatet direkt.", R.drawable.intro_image2, R.drawable.intro_bg));
		introPages.add(new IntroPage("Anteckningar", "Markera text och peka på Anteckna för att skapa egen anteckning. Du når dina anteckningar från huvudmenyn.", R.drawable.intro_image3, R.drawable.intro_bg));
		introPages.add(new IntroPage("Bokmärken", "Skapa dina egna bokmärken genom att markera text och peka på Bokmärk. Du hittar dina bokmärken genom huvudmenyn.", R.drawable.intro_image4, R.drawable.intro_bg));
		introPages.add(new IntroPage("Register", "I Register hittar du tre flikar som hjälper dig att hitta rätt i Lagboken: Innehåll, Författningar och Sakregister.", R.drawable.intro_image5, R.drawable.intro_bg));
		introView.setData(introPages);
		introView.setOnSkipClickListener(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		// getMenuInflater().inflate(R.menu.intro_view, menu);
		return true;
	}

	@Override
	public void onSkipClicked() {
		SharedPreferences prefs = this.getSharedPreferences(INTRO_KEY, Context.MODE_PRIVATE);
		prefs.edit().putString(INTRO_VERSION_KEY, INTRO_VERSION).commit();
	 	Intent intent = new Intent(this, ChooserPageScreen.class);
      	// intent.putExtra(NameFromList, 0);
		startActivity(intent);
		overridePendingTransition(0, 0);
		finish();
	}
	
	@Override
	public boolean onKeyUp(int keyCode, KeyEvent msg) {
	     switch(keyCode) {
	     case(KeyEvent.KEYCODE_BACK):
	    	 	onSkipClicked();
				return true;    
	     }
	     return false;
	}
}

