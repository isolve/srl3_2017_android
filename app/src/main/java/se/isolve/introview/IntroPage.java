package se.isolve.introview;

import android.os.Parcel;
import android.os.Parcelable;

public class IntroPage implements Parcelable {
	public String title;
    public String text;
    public int imgResource;
    public int bgResource;

    public IntroPage(String title, String text, int imgResource, int bgResource) {
    	this.title = title;
    	this.text = text;
        this.imgResource = imgResource;
        this.bgResource = bgResource;
    }

    public IntroPage(Parcel in){
    	this.title = in.readString();
        this.text = in.readString();
        this.imgResource = in.readInt();
        this.bgResource = in.readInt();
    }

    public static final Parcelable.Creator<IntroPage> CREATOR
            = new Parcelable.Creator<IntroPage>() {
        public IntroPage createFromParcel(Parcel in) {
            return new IntroPage(in);
        }

        public IntroPage[] newArray(int size) {
            return new IntroPage[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    	dest.writeString(title);
        dest.writeString(text);
        dest.writeInt(imgResource);
        dest.writeInt(bgResource);
    }
}