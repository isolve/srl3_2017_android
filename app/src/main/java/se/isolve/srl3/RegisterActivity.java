package se.isolve.srl3;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

import se.isolve.srl3.IndexView.IndexViewTouchEventListener;
import se.isolve.srl3.database.BookDB;
import se.isolve.srl3.database.BookElement;
import se.isolve.srl3.reader.Pages;
import utils.LayoutUtils;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.VideoView;

public class RegisterActivity extends SRLMenuActivity implements OnCompletionListener,
		OnPreparedListener, OnClickListener, OnItemClickListener, IndexViewTouchEventListener {
	private VideoView mVV;
	private LinearLayout llRegister;

	private ArrayList<BookElement> searchArray1 = new ArrayList<BookElement>(),
			searchArray2 = new ArrayList<BookElement>();
	private Button btn1, btn2, btn3;
	private BookDB mDbHelper;
	private ListView lvRegister1, lvRegister2;
	private ImageView imgRegister;
	private int width = 0;
	private int height = 0;
	private RelativeLayout llListRegister, wvRegisterContainer;
	private LinearLayout llbanner;
//	private TaskGetList task = null;
	private CountDownTimer mSearchTimer;
	private String NameFromList = "list";
	private int extraInt;
	private int VIDEO_STATE = 0;
	private static final int VIDEO_P2_IS_PLAYING = 4;
	private static final int VIDEO_P3_IS_PLAYING = 2;
	private static final int NO_VIDEOS_PLAYING = 0;
	private static final String SAKREGISTER_LOCATION = "file:///android_asset/html/sakregister.xhtml";
	private static final String ASSETS_LOCATION_PREFIX = "file:///android_asset/html/";
	private static final String ARTICLE_PREFIX = "SFS%20";
	private static final String CELEX_PREFIX = "celex%20";
	// private static final String ZUSK = "zusk";
    private static final String SR_LINK = "sr_";
	private static final String LIST_1 = "list1";
	private static final String LIST_2 = "list2";
	private static final String SELECTED_TAB = "selected_tab";
	private static final String REGISTER_STATE = "register_state";
	private SharedPreferences mPrefs;
	private int selectedTab;
	
	private IndexView indexView;
	
	private WebView wvRegister;
	//private ProgressDialog dialog;
	private ProgressBar wvProgress;
	
	boolean init = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.register);

		init = true;
		
		VIDEO_STATE = NO_VIDEOS_PLAYING;
		
		mPrefs = this.getSharedPreferences(REGISTER_STATE, Context.MODE_PRIVATE);
		
		extraInt = getIntent().getIntExtra(NameFromList, 0);
		initViews();
		if (extraInt == 1) {
			initSplashVideo("join3");
			mVV.start();
			VIDEO_STATE = VIDEO_P3_IS_PLAYING;
		} else {
			mVV.setVisibility(View.GONE);
			llRegister.setAlpha(1);
			llRegister.setVisibility(View.VISIBLE);
			setView();
			
			restoreState();
		}
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		mDbHelper = new BookDB(this);
		if (extraInt == 1) {
			new Thread() {
				public void run() {
					mVV.start();
					extraInt = 0;
				};
			}.start();
			
			restoreState();
			
		} else {
			if (!init) {
				initSplashVideo("join3");
				mVV.start();
				VIDEO_STATE = VIDEO_P3_IS_PLAYING;
			}
		}
	}
	
	public void initViews() {
		llRegister = (LinearLayout)findViewById(R.id.ll_box_llregister);
		Display display = getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		width = size.x;
		height = size.y;
		Typeface tfMyriadPro = SRL3ApplicationClass.getMyriadPro();
		btn1 = (Button) findViewById(R.id.btn1);
		btn1.setOnClickListener(this);
		btn2 = (Button) findViewById(R.id.btn2);
		btn2.setOnClickListener(this);
		btn3 = (Button) findViewById(R.id.btn3);
		btn3.setOnClickListener(this);
		btn1.setTypeface(tfMyriadPro);
		btn2.setTypeface(tfMyriadPro);
		btn3.setTypeface(tfMyriadPro);
		
		lvRegister1 = (ListView) findViewById(R.id.lv_register_1);
		lvRegister2 = (ListView) findViewById(R.id.lv_register_2);
		
		lvRegister1.setVisibility(View.VISIBLE);
		lvRegister2.setVisibility(View.INVISIBLE);
		
		lvRegister1.setTag(LIST_1);
		lvRegister2.setTag(LIST_2);
		
		lvRegister1.setOnItemClickListener(RegisterActivity.this);
		lvRegister2.setOnItemClickListener(RegisterActivity.this);
		
		wvRegister = (WebView) findViewById(R.id.wv_register);
		wvRegisterContainer = (RelativeLayout) findViewById(R.id.wv_register_container);
		wvRegister.setBackgroundColor(Color.argb(1, 0, 0, 0));
		WebSettings settings = wvRegister.getSettings();
		settings.setJavaScriptEnabled(true);
		imgRegister = (ImageView) findViewById(R.id.imgRegister);
		imgRegister.setOnClickListener(this);
		llbanner = (LinearLayout) findViewById(R.id.llbanner);
		llListRegister = (RelativeLayout) findViewById(R.id.lllistregister);
		mVV = (VideoView)findViewById(R.id.myvideoview);
		mVV.setOnCompletionListener(this);
		mVV.setOnPreparedListener(this);
		double f = 0.2; // 0.189;
		int h = (int)(height * f);
		llbanner.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,h));
		double widthMarginRatio = 0.14;
		int w = (int) (width * widthMarginRatio); // getResources().getDimension(R.dimen.marginbook);
		LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(width - w, LayoutParams.MATCH_PARENT);
		layoutParams.gravity = Gravity.CENTER;
		llListRegister.setLayoutParams(layoutParams);
		
		wvProgress = (ProgressBar) findViewById(R.id.progressBar);
		
		int indexTop = h + (int) getResources().getDimension(R.dimen.edittext_search_heigh);
		int indexHeight = height - indexTop;
		
		indexView = (IndexView) findViewById(R.id.reg_indexview);
		LayoutUtils.setLayout(indexView, 0, indexTop, indexHeight, (int)((float)width*(IndexView.INDEX_WIDTH_PERCENT/100.0)));
		indexView.showTouchedAlphabetInContainerView((RelativeLayout) findViewById(R.id.reg_main_view));
		indexView.setIndexViewTouchEventListener(this);
		indexView.setVisibility(View.GONE);
		
		// Hide the Sakregister.
		wvRegisterContainer.setVisibility(View.INVISIBLE); 
		
//		dialog = new ProgressDialog(this);
		
		this.showProgress();
		
		wvRegister.loadUrl(SAKREGISTER_LOCATION);
		
		handleWebViewEvents();
	}

	private void handleWebViewEvents () {
		wvRegister.setWebViewClient(new WebViewClient () {
			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				if (url.equals(SAKREGISTER_LOCATION)) {
					return false;
				}
				
				//Log.d("REG", "should override url - "+url);
				// Remove the prefix of the url pointing to the html file location.
				url = url.replace(ASSETS_LOCATION_PREFIX, "");
				
				if (url.startsWith(SR_LINK)) {
					gotoElementWithID(url);
					return true;
				} else {
					String urlDecoded;
					try {
						urlDecoded = URLDecoder.decode(url, "UTF-8");
					} catch (UnsupportedEncodingException e) {
						// TODO Auto-generated catch block
						return true;
					}
					String[] arr = urlDecoded.split(" ");
					String[] alpha_categories = { "a", "b", "c", "d", "e", "f", "g", "h",
							"i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t",
							"u", "v", "w", "x", "y", "z" };
					int i = 0;

					if (i < arr.length && arr[i].equalsIgnoreCase("SFS")) {
						i++;
					}
					if (i < arr.length && arr[i].equalsIgnoreCase("celex")) {
						i++;
					}
					
					String law = "";
					if (i < arr.length) {
						law = arr[i++];
					}
					String chapter = "";
					if (i < arr.length) {
						chapter = arr[i++];
					}

					if (i < arr.length) {
						for (int j = 0; j < alpha_categories.length; j++) {
							if (alpha_categories[j].equalsIgnoreCase(arr[i])) {
								chapter += " " + arr[i];
								i++;
								break;
							}
						}
					}

					String paragraph = "";
					    
					// Check for "§"
					if ((i < arr.length) && arr[i].startsWith("§") ) {
					     paragraph = chapter;
					     chapter = "";
					} else {
						// Check for "kap."
						if (i < arr.length && arr[i].toLowerCase().startsWith("kap")) {
							i++;
						}

						// Get paragraph
						if (i < arr.length) {
							paragraph = arr[i];
							i++;
						}

						// Check for paragraph category (ie a-z)
						if (i < arr.length) {
							for (int j = 0; j < alpha_categories.length; j++) {
								if (alpha_categories[j].equalsIgnoreCase(arr[i])) {
									paragraph += " " + arr[i];
									i++;
									break;
								}
							}
						}
					}
					
					// Don't care about rest
			        ArrayList<BookElement> result = mDbHelper.searchLaw(law, chapter, paragraph);
			        if (result.size() > 0) {
			        	BookElement elem = result.get(0);
				        
			        	indexView.setVisibility(View.GONE);
			        	
			        	book = elem;
			        	llRegister.animate().alpha(0f).setDuration(50);
						mVV.setVisibility(View.VISIBLE);
						int res = RegisterActivity.this.getResources().getIdentifier("join2", "raw",
								getPackageName());
						mVV.setVideoURI(Uri.parse("android.resource://" + getPackageName()
								+ "/" + res));
						mVV.start();
						RegisterActivity.this.VIDEO_STATE = VIDEO_P2_IS_PLAYING;
			        	
				        //Log.d("REG", "Gonna open ID - "+elem.ID);
				        
			        }
				}
				
				return true;
			}
			
			@Override
			public void onPageFinished(WebView view, String url) {
				hideProgress();
			}
		});
	}
	
	private boolean arrayContainsElement (String[] arr, String element) {
		for (String s : arr) {
			if (s.equals(element)) {return true;}
		}
		
		return false;
	}
	
	private void showProgress () {
//		dia//Log.show();
		wvProgress.animate();
	}
	
	private void hideProgress () {
//		dia//Log.dismiss();
		wvProgress.clearAnimation();
		wvProgress.setVisibility(View.INVISIBLE);
	}
	
	private void gotoElementWithID (String ID) {
	    String js = "javascript:var p=$('#"+ID+"').position().top;$(window).scrollTop(p);";
	    wvRegister.loadUrl(js);
	}

	
	private void startTimer() {
		if (mSearchTimer != null) {
			mSearchTimer.cancel();

			// //Log.e(LOG_TAG, "Timer was cancelled");
		}
		mSearchTimer = new CountDownTimer(400, 400) {
			public void onTick(long millisUntilFinished) {/* do nothing */
			}

			public void onFinish() {
//				if (task != null
//						&& task.getStatus().equals(AsyncTask.Status.RUNNING)) {
//					task.cancel(true);
//				}

				TaskGetList task1 = new TaskGetList(searchArray1, lvRegister1);
				task1.execute("");
				
				TaskGetList task2 = new TaskGetList(searchArray2, lvRegister2);
				task2.execute("");

				// //Log.e(LOG_TAG, "Done timer starting search with: " +
				// filterText.getText().toString());
				mSearchTimer = null;
			}
		}.start();
	}

	private void selectedTabChanged(int btnID) {
		int blue = getApplicationContext().getResources().getColor(R.color.blueback);
		int white = getApplicationContext().getResources().getColor(R.color.white);
		
		switch (btnID) {
		case R.id.btn1:
			btn1.setBackgroundResource(R.drawable.left_around_white);
			btn2.setBackgroundResource(R.drawable.button_center_selector);
			btn3.setBackgroundResource(R.drawable.button_right_selector);
			btn1.setTextColor(blue);
			btn2.setTextColor(white);
			btn3.setTextColor(white);
			break;
		case R.id.btn2:
			btn1.setBackgroundResource(R.drawable.button_left_selector);
			btn2.setBackgroundResource(R.drawable.center_around_white);
			btn3.setBackgroundResource(R.drawable.button_right_selector);
			btn1.setTextColor(white);
			btn2.setTextColor(blue);
			btn3.setTextColor(white);
			break;
		case R.id.btn3:
			btn1.setBackgroundResource(R.drawable.button_left_selector);
			btn2.setBackgroundResource(R.drawable.button_center_selector);
			btn3.setBackgroundResource(R.drawable.right_around_white);
			btn1.setTextColor(white);
			btn2.setTextColor(white);
			btn3.setTextColor(blue);
			break;
		default:
			break;
		}
	}
	
	private void restoreState () {
		selectedTab = mPrefs.getInt(SELECTED_TAB, 1);
		View selectedView = viewWithTag(selectedTab);
		
		if (selectedView != null) {
			viewClicked(selectedView);
		}
	}
	
	private View viewWithTag (int tag) {
		if (tag == 1) {
			return btn1;
		} else if (tag == 2) {
			return btn2;
		} else if (tag == 3) {
			return btn3;
		} 
		
		return null;
	}
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		init = false;
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
	}

	private void setAdapter(ListView list, ArrayList<BookElement> arr) {
		ArrayList<String> listStr = new ArrayList<String>();
		ArrayList<Integer> listPoss = new ArrayList<Integer>();
		
		ArrayList<BookElement> searchArray = arr;
		
		if (list.getTag().equals(LIST_1)) {
			searchArray1 = arr;
		} else {
			searchArray2 = arr;
		}
		
		if (searchArray == null)
			return;
		for (BookElement e : searchArray) {
			listStr.add(labelForElement(e));
			float pos = (float)mDbHelper.positionForElement(e);
			// double indexPosX = ( pos * 0.845 )*100;
			int indexPosX = (int) (pos * 1000);
			//Log.i("indexPosX", "indexPosX " + indexPosX);
			//Log.i("PosX", "PosX " + pos);
			listPoss.add(indexPosX);
		}
		RegisterAdapter adapter = new RegisterAdapter(RegisterActivity.this, listPoss,
				listStr);
		list.setAdapter(adapter);
	}

	private void setSelectedTab (View v) {
		int tabNumber = Integer.parseInt(v.getTag().toString());
		selectedTab = tabNumber;
		SharedPreferences.Editor ed = mPrefs.edit();
        ed.putInt(SELECTED_TAB, selectedTab);
        ed.commit();
	}
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		viewClicked(v);
	}
	
	private void viewClicked (View v) {
		
		switch (v.getId()) {
		case R.id.btn1:
			indexView.setVisibility(View.GONE);
			wvRegisterContainer.setVisibility(View.INVISIBLE);
			this.showProgress();
			lvRegister1.setVisibility(View.VISIBLE);
			lvRegister2.setVisibility(View.INVISIBLE);
			selectedTabChanged(v.getId());
			setSelectedTab(v);
//			startTimer("btn1");
			break;
		case R.id.btn2:
			indexView.setVisibility(View.GONE);
			wvRegisterContainer.setVisibility(View.INVISIBLE);
			this.showProgress();
			lvRegister1.setVisibility(View.INVISIBLE);
			lvRegister2.setVisibility(View.VISIBLE);
			selectedTabChanged(v.getId());
			setSelectedTab(v);
//			startTimer("btn2");
			break;
		case R.id.btn3:		
			if (VIDEO_STATE != VIDEO_P3_IS_PLAYING) {
				indexView.setVisibility(View.VISIBLE);
			}
			lvRegister1.setVisibility(View.INVISIBLE);
			lvRegister2.setVisibility(View.INVISIBLE);
			wvRegisterContainer.setVisibility(View.VISIBLE);
//			wvRegister.loadUrl(SAKREGISTER_LOCATION); 
			selectedTabChanged(v.getId());
			setSelectedTab(v);
			//startTimer("btn3");
			break;
		case R.id.imgRegister:
			Intent intent = new Intent(RegisterActivity.this, ListGuide.class);
			startActivity(intent);
			overridePendingTransition(R.anim.your_left_to_right, R.anim.your_right_to_left);
//			RegisterActivity.this.finish();
			break;
		default:
			break;
		}
	}

	class TaskGetList extends AsyncTask<String, Void, RegisterAdapter> {
		
		ArrayList<BookElement> searchArray = new ArrayList<BookElement>();
		ListView list = null;
		
		TaskGetList (ArrayList<BookElement> arr, ListView l) {
			this.searchArray = arr;
			this.list = l;
		}
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
//			searchArray.clear();
			//Log.d("REG", "Gonna load " + list.getTag());
		}

		@Override
		protected RegisterAdapter doInBackground(String... params) {
			// TODO Auto-generated method stub
			if (list.getTag().equals(LIST_1)) {
				mDbHelper.createDatabase();
				searchArray = mDbHelper.getTOC();
				mDbHelper.close();
			} else if (list.getTag().equals(LIST_2)) {
				mDbHelper.createDatabase();
				searchArray = mDbHelper.getLawsInOrderOfSFS(); // getLawsInOrder();
				mDbHelper.close();
			}
//			else if (params[0].equalsIgnoreCase("btn3")) {
//				mDbHelper.createDatabase();
//				searchArray = mDbHelper.getRandomParagraphs(8);
//				mDbHelper.close();
//			}
			
			ArrayList<String> listStr = new ArrayList<String>();
			ArrayList<Integer> listPoss = new ArrayList<Integer>();
			
			if (list.getTag().equals(LIST_1)) {
				searchArray1 = searchArray;
			} else {
				searchArray2 = searchArray;
			}
			
			if (searchArray == null)
				return null;
			for (BookElement e : searchArray) {
				listStr.add(labelForElement(e));
				float pos = (float)mDbHelper.positionForElement(e);
				// double indexPosX = ( pos * 0.845 )*100;
				int indexPosX = (int) (pos * 1000);
				//Log.i("indexPosX", "indexPosX " + indexPosX);
				//Log.i("PosX", "PosX " + pos);
				listPoss.add(indexPosX);
			}
			RegisterAdapter adapter = new RegisterAdapter(RegisterActivity.this, listPoss,
					listStr);
			
			return adapter;
		}

		@Override
		protected void onPostExecute(RegisterAdapter result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			//Log.d("REG", "Loaded list - "+list.getTag());
			//Log.d("REG", "Loaded list of elements - "+searchArray.toString());
//			setAdapter(list, searchArray);
//			list.setVisibility(View.VISIBLE);
			list.setAdapter(result);
			// hideProgress();
		}
	}

	private String labelForElement(BookElement elem) {
		String text = elem.getText();
		if (text != null && !text.trim().equalsIgnoreCase("")) {
			return text;
		}
		StringBuffer bf = new StringBuffer();
		if (elem.getChapter() != null && elem.getChapter().length() > 0) {
			int c = Integer.parseInt(elem.getChapter());
			bf.append(c).append(" kap.");
		}
		if (elem.getParagraph() != null && elem.getParagraph().length() > 0) {
			int p = Integer.parseInt(elem.getParagraph());
			bf.append(p).append(" §");
		}
		return bf.toString();
	}

	protected void initSplashVideo(String name) {
		int res = this.getResources().getIdentifier(name, "raw",
				getPackageName());
		if (!playFileRes(res))
			return;
	}

	private boolean playFileRes(int fileRes) {
		if (fileRes == 0) {
			stopPlaying();
			return false;
		} else {
			mVV.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" + fileRes));
			return true;
		}
	}

	public void stopPlaying() {
		mVV.stopPlayback();
	}

	private void setView() {
		
		if (mSearchTimer != null) {
			mSearchTimer.cancel();
		}
		mSearchTimer = new CountDownTimer(400, 400) {
			public void onTick(long millisUntilFinished) {/* do nothing */
			}

			public void onFinish() {
				//				if (task != null
				//						&& task.getStatus().equals(AsyncTask.Status.RUNNING)) {
				//					task.cancel(true);
				//				}

				if (searchArray1.size() == 0 || searchArray2.size() == 0) {
					Log.d("REG", "Gonna load lists");
					TaskGetList task1 = new TaskGetList(searchArray1, lvRegister1);
					task1.execute("");
					
					TaskGetList task2 = new TaskGetList(searchArray2, lvRegister2);
					task2.execute("");

					// //Log.e(LOG_TAG, "Done timer starting search with: " +
					// filterText.getText().toString());
					mSearchTimer = null;
				}
			}
		}.start();
	}

	@Override
	public void onPrepared(MediaPlayer mp) {
		// TODO Auto-generated method stub
		mp.setLooping(false);
	}

	@Override
	public void onCompletion(MediaPlayer arg0) {
		// TODO Auto-generated method stub
		if (VIDEO_STATE == VIDEO_P2_IS_PLAYING) {
			VIDEO_STATE = NO_VIDEOS_PLAYING;
			//Log.d("Reg", "Opening book with ID - "+book.ID);
			Intent intent = new Intent(RegisterActivity.this, Pages.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			intent.putExtra(BookElement.BOOK_ID, book.ID);
			startActivity(intent);
			overridePendingTransition (0, 0);
			extraInt = 0;
			return;			
		} else if (VIDEO_STATE == VIDEO_P3_IS_PLAYING) {
			VIDEO_STATE = NO_VIDEOS_PLAYING;
			llRegister.setAlpha(1);
			llRegister.setVisibility(View.VISIBLE);
			setView();
			
			restoreState();
			
			return;
		}
		
		VIDEO_STATE = NO_VIDEOS_PLAYING;
	}
	
	
	private BookElement book;
	@Override
	public void onItemClick(AdapterView<?> parent, View arg1, int arg2, long arg3) {
		// TODO Auto-generated method stub
		
		ArrayList<BookElement> searchArray = searchArray1;
		
		if (parent.getTag().equals(LIST_2)) {
			searchArray = searchArray2;
		} 
		
		if(searchArray != null && searchArray.size() > 0){
			
			book = searchArray.get(arg2);
			llRegister.animate().alpha(0f).setDuration(50);
			mVV.setVisibility(View.VISIBLE);
			int res = RegisterActivity.this.getResources().getIdentifier("join2", "raw",
					getPackageName());
			mVV.setVideoURI(Uri.parse("android.resource://" + getPackageName()
					+ "/" + res));
			mVV.start();
			this.VIDEO_STATE = VIDEO_P2_IS_PLAYING;
		}
	}
	
	
	public class RegisterAdapter extends BaseAdapter {
		List<String> result;
		Context context;
		List<Integer> listPos;

		public RegisterAdapter(Activity activity, List<Integer> listPos,
				List<String> objects) {
			result = objects;
			context = activity;
			this.listPos = listPos;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return result.size();
		}

		@Override
		public Object getItem(int arg0) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return arg0;
		}

		public class Holder {
			TextView tvOrder;
			SeekBar bookmarkBar;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			Holder holder = new Holder();
			if (convertView == null) {
				LayoutInflater inflater = ((Activity) context).getLayoutInflater();
				convertView = inflater.inflate(R.layout.list_item, null);
				holder.tvOrder = (TextView) convertView.findViewById(R.id.twOrder);
				holder.bookmarkBar = (SeekBar) convertView.findViewById(R.id.bookmarkBar);
				convertView.setTag(holder);

			} else {
				holder = (Holder) convertView.getTag();
			}
			holder.tvOrder.setText(result.get(position));
			holder.bookmarkBar.setProgress(listPos.get(position));
			//holder.bookmarkBar.setThumb(context.getResources().getDrawable(R.drawable.red_arrow));
			holder.bookmarkBar.setEnabled(false);
			Typeface font1 = Typeface.createFromAsset(context.getAssets(),
				    "html/font/MyriadPro-Regular.otf");
			holder.tvOrder.setTypeface(font1);
			return convertView;
		}

	}

	@Override
	public void indexViewTouchesEnded(int index) {
		// TODO Auto-generated method stub
		this.goToIndex(index);
	}
	
	private void goToIndex (int index) {
		String js = "javascript:var p=$('.srbok:eq("+index+")').position().top - 10 ;$(window).scrollTop(p);";
		this.wvRegister.loadUrl(js);
	}
}
