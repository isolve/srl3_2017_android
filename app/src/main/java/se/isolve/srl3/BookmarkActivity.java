package se.isolve.srl3;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.DataSetObserver;
import android.graphics.Point;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.VideoView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import se.isolve.srl3.database.BookDB;
import se.isolve.srl3.database.BookElement;
import se.isolve.srl3.reader.BookmarkHistoryItem;
import se.isolve.srl3.reader.BookmarkHistoryNotesManager;
import se.isolve.srl3.reader.Pages;

public class BookmarkActivity extends SRLMenuActivity implements OnCompletionListener,
		OnPreparedListener, OnItemClickListener, OnMenuItemClickListener {
	public ArrayList<BookElement> searchArray = new ArrayList<BookElement>();
	private ArrayList<String> textArray = new ArrayList<String>();
	private ArrayList<BookmarkHistoryItem> bookmarkHistoryItems = new ArrayList<BookmarkHistoryItem>();
	private BookDB bookDB;
	private ListView lvBookmark;
	private ImageView imgBookmark;
	private int width = 0;
	private int height = 0;
	private LinearLayout mListWrapper;
	private LinearLayout mLinearList;
	private LinearLayout llbookmark;
	private TaskGetList task = null;
	private CountDownTimer mSearchTimer;
	private String NameFromList = "list";
	private int extraInt;
	private VideoView mVV;
	private BookElement book;
	private int VIDEO_STATE = 0;
	private ImageButton deleteButton;
	private TextView bookmarkHeader;
	private static final int VIDEO_P2_IS_PLAYING = 4;
	private static final int VIDEO_P3_IS_PLAYING = 2;
	BookmarkAdapter adapter;
	private ArrayList<DataSetObserver> observers = new ArrayList<DataSetObserver>();
	ArrayList<String> listStr;
	ArrayList<Integer> listPoss;
	ArrayList<String> listText;
	private boolean isEditing;
	private BookmarkHistoryItem bookmarkHistoryItem;
	private Integer tag;
	private TextView mainHeader;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.bookmark);
		isEditing = false;
		
		initViews();
		extraInt = getIntent().getIntExtra(NameFromList, 0);
		initViews();
		if (extraInt == 1) {
			initSplashVideo("join3");
			VIDEO_STATE = VIDEO_P3_IS_PLAYING;
		} else {
			mVV.setVisibility(View.GONE);
			mListWrapper.setVisibility(View.VISIBLE);
			setView();
		}
	}

//	@Override
//	public boolean onKeyUp(int keyCode, KeyEvent msg) {
//
//	     switch(keyCode) {
//	     case(KeyEvent.KEYCODE_BACK):
//	    	 	Intent intent = new Intent(this, ChooserPageScreen.class);
//	          	intent.putExtra(NameFromList, 0);
//				startActivity(intent);
//				overridePendingTransition(0, 0);
//				finish();
//				return true;    
//	     }
//	     return false;
//	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		bookDB = new BookDB(this);
		if(extraInt == 1){
			new Thread(){
				public void run() {
					mVV.start();
					extraInt = 0;
				};
			}.start();
		} else {
			initSplashVideo("join3");
			mVV.start();
			VIDEO_STATE = VIDEO_P3_IS_PLAYING;
		}
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
	}

	private class CustomComparator implements Comparator<BookmarkHistoryItem> {
		@Override
		public int compare(BookmarkHistoryItem item1, BookmarkHistoryItem item2) {
			return item1.getDate().compareTo(item2.getDate());
		}
	}

	public void initViews() {
		mainHeader = (TextView) findViewById(R.id.lbh_title);
		mainHeader.setTypeface(SRL3ApplicationClass.getMyriadPro());
		mListWrapper = (LinearLayout) findViewById(R.id.ll_notes);
		mVV = (VideoView) findViewById(R.id.myvideoview_note);
		Display display = getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		width = size.x;
		height = size.y;
		lvBookmark = (ListView) findViewById(R.id.lv_bookmark);
		lvBookmark.setOnItemClickListener(this);
		imgBookmark = (ImageView) findViewById(R.id.imgBookmark);
		imgBookmark.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(BookmarkActivity.this, ListGuide.class);
				startActivity(intent);
				overridePendingTransition(R.anim.your_left_to_right, R.anim.your_right_to_left);
//				BookmarkActivity.this.finish();
			}
		});
		llbookmark = (LinearLayout) findViewById(R.id.llBookmark);

		double f = 0.2;
		int h = (int) (height * f);
		llbookmark.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, h));

		mLinearList = (LinearLayout) findViewById(R.id.layout_bookmark);
		double widthMarginRatio = 0.14;
		int w = (int) (width * widthMarginRatio); // getResources().getDimension(R.dimen.marginbook);
		LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(width - w, LayoutParams.MATCH_PARENT);
		layoutParams.gravity = Gravity.CENTER;
		mLinearList.setLayoutParams(layoutParams);
		mVV.setOnCompletionListener(this);
		mVV.setOnPreparedListener(this);
	}

	class TaskGetList extends AsyncTask<String, Void, Integer> {
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			searchArray.clear();
			textArray.clear();
		}

		@Override
		protected Integer doInBackground(String... params) {
			bookDB.createDatabase();
			bookmarkHistoryItems = BookmarkHistoryNotesManager.getInstance().getBookmarks();
			Collections.sort(bookmarkHistoryItems, new BookmarkActivity.CustomComparator());
			Collections.reverse(bookmarkHistoryItems);
			for (BookmarkHistoryItem item : bookmarkHistoryItems) {
				if	(item.text != null && item.text.length() > 0) {
					textArray.add(item.text);
				} else {
					textArray.add(bookDB.articleNameForElement(bookDB.elementWithID(item.ID)));
				}
				BookElement element = bookDB.elementWithID(item.ID);
				searchArray.add(element);
			}
			bookDB.close();
			return null;
		}

		@Override
		protected void onPostExecute(Integer result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			setAdapter();
		}

	}

	private void setAdapter() {
		listStr = new ArrayList<String>();
		listPoss = new ArrayList<Integer>();
		if (searchArray == null)
			return;
		for (int i = 0; i < searchArray.size(); i++) {
			BookElement e = searchArray.get(i);
			//listStr.add(labelForElement(e));
			
			// CJA 2015.4.28
			//String articleName = bookDB.articleNameForElement(e);
			//listStr.add(articleName);
				
			listStr.add(textArray.get(i));
			
			float pos = (float)bookDB.positionForElement(e);
			// double indexPosX = ( pos * 0.845 )*100;
			int indexPosX = (int) (pos * 1000);
			//Log.i("indexPosX", "indexPosX " + indexPosX);
			//Log.i("PosX", "PosX " + pos);
			listPoss.add(indexPosX);
		}
		if (searchArray.isEmpty()) {
			BookElement noItem = new BookElement();
			noItem.ID = -1;
			listStr.add(getResources().getString(R.string.message_no_bookmarks));
			listPoss.add(-1);
			searchArray.add(noItem);
		} else {
			lvBookmark.setOnItemLongClickListener( new AdapterView.OnItemLongClickListener () {
				public boolean onItemLongClick(AdapterView<?> av, View v, int pos, long id) {
					showPopup(v);
					return true;
				}
			});
		}
		adapter = new BookmarkAdapter(BookmarkActivity.this);
		lvBookmark.setAdapter(adapter);
	}
	
	public void showPopup(View v) {
	    PopupMenu popup = new PopupMenu(this, v);
	    BookmarkAdapter.Holder h = (BookmarkAdapter.Holder) v.getTag();
	    tag = (Integer) h.pos;
		BookElement bookie = searchArray.get(tag);
		bookmarkHistoryItem = new BookmarkHistoryItem();
		bookmarkHistoryItem.ID = bookie.ID;
	    MenuInflater inflater = popup.getMenuInflater();
	    popup.setOnMenuItemClickListener(this);
	    inflater.inflate(R.menu.delete_item_menu, popup.getMenu());
	    popup.show();
	}
	
	@Override
	public boolean onMenuItemClick(MenuItem item) {
	    switch (item.getItemId()) {
	        case R.id.menu_ta_bort:
				BookmarkHistoryNotesManager.getInstance().deleteBookmark(bookmarkHistoryItem);
				searchArray.remove(tag.intValue());
				textArray.remove(tag.intValue());
				setAdapter();
	            return true;
	        case R.id.menu_ta_bort_alla:
	        	BookmarkHistoryNotesManager.getInstance().deleteAllBookmarks();		
				searchArray.clear();
				textArray.clear();
				listPoss.clear();
				listStr.clear();
				// adapter.notifyDataSetChanged();
				setAdapter();
	            return true;
	        default:
	            return false;
	    }
	}

	private String labelForElement(BookElement elem) {
		// String lawName = "";
		String title = elem.getTitle();
		if (title != null && !title.trim().equalsIgnoreCase("")) {
			return title;
		}
		StringBuffer retStr = new StringBuffer();
		if (elem.getChapter() != null
				&& !elem.getChapter().trim().equalsIgnoreCase(""))
			retStr.append(elem.getChapter()).append(" kap. ");
		if (elem.getParagraph() != null
				&& !elem.getParagraph().trim().equalsIgnoreCase(""))
			retStr.append(elem.getParagraph()).append(" § ");
		return retStr.toString();
	}

	protected void initSplashVideo(String name) {
		int res = this.getResources().getIdentifier(name, "raw",
				getPackageName());
		if (!playFileRes(res))
			return;
	}

	private boolean playFileRes(int fileRes) {
		if (fileRes == 0) {
			stopPlaying();
			return false;
		} else {
			mVV.setVideoURI(Uri.parse("android.resource://" + getPackageName()
					+ "/" + fileRes));

			return true;
		}
	}

	private void setView() {
		
		if (mSearchTimer != null) {
			mSearchTimer.cancel();
		}
		mSearchTimer = new CountDownTimer(100, 100) {
			public void onTick(long millisUntilFinished) {/* do nothing */
			}

			public void onFinish() {
				if (task != null
						&& task.getStatus().equals(AsyncTask.Status.RUNNING)) {
					task.cancel(true);
				}

				if (searchArray.size() == 0) {
					task = new TaskGetList();
					task.execute("");
				}

				// //Log.e(LOG_TAG, "Done timer starting search with: " +
				// filterText.getText().toString());
				mSearchTimer = null;
			}
		}.start();
	}

	public void stopPlaying() {
		mVV.stopPlayback();
	}

	@Override
	public void onPrepared(MediaPlayer mp) {
		// TODO Auto-generated method stub
		mp.setLooping(false);
	}

	@Override
	public void onCompletion(MediaPlayer arg0) {
		// TODO Auto-generated method stub
		if(VIDEO_STATE == VIDEO_P2_IS_PLAYING){
			Intent intent = new Intent(BookmarkActivity.this, Pages.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			intent.putExtra(BookElement.BOOK_ID, book.ID);
			startActivity(intent);
			overridePendingTransition (0, 0);
			extraInt = 0;
			return;			
		} else if (VIDEO_STATE == VIDEO_P3_IS_PLAYING) {
			mVV.setVisibility(View.GONE);
			mListWrapper.setAlpha(1);
			mListWrapper.setVisibility(View.VISIBLE);
			setView();
			return;
		}
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		if (searchArray != null && searchArray.size() > 0){
			book = searchArray.get(arg2);
			if (book.ID > 0) {
				mListWrapper.animate().alpha(0f).setDuration(50);
				mVV.setVisibility(View.VISIBLE);
				int res = BookmarkActivity.this.getResources().getIdentifier("join2", "raw", getPackageName());
				mVV.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" + res));
				mVV.start();
				this.VIDEO_STATE = VIDEO_P2_IS_PLAYING;	
			}
		}
	}
	

public class BookmarkAdapter extends BaseAdapter {
	List<String> result;
	Context context;
	List<Integer> listPos;
	List<String> headerT;

	public BookmarkAdapter(Activity activity) {
		result = listStr;
		context = activity;
		this.listPos = listPoss;
		headerT = listText;
		
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return result.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

	public class Holder {
		TextView tvOrder;
		SeekBar bookmarkBar;
		Integer pos;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		Holder holder = new Holder();
		if (convertView == null) {
			
			LayoutInflater inflater = ((Activity) context).getLayoutInflater();
			convertView = inflater.inflate(R.layout.list_item, null);
			holder.tvOrder = (TextView) convertView.findViewById(R.id.twOrder);
			holder.bookmarkBar = (SeekBar) convertView.findViewById(R.id.bookmarkBar);
			bookmarkHeader = (TextView) convertView.findViewById(R.id.twOrder);
			
			convertView.setTag(holder);

		} else {
			holder = (Holder) convertView.getTag();
		}
		holder.pos = position;
		holder.tvOrder.setText(result.get(position));
		Integer listPosition = listPos.get(position);
		if (listPosition >= 0) { 
			holder.bookmarkBar.setProgress(listPosition);
			holder.bookmarkBar.setEnabled(false);
		} else {
			holder.bookmarkBar.setVisibility(View.GONE);
			convertView.setEnabled(false);
			convertView.setOnClickListener(null);
		}
		Typeface font1 = Typeface.createFromAsset(context.getAssets(), "html/font/MyriadPro-Regular.otf");
		holder.tvOrder.setTypeface(font1);
		return convertView;
	}
	
	@Override
    public void registerDataSetObserver(DataSetObserver observer) {
        observers.add(observer);
    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver observer) {

    }
	
	public void notifyDataSetChanged(){
        for (DataSetObserver observer: observers) {
            observer.onChanged();
        }
    }

}

}
