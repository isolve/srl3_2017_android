package se.isolve.srl3;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.util.AttributeSet;
//import android.util.Log;
import android.view.Display;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;

public class MySurfaceView extends SurfaceView implements Runnable {

	SurfaceHolder ourHolder;
	Thread ourThread = null;
	boolean isRunning = false;
	Bitmap pageCrack;
	Bitmap background;
	float x, y, oldX, oldY;;
	PlayVideoHandler playHandler;
	boolean isStart = false, isResume = false;
	// Display display;
	float displayWidth, displayHeight;
	boolean isTransparent = false;

	public MySurfaceView(Context context, AttributeSet attrs) {
		super(context, attrs);
		ourHolder = getHolder();
		ourHolder.setFormat(PixelFormat.TRANSPARENT);
		x = 0;
		y = 0;
		WindowManager wm = (WindowManager) this.getContext().getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		displayWidth = size.x;
		displayHeight = size.y;
		pageCrack = BitmapFactory.decodeResource(getResources(), R.drawable.pagecrack);
		//test = Bitmap.createScaledBitmap(test, test.getWidth() / 3, (int) (test.getHeight() * 1.9f), true);
		pageCrack = Bitmap.createScaledBitmap(pageCrack, (int)(displayWidth / 60), (int)(displayHeight / 2), true);
		background = BitmapFactory.decodeResource(getResources(), R.drawable.firstimage);
		setZOrderOnTop(true);
		
		isStart = true;
	}

	public void updateXY(float x, float y) {
		this.x = x;
		this.y = y;
	}

	public void clearSurface() {
		if (x != 0 && y != 0 && ourHolder != null) {
			Canvas canvas = ourHolder.lockCanvas();
			if (canvas == null)
				return;
			canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
			isTransparent = true;
			//Log.i("Surface transparent", "trangspatent");
			ourHolder.unlockCanvasAndPost(canvas);
		}
	}

	public void pause() {
		isRunning = false;
		while (true) {
			try {
				ourThread.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			break;
		}
		ourThread = null;
	}

	public void resume() {
		isRunning = true;
		ourThread = new Thread(this);
		ourThread.start();
		isStart = false;
		isResume = true;
	}
	
	public void start() {
		isRunning = true;
		ourThread = new Thread(this);
		ourThread.start();
		isStart = true;
		isResume = false;
	}

	public void stop() {
		this.clearSurface();
		ourThread.interrupt();
	}

	@Override
	public void run() {
		while (isRunning) {
			if (!ourHolder.getSurface().isValid()) {
				continue;
			}

			Canvas canvas = null;

			try {
				canvas = ourHolder.lockCanvas();
				if (x == -1.0 && y == -1.0) {
					canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
					//Log.i("Xoa surface", "x = " + x);
					return;
				}
				synchronized (ourHolder) {

					if (isStart) {
						//Log.i("Isolve", "isStart : " + isStart);
						playHandler.playVideo(0);
						setVisibility(View.VISIBLE);
						isStart = false;
						isResume = false;
					} 
					
					if (isResume) {
						playHandler.handlerResumed();
						setVisibility(View.VISIBLE);
						isResume = false;
						isStart = false;
					}

					if (!isStart) {

						canvas.drawColor(Color.TRANSPARENT,
								PorterDuff.Mode.CLEAR);
						if (x != 0 && y != 0) {
							// //Log.i("Isolve drawCanvas 1", "x = " + x + " y=" +
							// y);
							if (isTransparent) {
								canvas.drawColor(Color.TRANSPARENT,
										PorterDuff.Mode.CLEAR);
							} else {
								// if (x > 620) {
								// x = 620;
								// }
								// if (x < 100) {
								// x = 100;
								// }
								if (x == -1.0 || y == -1.0) {
									canvas.drawColor(Color.TRANSPARENT,
											PorterDuff.Mode.CLEAR);
									return;
								} else {
									canvas.drawBitmap(pageCrack, x - (pageCrack.getWidth() / 2), displayHeight / 3, null);
								}
							}
						}
					}

					// if(x != 0 && y != 0 && oldX != x){
					// oldX = x;
					// float maxX = x-(test.getWidth()/2);
					// int screenWidth = display.getWidth();
					//
					// int peeds = screenWidth/test.getWidth();
					//
					// if(maxX > (peeds-2)*test.getWidth()){
					// maxX = (peeds-2)*test.getWidth();
					// }
					// canvas.drawColor(Color.TRANSPARENT,
					// PorterDuff.Mode.CLEAR);
					// canvas.drawBitmap(test, maxX, display.getHeight()/4,
					// null);
					// ourHolder.unlockCanvasAndPost(canvas);
					// }

					// }
				}
			} finally {
				if (ourHolder != null) {
					ourHolder.unlockCanvasAndPost(canvas);
				}
			}

		}
	}
	
	public void drawBitmap (double xPos) {
		if (ourHolder != null) {
			Canvas canvas = ourHolder.lockCanvas();
			try {
				//Log.d("MSV", "draw bitmap at position - "+xPos);
				if (canvas != null) {
					canvas.drawBitmap(pageCrack, (float)(xPos - (pageCrack.getWidth() / 2)), displayHeight / 3, null);
				}
			} finally {
				if (ourHolder != null) {
					ourHolder.unlockCanvasAndPost(canvas);
				}
			}
		}
	}

	public void setIsTransparent(boolean b) {
		this.isTransparent = b;
	}

}
