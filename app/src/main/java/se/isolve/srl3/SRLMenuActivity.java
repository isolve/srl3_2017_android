package se.isolve.srl3;
import android.app.Activity;
import android.content.Intent;
import android.view.KeyEvent;


public class SRLMenuActivity extends Activity {

	@Override
	public boolean onKeyUp(int keyCode, KeyEvent msg) {

	     switch(keyCode) {
	     case(KeyEvent.KEYCODE_BACK):
	    	handleNavBack();
			return true;    
	     }
	     return false;
	}
	
	protected void handleNavBack () {
		if (this.isTaskRoot()) {
			// If list-guide is at the root of the activity stack, then open the Chooser page screen.
			openBladdring();
		} else {
			finish();
			overridePendingTransition(0, 0);
		}
	}
	
	private void openBladdring () {
		Intent intent = new Intent(this, ChooserPageScreen.class);
		startActivity(intent);
		overridePendingTransition(0, 0);
		finish();
	}
	
}
