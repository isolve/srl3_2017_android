package se.isolve.srl3;

import se.isolve.srl3.reader.AutoFitTextView;
import utils.LayoutUtils;
import android.content.Context;
import android.graphics.Point;
import android.util.AttributeSet;
import android.view.Display;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.RelativeLayout;

public class IndexView extends RelativeLayout {

	private static char alphabets[] = {'a','b','c','d','e','f','g','h'
		,'i','j','k','l','m','n','o','p'
		,'r','s','t','u','v','y','z','å', 'ä', 'ö'}; // no q, w or x
	private static int VIEW_WIDTH = 45;
	private static int TAV_WIDTH = 180;
	private static int TAV_HEIGHT = 60;
	public static final double INDEX_WIDTH_PERCENT = 7.0;
	
	private static String TAG = "INDEX_VIEW";
 	
	private int indexViewHeight, height, width;
	private RelativeLayout containerView;
	private AutoFitTextView touchedAlphabetView;
	private Context mContext;
	
	IndexViewTouchEventListener touchEventListener;
	
	public interface IndexViewTouchEventListener {
		//public void indexViewTouched(int index);
		public void indexViewTouchesEnded(int index);
	}
	
	public IndexView (Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		init(context);
		mContext = context;
	}

	public IndexView (Context context, AttributeSet attrs) {
		this(context, attrs, 0);
		init(context);
		
		mContext = context;
	}

	public IndexView (Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context);
		
		mContext = context;
	}
	
	private void init (Context context) {
		
		Display display = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		height = size.y;
		width = size.x;
		double f = 0.189;
		int h = (int)(height * f);
		int indexTop = h + (int) getResources().getDimension(R.dimen.edittext_search_heigh);
		indexViewHeight = height - indexTop;
		int tvHeight = indexViewHeight/alphabets.length;
		// Add all the alphabets using TextViews.
		for (int i = 0; i<alphabets.length; i++) {
			AutoFitTextView tv = new AutoFitTextView(context);
			tv.setMaxTextSize(25);
			tv.setMinTextSize(15);
			tv.setText(String.valueOf(alphabets[i]).toUpperCase());
			tv.setGravity(Gravity.CENTER);
			tv.setTextColor(context.getResources().getColor(R.color.white));
			LayoutUtils.setLayout(tv, 2, (i*tvHeight), tvHeight, (int)((float)width*(INDEX_WIDTH_PERCENT/100.0)));
			this.addView(tv);
		}
	}
	
	public void setIndexViewTouchEventListener (IndexViewTouchEventListener listener) {
		this.touchEventListener = listener;
	}
	
	public void showTouchedAlphabetInContainerView (RelativeLayout view) {
		this.containerView = view;
		
		if (view != null) {
			touchedAlphabetView = new AutoFitTextView(mContext);
			touchedAlphabetView.setText("TEST");
			touchedAlphabetView.setMaxTextSize(30);
			touchedAlphabetView.setMinTextSize(25);
			touchedAlphabetView.setGravity(Gravity.LEFT | Gravity.CENTER);
			touchedAlphabetView.setTextColor(mContext.getResources().getColor(R.color.white));
			touchedAlphabetView.setBackgroundColor(mContext.getResources().getColor(R.color.black_transparent));
			int w = view.getWidth();
			if (w <= 0) {
				w = width;
			}
			LayoutUtils.setLayout(touchedAlphabetView, w - VIEW_WIDTH - TAV_WIDTH - 20, 0, TAV_HEIGHT, TAV_WIDTH);
			containerView.addView(touchedAlphabetView);
			
			touchedAlphabetView.setVisibility(View.INVISIBLE);
		}
	}
	
	private void displayTouchedCharacter (String character, int y) {
		if (touchedAlphabetView != null) {
			touchedAlphabetView.setVisibility(View.VISIBLE);
			touchedAlphabetView.setText("  "+character);
			
			LayoutUtils.setLayout(touchedAlphabetView, -1, y - (int)(TAV_HEIGHT/2.0) + this.getTop(), TAV_HEIGHT, TAV_WIDTH);
		}
	}
	
	private void hideTouchedAlphabetDisplay () {
		touchedAlphabetView.setVisibility(View.INVISIBLE);
	}
	
	@Override
	public boolean onTouchEvent (MotionEvent event) {
		super.onTouchEvent(event);
		
		float y = event.getY();
		
		if (event.getY() > 0 && event.getY() < indexViewHeight) {
			int tvHeight = indexViewHeight/alphabets.length;
			int touchedIndex = (int) (event.getY()/tvHeight)%alphabets.length;
			String charTouched = String.valueOf(alphabets[touchedIndex]).toUpperCase();
			
			switch (event.getAction()) {
				case MotionEvent.ACTION_DOWN: 
					
					//this.touchEventListener.indexViewTouched(touchedIndex);
					this.displayTouchedCharacter(charTouched, (int)y);
//					Log.d(TAG, "Action down. Y - "+event.getY()+" index - "+touchedIndex+", char - "+charTouched);
					
					break;
				
				case MotionEvent.ACTION_MOVE:
					
					//this.touchEventListener.indexViewTouched(touchedIndex);
					this.displayTouchedCharacter(charTouched, (int)y);
//					Log.d(TAG, "Action move. Y - "+event.getY()+" index - "+touchedIndex+", char - "+charTouched);
					
					break;
					
				case MotionEvent.ACTION_UP:
					
					this.touchEventListener.indexViewTouchesEnded(touchedIndex);
					this.hideTouchedAlphabetDisplay();
					
					break;
			}
		} else {
			this.hideTouchedAlphabetDisplay();
		}
		
		return true;
	}
}
