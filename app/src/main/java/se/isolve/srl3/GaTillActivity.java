package se.isolve.srl3;

import java.util.ArrayList;

import se.isolve.srl3.adapter.GaTillAdapter;
import se.isolve.srl3.database.BookDB;
import se.isolve.srl3.database.BookElement;
import se.isolve.srl3.reader.Pages;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Point;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.VideoView;

public class GaTillActivity extends SRLMenuActivity  implements OnCompletionListener,
OnPreparedListener, OnItemClickListener{
	private EditText etGaTill;
	private ListView lvGaTill;
	private CountDownTimer mSearchTimer;

	private ArrayList<String> listStr;
	private ArrayList<Integer> listPoss;

	private BookDB mDbHelper;
	private LinearLayout rl1;
	private  LinearLayout mListLayout;
	private ImageView imgGaTill, searchIcon;
	private int width = 0;
	private int height = 0;
	private ProgressBar gtProgress;
	
	private VideoView mVV;
	private LinearLayout llgatil2;
	private String NameFromList = "list";
	private int extraInt;
	ArrayList<BookElement> arrBooks;
	GaTillAdapter adapter;
	private int VIDEO_STATE = 0;
	private static final int VIDEO_P2_IS_PLAYING = 4;
	private static final int VIDEO_P3_IS_PLAYING = 2;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.gatill);
		Display display = getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		width = size.x;
		height = size.y;
		rl1 = (LinearLayout)findViewById(R.id.rlh1);
		double f = 0.2; // 0.189;
		int h = (int)(height * f);
		rl1.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,h));
		mListLayout = (LinearLayout)findViewById(R.id.list_layout);
		double widthMarginRatio = 0.14;
		int w = (int) (width * widthMarginRatio); // getResources().getDimension(R.dimen.marginbook);
		LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(width - w, LayoutParams.MATCH_PARENT);
		layoutParams.gravity = Gravity.CENTER;
		mListLayout.setLayoutParams(layoutParams);
		etGaTill = (EditText) findViewById(R.id.search_keyword);
		lvGaTill = (ListView) findViewById(R.id.lvGatill);
		gtProgress = (ProgressBar) findViewById(R.id.gt_progress);

		//etGaTill.addTextChangedListener(filterTextWatcher);



		etGaTill.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {

			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				if (s.length() <= 0) {
					lvGaTill.setAdapter(null);
					return;
				}

				//If timer is still active search is not performed
				if (mSearchTimer != null) {
					mSearchTimer.cancel();

					// //Log.e(LOG_TAG, "Timer was cancelled");
				}
				mSearchTimer = new CountDownTimer(400, 400) {
					public void onTick(long millisUntilFinished) {/* do nothing */
						//Nothing needs to be done since its just counting down
					}

					public void onFinish() {

						Handler searchThread = new Handler();
						searchThread.post(new Runnable() {
							@Override
							public void run() {
								showProgress();
								String wordToSearch = etGaTill.getText().toString().trim();
								arrBooks = performSearch(wordToSearch);
								listStr = new ArrayList<String>();
								listPoss = new ArrayList<Integer>();

								if (arrBooks == null) {
									return;
								}
								for (BookElement e : arrBooks) {
									listStr.add(labelForElement(e));
									float pos = (float) mDbHelper.positionForElement(e);
									int indexPosX = (int) (pos * 1000);
									listPoss.add(indexPosX);
								}


								if (arrBooks == null) {
									lvGaTill.setAdapter(null);
									return;
								}


									adapter = new GaTillAdapter(GaTillActivity.this, listPoss, listStr);
									adapter.notifyDataSetChanged();

								lvGaTill.setAdapter(adapter);
								if (!lvGaTill.hasOnClickListeners()) {
									lvGaTill.setOnItemClickListener(GaTillActivity.this);
								}
								hideProgress();
							}
						});
						mSearchTimer = null;
					}
				}.start();
			}

			@Override
			public void afterTextChanged(Editable s) {

			}
		});
		etGaTill.setTypeface(SRL3ApplicationClass.getMyriadPro());

		//Fredrik Sebek - 5 Jan 2016

		imgGaTill = (ImageView)findViewById(R.id.imgGaTill);
		searchIcon = (ImageView)findViewById(R.id.search_cancel);
		imgGaTill.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(GaTillActivity.this, ListGuide.class);
				startActivity(intent);
				overridePendingTransition(R.anim.your_left_to_right, R.anim.your_right_to_left);
//				GaTillActivity.this.finish();
			}
		});
		llgatil2 = (LinearLayout)findViewById(R.id.llgatill2);
		mVV = (VideoView) findViewById(R.id.myvideoview);
		mVV.setOnCompletionListener(this);
		mVV.setOnPreparedListener(this);
//		Resources resources = this.getResources();
//	    DisplayMetrics metrics = resources.getDisplayMetrics();
//	    float dp = 142 / (metrics.densityDpi / 160f);
		extraInt = getIntent().getIntExtra(NameFromList, 0);
		if(extraInt == 1){
			initSplashVideo("join3");
			VIDEO_STATE = VIDEO_P3_IS_PLAYING;
		}else{
			mVV.setVisibility(View.GONE);
			llgatil2.setVisibility(View.VISIBLE);
		}

		int time = 100;

		if (extraInt == 1) {
			time = 1500;
		}
		Handler go = new Handler();
		go.postDelayed(new Runnable() {
			@Override
			public void run() {
				try {
					etGaTill.requestFocus();
					InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.showSoftInput(etGaTill, InputMethodManager.SHOW_IMPLICIT);
				} catch (Exception e) {
					Log.d("GATillActivity", "Unable to request keyboard focus");
				}
			}
		}, time);




	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		mDbHelper = new BookDB(this);
		mDbHelper.createDatabase();
		if(extraInt == 1){
			new Thread(){
				public void run() {
					extraInt = 0;
					mVV.start();
				};
			}.start();
		} else {
			initSplashVideo("join3");
			mVV.start();
			VIDEO_STATE = VIDEO_P3_IS_PLAYING;
		}
	}
	
	private void showProgress () {
//		dia//Log.show();
		this.gtProgress.setVisibility(View.VISIBLE);
		this.gtProgress.animate();
		
		// Hide the search icon.
		this.searchIcon.setVisibility(View.INVISIBLE);
	}
	
	private void hideProgress () {
//		dia//Log.dismiss();
		this.gtProgress.clearAnimation();
		this.gtProgress.setVisibility(View.INVISIBLE);
		
		// Show the search icon.
		this.searchIcon.setVisibility(View.VISIBLE);
	}
	
	private TextWatcher filterTextWatcher = new TextWatcher() {

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			// TODO Auto-generated method stub

		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			// TODO Auto-generated method stub

		}

		@Override
		public void afterTextChanged(Editable s) {
			// TODO Auto-generated method stub

		}
	};


	public ArrayList<BookElement> performSearch(String searchedString) {
		String[] arr = searchedString.split(" ");
		String[] alpha_categories = { "a", "b", "c", "d", "e", "f", "g", "h",
				"i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t",
				"u", "v", "w", "x", "y", "z" };
		int i = 0;

		if (i < arr.length && arr[i].equalsIgnoreCase("SFS")) {
			i++;
		}

		String law = "";
		if (i < arr.length) {
			law = arr[i++];
		}
		String chapter = "";
		if (i < arr.length) {
			chapter = arr[i++];
		}

		if (i < arr.length) {
			for (int j = 0; j < alpha_categories.length; j++) {
				if (alpha_categories[j].equalsIgnoreCase(arr[i])) {
					chapter += " " + arr[i];
					i++;
					break;
				}
			}
		}

		// Check for "kap."
		if (i < arr.length && arr[i].toLowerCase().contains("kap")) {
			i++;
		}

		// Get paragraph
		String paragraph = "";
		if (i < arr.length) {
			paragraph = arr[i];
			i++;
		}

		// Check for paragraph category (ie a-z)
		if (i < arr.length) {
			for (int j = 0; j < alpha_categories.length; j++) {
				if (alpha_categories[j].equalsIgnoreCase(arr[i])) {
					paragraph += " " + arr[i];
					i++;
					break;
				}
			}
		}
		//Log.i("ARRBOOKS", "" + law + chapter+
//				paragraph);
		ArrayList<BookElement> arrBooks = mDbHelper.searchLaw(law, chapter,
				paragraph);
		
//		if (arrBooks != null && (arrBooks.size() > 0)) {
//			ArrayList<BookElement> temp = new ArrayList<BookElement>();
//			for (BookElement e : arrBooks) {
//				if (Integer.toString(e.ID).equals(e.chapter)) {
//					// The chapter and ID are the same.
//					temp.add(this.mDbHelper.elementWithID(e.ID));
//				}
//			}
//			arrBooks.clear();
//			arrBooks = temp;
//		}
		return arrBooks;
	}

	public String labelForElement(BookElement elem) {
		if (elem == null)
			return null;
		String title = mDbHelper.lawSFSForElement(elem);
		return title;
	}
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		mDbHelper.close();
	}
	
	protected void initSplashVideo(String name) {
		int res = this.getResources().getIdentifier(name, "raw",
				getPackageName());
		if (!playFileRes(res))
			return;
	}

	private boolean playFileRes(int fileRes) {
		if (fileRes == 0) {
			stopPlaying();
			return false;
		} else {
			mVV.setVideoURI(Uri.parse("android.resource://" + getPackageName()
					+ "/" + fileRes));
			return true;
		}
	}

	public void stopPlaying() {
		mVV.stopPlayback();
	}
	
	@Override
	public void onPrepared(MediaPlayer mp) {
		// TODO Auto-generated method stub
		mp.setLooping(false);
	}
	private BookElement book;
	@Override
	public void onCompletion(MediaPlayer arg0) {
		// TODO Auto-generated method stub
		if(VIDEO_STATE == VIDEO_P2_IS_PLAYING){
			Intent intent = new Intent(GaTillActivity.this, Pages.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			intent.putExtra(BookElement.BOOK_ID, book.ID);
			startActivity(intent);
			overridePendingTransition (0, 0);
			extraInt = 0;
			return;			
		} else if (VIDEO_STATE == VIDEO_P3_IS_PLAYING) {
			mVV.setVisibility(View.GONE);
			llgatil2.setAlpha(1);
			llgatil2.setVisibility(View.VISIBLE);
			return;
		}
	}
		
	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		// TODO Auto-generated method stub
		if(arrBooks != null && arrBooks.size() > 0){
			InputMethodManager mgr = (InputMethodManager) this
					.getSystemService(Context.INPUT_METHOD_SERVICE);
			mgr.hideSoftInputFromWindow(etGaTill.getWindowToken(), 0);
			book = arrBooks.get(arg2);
//			Intent intent = new Intent(GaTillActivity.this, Pages.class);
//			startActivity(intent);
//			GaTillActivity.this.finish();
//			llgatil2.setVisibility(View.GONE);
			llgatil2.animate().alpha(0f).setDuration(50);
			mVV.setVisibility(View.VISIBLE);
			int res = GaTillActivity.this.getResources().getIdentifier("join2", "raw",
					getPackageName());
			mVV.setVideoURI(Uri.parse("android.resource://" + getPackageName()
					+ "/" + res));
			mVV.start();
			this.VIDEO_STATE = VIDEO_P2_IS_PLAYING;
			
		}
	}
	
}
