package se.isolve.srl3.database;

import java.io.Serializable;

public class BookElement implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static String BOOK_ID = "_id";
	public static String BOOK_ABBR_ID = "ID";
	public static String BOOK_ABBR = "abbr";
	public static String BOOK_SHORT = "short";
	public static String BOOK_ARTICLE = "article";
	public static String BOOK_WEIGHT = "weight";
	public static String BOOK_TYPE = "type";
	public static String BOOK_STYLE = "style";
	public static String BOOK_LAW = "law";
	public static String BOOK_CHAPTER = "chapter";
	public static String BOOK_PARAGRAPH = "paragraph";
	public static String BOOK_HTML = "html";
	public static String BOOK_TEXT = "text";
	public static String BOOK_TITLE = "sfs";
	
	public int ID = 0, article = 0, weight = 0, type = 0, style = 0, law = 0;
	public String chapter = null, paragraph = null, html = null, text = null, title = null;
	
	public BookElement(){
		
	}
	
	public BookElement(int id, int article, int weight, int type, int style, String chapter, String html, String text, String title){
		
	}

	public int getId() {
		return ID;
	}

	public void setId(int id) {
		this.ID = id;
	}

	public int getArticle() {
		return article;
	}

	public void setArticle(int article) {
		this.article = article;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getStyle() {
		return style;
	}

	public void setStyle(int style) {
		this.style = style;
	}

	public int getLaw() {
		return law;
	}

	public void setLaw(int law) {
		this.law = law;
	}

	public String getChapter() {
		return chapter;
	}

	public void setChapter(String chapter) {
		this.chapter = chapter;
	}

	public String getHtml() {
		return html;
	}

	public void setHtml(String html) {
		this.html = html;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	public String getParagraph() {
		return paragraph;
	}

	public void setParagraph(String paragraph) {
		this.paragraph = paragraph;
	}
}
