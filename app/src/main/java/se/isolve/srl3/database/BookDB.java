package se.isolve.srl3.database;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
//import android.util.Log;

public class BookDB extends SQLiteOpenHelper{
	
	private static final String DATABASE_NAME = "srl3.sqlite";
	private static final String TABLE_NAME = "srl3";
	private static final String TABLE_ARTICLES = "articles";
	private static final String TABLE_LAWS = "laws";
	private static final String TABLE_ABBR = "abbr";
	private static final String DATABASE_PATH = "data/data/se.isolve.srl3_2018/databases/";
	private static final int DATABASE_VERSION = 1;
	private static final int ELEMENTS_PER_BATCH = 60;
	private static final int SEARCH_LAW_LIMIT = 99; // PWA: 2015-08-02 Changed from 20 to 99
	private static final int START_ID_OF_BIHANG = 63401; // PWA: 2017-12-19 Updated
	private static final double START_POS_OF_BIHANG = 0.5; 
	private static final String DB_VERSION = "dbversion";
	private static final int CURRENT_DB_VERSION = 1800;
	
	public static final int NUMBER_OF_ARTICLES = 181; // PWA: TODO: Make this dynamic.
	
	public SQLiteDatabase database;
	public ArrayList<BookElement> articleArray = new ArrayList<BookElement>(),
			lawArray = new ArrayList<BookElement>();
	
	private Context myContext;
	
	public BookDB(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		// TODO Auto-generated constructor stub
		this.myContext = context;
	}

	public void createDatabase () {
		createDB();
	}
	
	private void createDB () {
		Log.d("BOOKDB", "Gonna check if database exists");
		boolean dbExists = DBExists();
		if (!dbExists || !checkDBVersion()) {
			Log.d("BOOKDB", "Gonna open database");
			this.getReadableDatabase();
			copyDBFromResource();
		}
		
		//Toast t = Toast.makeText(myContext, "Database created!", Toast.LENGTH_SHORT);
		//t.show();
	}
	
	private boolean checkDBVersion() {
		SharedPreferences sharedPrefs = this.myContext.getSharedPreferences(DB_VERSION, Context.MODE_PRIVATE);
		return CURRENT_DB_VERSION == sharedPrefs.getInt(DB_VERSION, 0);
	}
	
	private void setDBVersion() {
		SharedPreferences sharedPrefs = this.myContext.getSharedPreferences(DB_VERSION, Context.MODE_PRIVATE);
		SharedPreferences.Editor sharedPrefsEdit = sharedPrefs.edit();
		sharedPrefsEdit.putInt(DB_VERSION, CURRENT_DB_VERSION);
		sharedPrefsEdit.commit();
	}
	
	private void copyDBFromResource () {
		InputStream inputStream = null;
		OutputStream outputStream = null;
		String dbFilePath = DATABASE_PATH + DATABASE_NAME;
		
		try {
			inputStream = myContext.getAssets().open(DATABASE_NAME);
			outputStream = new FileOutputStream(dbFilePath);
			byte[] buffer = new byte[1024];
			
			int length;
			
			while ((length = inputStream.read(buffer)) > 0) {
				outputStream.write(buffer, 0, length);
			}
			
			outputStream.flush();
			outputStream.close();
			inputStream.close();

			setDBVersion();
			
		} catch (IOException e) {
			throw new Error ("Problem copying database from resource file."); 
		}
	}
	
	private boolean DBExists () {
		SQLiteDatabase db = null; 
		
		try {
			String dbPath = DATABASE_PATH + DATABASE_NAME;
			db = SQLiteDatabase.openDatabase(dbPath, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS | SQLiteDatabase.CREATE_IF_NECESSARY 
					| SQLiteDatabase.OPEN_READWRITE);
//			db.setLocale(Locale.getDefault());
			db.setVersion(DATABASE_VERSION);
		} catch (SQLiteException e) {
			////Log.e("BookDB", "Database not found");
		}
		
		if (db != null) {
			db.close();
		}
		
		return db != null ? true:false;
	}
	
	@Override
	public void onCreate(SQLiteDatabase arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}
	
	public ArrayList<BookElement> getArticles () {
		if (articleArray.isEmpty()) {
			String sql = "select _id, article, weight, type, style, law, chapter, paragraph, html, text, sfs from "+TABLE_NAME+" where type=1 and weight in (1,2,5) order by _id asc";
			articleArray = bookElementsWithQuery(sql, null);
		}
		return articleArray;
	}
	
	public ArrayList<BookElement> getLaws () {
	    if (lawArray.isEmpty()) {
	        String sql = "select _id, article, weight, type, style, law, chapter, paragraph, html, text, sfs from "+TABLE_NAME+" where type=1 order by _id asc";
	        lawArray = bookElementsWithQuery(sql, null);
	    }
		return lawArray;
	}

	private synchronized  int integerWithQuery (String sql, String[] args) {
		if (this.getReadableDatabase() == null) {
			return 0;
		}
		int result = 0;
		Cursor cursor = null;
		try {
			cursor = this.getReadableDatabase().rawQuery(sql, args);
			if (cursor.moveToFirst()) {
				result = cursor.getInt(0);
			}
		} finally {
			if (cursor != null)
				cursor.close();
		}
		return result;
	}
	
	private synchronized String stringWithQuery (String sql, String[] args) {
		if (this.getReadableDatabase() == null) { 
			return "";
		}
		String str = null;
		Cursor cursor = null;
		try {
			cursor = this.getReadableDatabase().rawQuery(sql, args);     
			if (cursor.moveToFirst()) {
				str = cursor.getString(0);                        
			}
		} finally {
			if (cursor != null)
		        cursor.close();
		}
	    return str;
	}
	
	public synchronized int getElementsInDatabase () {
				
		int id = 0;  
		
		Cursor cursor = null;
		try {
		
			cursor = this.getReadableDatabase().rawQuery("select max(_id) from "+TABLE_NAME, null);
			
	    if (cursor.moveToFirst())
	    {
	        do
	        {           
	            id = cursor.getInt(0);                  
	        } while(cursor.moveToNext());           
	    }
	    
		} finally {
			if(cursor != null)
		        cursor.close();
		}
		
		return id;
	}
	
	public synchronized int getArticlesInDatabase () {
				
		int id = 0;
		
		Cursor cursor = null;
		try {
		
			cursor = this.getReadableDatabase().rawQuery("select max(article) from "+TABLE_NAME, null);
			
	    if (cursor.moveToFirst())
	    {
	        do
	        {           
	            id = cursor.getInt(0);                  
	        } while(cursor.moveToNext());           
	    }
		
	} finally {
		if(cursor != null)
	        cursor.close();
	}
	    
		return id;
	}
	
	private ArrayList<BookElement> bookElementsWithQuery (String query, String[] args) {
		return this.bookElementsWithQuery(query, args, "", null, null);
	}
	
	private synchronized ArrayList<BookElement> bookElementsWithQuery (String query, String[] args, String tableName, String searchColumn) {
		return this.bookElementsWithQuery(query, args, tableName, searchColumn, null);
	}
	
	private synchronized ArrayList<BookElement> bookElementsWithQuery (String query, String[] args, String lawName) {
		return this.bookElementsWithQuery(query, args, "", null, lawName);
	}
	
	private synchronized ArrayList<BookElement> bookElementsWithQuery (String query, String[] args, String tableName, String searchColumn, String lawName) {
		
		// public int ID = 0, article = 0, weight = 0, type = 0, style = 0, law = 0;
		// public String chapter = null, paragraph = null, html = null, text = null, title = null;
		
		ArrayList<BookElement> queryResults = new ArrayList<BookElement>();
		//////Log.d("DB", "Processing query - "+query+" with args - "+args.toString());
		
		if (tableName.equals(TABLE_ABBR)) {
			
			Cursor cursor = null;
			try {
				cursor = this.getReadableDatabase().rawQuery(query, args);
				
				if (cursor != null) {
					if (cursor.moveToFirst())
				    {
						////Log.d("DB", "Moved cursor to first");
				        do
				        {     
				        	
				        	// Initialize a new element to be added to the results array.
				        	BookElement bookElement = new BookElement();
				        	//////Log.d("DB", "Initialized book element");
				            bookElement.ID = cursor.getInt(getColumnIndex(cursor, BookElement.BOOK_ABBR_ID));
				            bookElement.article = 0;
				            bookElement.weight = 0;
				            bookElement.type = 1;
				            bookElement.style = 0;
				            bookElement.law = cursor.getInt(getColumnIndex(cursor, BookElement.BOOK_LAW));
				            bookElement.chapter = "";
				            bookElement.paragraph = "";
				            bookElement.html = "";
				            bookElement.text = cursor.getString(getColumnIndex(cursor, searchColumn));
				            bookElement.title = cursor.getString(getColumnIndex(cursor, searchColumn));
				            ////Log.d("DB", "Adding result with law - "+bookElement.law+", and ID - "+bookElement.ID);
				            queryResults.add(bookElement);
				        } while(cursor.moveToNext());           
				    }
				}
				
			} finally {
				if(cursor != null) {
					cursor.close();
					////Log.d("DB", "Cursor closed for query - "+query);
				}		   
			}
			
		} else {
			Cursor cursor = null;
			try {
				cursor = this.getReadableDatabase().rawQuery(query, args);
				
				if (cursor != null) {
					if (cursor.moveToFirst())
				    {
						////Log.d("DB", "Moved cursor to first");
				        do
				        {       
				        	// Initialize a new element to be added to the results array.
				        	BookElement bookElement = new BookElement();
				        	//////Log.d("DB", "Initialized book element");
				            bookElement.ID = cursor.getInt(getColumnIndex(cursor, BookElement.BOOK_ID));
				            bookElement.article = cursor.getInt(getColumnIndex(cursor, BookElement.BOOK_ARTICLE));
				            bookElement.weight = cursor.getInt(getColumnIndex(cursor, BookElement.BOOK_WEIGHT));
				            bookElement.type = cursor.getInt(getColumnIndex(cursor, BookElement.BOOK_TYPE));
				            bookElement.style = cursor.getInt(getColumnIndex(cursor, BookElement.BOOK_STYLE));
				            bookElement.law = cursor.getInt(getColumnIndex(cursor, BookElement.BOOK_LAW));
				            bookElement.chapter = cursor.getString(getColumnIndex(cursor, BookElement.BOOK_CHAPTER));
				            bookElement.paragraph = cursor.getString(getColumnIndex(cursor, BookElement.BOOK_PARAGRAPH));
				            String str = cursor.getString(getColumnIndex(cursor, BookElement.BOOK_HTML));
				            String cr = null;
				            String crc = null;
				            try {
								cr = crypt2("abcdefgh");
								crc = crypt2(cr);
							} catch (UnsupportedEncodingException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
				            
				            try {
								bookElement.html = crypt2(cursor.getString(getColumnIndex(cursor, BookElement.BOOK_HTML)));
							} catch (UnsupportedEncodingException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
				            				            
								
				            if (lawName != null) {
				            	bookElement.text = lawName;
					            bookElement.title = lawName;
				            } else {
				            	bookElement.text = cursor.getString(getColumnIndex(cursor, BookElement.BOOK_TEXT));
					            bookElement.title = cursor.getString(getColumnIndex(cursor, BookElement.BOOK_TITLE));
				            }
				            
				            ////Log.d("DB", "Adding result with law - "+bookElement.law+", and ID - "+bookElement.ID);
				            queryResults.add(bookElement);
				        } while(cursor.moveToNext());           
				    }
				}
				
			} finally {
				if(cursor != null) {
					cursor.close();
					////Log.d("DB", "Cursor closed for query - "+query);
				}		   
			}
		}

		return queryResults;
	}
	
	private int getColumnIndex (Cursor cursor, String columnName) {
		int index = cursor.getColumnIndex(columnName);
		
//		if (index < 0) {
//			////Log.e("DB", "index is < 0 for column - "+columnName);
//			index = 0;
//		}
		
		return index;
	}
	
	public synchronized BookElement elementWithID (int ID) {
		
		if (this.getReadableDatabase() == null) { 
			return null;
		}
		
		String query = "SELECT _id" +
				", article" +
				", weight" +
				", type" +
				", style" +
				", law" +
				", chapter" +
				", paragraph" +
				", html" +
				", text" +
				", sfs" +
				" FROM "+TABLE_NAME+" WHERE _id=?";
		ArrayList<BookElement> queryResults = bookElementsWithQuery(query, new String[] {String.valueOf(ID)});
		
		if (queryResults.isEmpty()) {
			////Log.d("DB", "Element result empty");
			return null;
		}
		return queryResults.get(0);
	}
	
	public synchronized int articleForID (int ID) {
		BookElement elementForGivenID = elementWithID(ID);
		if (elementForGivenID != null) {
			return elementForGivenID.article;
		}
		return 0;
	}
	
	public ArrayList<BookElement> elementsWithArticle (int article) {
		if (this.getReadableDatabase() == null) { 
			return null;
		}
		String query = "select _id, article, weight, type, style, law, chapter, paragraph, html, text, sfs from "+TABLE_NAME+" where article=? order by _id asc";
		String[] args = new String[] {String.valueOf(article)};
		return bookElementsWithQuery(query, args);
	}
	
	// "select _id, article, weight, type, style, law, chapter, paragraph, html, text, sfs from %@ where article=%d and _id between %d and %d order by _id asc"
	public ArrayList<BookElement> elementsWithArticleBetweenIDs (int article, int fromID, int toID) {
		if (this.getReadableDatabase() == null) { 
			return null;
		}
		String query = "select _id, article, weight, type, style, law, chapter, paragraph, html, text, sfs from "+TABLE_NAME+" where article=? and _id between ? and ? order by _id asc";
		String[] args = new String[] {String.valueOf(article), String.valueOf(fromID), String.valueOf(toID)};
		return bookElementsWithQuery(query, args);
	}
	
	// "select _id, article, weight, type, style, law, chapter, paragraph, html, text, sfs from %@ where article=%d order by _id asc limit %d"
	public ArrayList<BookElement> firstElementsOfArticle (int article) {
		if (this.getReadableDatabase() == null) { 
			return null;
		}
		String query = "select _id, article, weight, type, style, law, chapter, paragraph, html, text, sfs from "+TABLE_NAME+" where article=? order by _id asc limit ?";
		String[] args = new String[] {String.valueOf(article), String.valueOf(ELEMENTS_PER_BATCH)};
		return bookElementsWithQuery(query, args);
	}
	
	public ArrayList<BookElement> elementsWithArticleContainingID (int article, int ID) {
		if (this.getReadableDatabase() == null) { 
			return null;
		}
		
		int fromID = ID - ELEMENTS_PER_BATCH;
		if (ID < ELEMENTS_PER_BATCH) {
			fromID = 0;
		}
		
		int toID = ID + ELEMENTS_PER_BATCH;
		
		return elementsWithArticleBetweenIDs(article, fromID, toID);
	}
	
	public ArrayList<BookElement> elementsWithArticleStartingWithID (int article, int startID) {
		if (this.getReadableDatabase() == null) { 
			return null;
		}
		int fromID = startID;
		int toID = startID + ELEMENTS_PER_BATCH;
		return elementsWithArticleBetweenIDs(article, fromID, toID);
	}
	
	public String htmlForElementsWithArticle (int article) {
		ArrayList<BookElement> resultArray = elementsWithArticle(article);
		if (resultArray == null) {
			return null;
		}
		String html = "";
		BookElement elem = null;
		for (int i = 0; i < resultArray.size(); ++i) {
	        elem = resultArray.get(i);
	        html += elem.html;	   
	    }
		return html;
	}
	
	private String removeSFSFromLawName(String lawname) {
		String regex = " \\([0-9]{4}:[0-9]{1,4}\\)";
		String result = lawname.replaceAll(regex, "");
		return result;
	}
	
	public ArrayList<BookElement> getLawsInOrderOfSFS () {
		String sql = "select _id, article, weight, type, style, law, chapter, paragraph, html, text, sfs from "+TABLE_NAME+" where type=1 and sfs != ''";
		ArrayList<BookElement> array =  bookElementsWithQuery(sql, null);
		ArrayList<BookElement> result = new ArrayList<BookElement>();
		for (BookElement e: array) {
			e.text = e.title + ": " + removeSFSFromLawName(e.text);
			result.add(e);
		}
		Collections.sort(result, new Comparator<BookElement>() {
			public String padSFS(String sfs) {
			    String zerosPadding = ":";
			    switch (sfs.length()) {
			        case 6:
			            zerosPadding = ":000";
			            break;
			        case 7:
			            zerosPadding = ":00";
			            break;
			        case 8:
			            zerosPadding = ":0";
			            break;
			        default:
			            zerosPadding = ":";
			            break;
			    }
			    return sfs.replaceFirst(":", zerosPadding);
			}
			@Override
			public int compare(BookElement arg0, BookElement arg1) {
			    // Assumes that sfs number is stored in title property
			    // SFS = "NNNN:n" - "NNNN:nnnn"
			    // pad the n with 0 before so that they can be compared correctly
			    String arg0PaddedSFS = padSFS(arg0.title);
			    String arg1PaddedSFS = padSFS(arg1.title);
			    return arg0PaddedSFS.compareTo(arg1PaddedSFS);
			}
		});
		return result;
	}
	
	public ArrayList<BookElement> getLawsChaptersParagraphs () {
		String sql = "select _id, article, weight, type, style, law, chapter, paragraph, html, text, sfs from "+TABLE_NAME+" where type=1 or (weight in (1,2,5) and type in (3,6)) order by _id asc";
		return bookElementsWithQuery(sql, null);
	}
	
	public ArrayList<BookElement> getTOC () {
		String sql = "select _id, article, weight, type, style, law, chapter, paragraph, html, text, sfs from "+TABLE_NAME+" where type=1 and weight in (1,2,5) order by _id asc";
		return bookElementsWithQuery(sql, null);
	}
	
	public ArrayList<BookElement> getLawsChaptersParagraphsForArticle (int article) {
		String query = "select _id, article, weight, type, style, law, chapter, paragraph, html, text, sfs from "+TABLE_NAME+" where article=? and (type=1 or (weight in (1,2,5) and type in (3,6))) order by _id asc";
		String[] args = new String[] {String.valueOf(article)};
		return bookElementsWithQuery(query, args);
	}
	
	public ArrayList<BookElement> getLawsChaptersForArticle (int article) {
		String query = "select _id, article, weight, type, style, law, chapter, paragraph, html, text, sfs from "+TABLE_NAME+" where article=? and (type=1 or (weight in (1,2,5) and type=3)) order by _id asc";
		String[] args = new String[] {String.valueOf(article)};
		return bookElementsWithQuery(query, args);
	}
	
	public ArrayList<BookElement> getRandomParagraphs (int items) {
		String query = "select _id, article, weight, type, style, law, chapter, paragraph, html, text, sfs from "+TABLE_NAME+" where type=1 order by RANDOM() limit ?";
		String[] args = new String[] {String.valueOf(items)};
		return bookElementsWithQuery(query, args);
	}
	
	private String chapterParagraphStringForElement (BookElement elem, boolean showChapter, boolean showParagraph) {
		String str = "";
		// Add no-breaking spaces instead of regular space
		if (showChapter && elem.chapter.length() > 0) {
			str += (elem.chapter.replace(" ", " ") + " kap.");
		}
		if (showParagraph && elem.paragraph.length() > 0) {
			if (str.length() > 0) {
				str += " ";
			}
			str += (elem.paragraph.replace(" ", " ") + " §");
		}
		return str;
	}
	
	public String articleNameForElement (BookElement elem, boolean showChapter, boolean showParagraph) {
		if (elem == null) {
			return "";
		} 
		
		int articleID = elem.article;
		String query = "select text from "+TABLE_ARTICLES+" where article=?";
		String[] args = new String[] {String.valueOf(articleID)};
		
		String lawName = stringWithQuery(query, args);
		String chapterParagraphString = chapterParagraphStringForElement(elem, showChapter, showParagraph);
		String retStr = lawName +" "+ chapterParagraphString;
		
		return retStr;
	}
	
	public String articleNameForElement (BookElement elem) {
		return articleNameForElement (elem, true, true);
	}
	
	public String articleNameForID (int ID) {
		BookElement elem = elementWithID(ID);
		return articleNameForElement(elem);
	}

	public String articleTitleForElement (BookElement elem, boolean showChapter, boolean showParagraph) {
		if (elem == null) {
			return "";
		}
		int articleID = elem.article - 1;
		String lawName = "";
		ArrayList<BookElement> array = getArticles();
		if (articleID < array.size()) {
			BookElement articleElement = articleArray.get(articleID);
			lawName = articleElement.title;
		}
		String retStr = lawName;
	    if (elem.weight == 1 || elem.weight == 2 || elem.weight == 5) {
	        if (showChapter && elem.chapter.length() > 0) {
	        	retStr += elem.chapter;
	        }
	        	
	        if (showParagraph && elem.paragraph.length() > 0) {
	        	retStr += elem.paragraph;
	        }
	    }
	    return retStr;
	}
	
	public String articleTitleForElement (BookElement elem) {
	    return articleTitleForElement(elem, true, true);
	}

	public String articleTitleForID (int ID) {
	    BookElement elem = elementWithID (ID);
	    return articleTitleForElement(elem);
	}
	
	public String lawNameForElement (BookElement elem, boolean showChapter, boolean showParagraph) {
	    if (elem == null) {return "";} 
	    int lawID = elem.law;
	    String sql = "select text from "+TABLE_LAWS+" where law=?";
	    String[] args = new String[] {String.valueOf(lawID)};
		
		String lawName = stringWithQuery(sql, args);
		
	    String retStr = lawName;
	    String chapterParagraphString = chapterParagraphStringForElement (elem, showChapter, showParagraph);
	    retStr += " "+chapterParagraphString;
	    return retStr;
	}

	public String lawNameForElement (BookElement elem) {
	    return lawNameForElement(elem, true, true);
	}

	public String lawNameForID (int ID) {
	    BookElement elem = elementWithID(ID);
	    return lawNameForElement(elem);
	}
	
	public String lawTitleForElement (BookElement elem, boolean showChapter, boolean showParagraph) {
		if (elem == null) {return "";} 
	    int lawID = elem.law - 1;
	    String lawName = "";
	    ArrayList<BookElement> lawArray = getLaws();
	    if (lawID < lawArray.size()) {
	        BookElement articleElem = lawArray.get(lawID);
	        lawName = articleElem.title;
	    }
	    
	    String retStr = lawName;
	    if (showChapter && elem.chapter.length() > 0)
	        retStr += elem.chapter;
	    if (showParagraph && elem.paragraph.length() > 0)
	        retStr += elem.paragraph;
	    return retStr;
	}

	public String lawTitleForElement (BookElement elem) {
	    return lawTitleForElement(elem, true, true);
	}

	public String lawTitleForID (int ID) {
	    BookElement elem = elementWithID(ID);
	    return lawTitleForElement(elem);
	}
	
	public String articleAndLawNameForElement (BookElement elem) {
		String name = "";
		// PWA: 2018-01-08: Changed from 3000 to 5000: was not enough for HB 1 kap. FAL 14 kap.
		int fromID = elem.ID - 5000;
		int toID = elem.ID;
		if (elem.weight == 1 || elem.weight == 2 || elem.weight == 5) { // top level
	        name = articleNameForElement(elem);
	    } else if (elem.weight == 8) {
	    	String query = "select _id, article, weight, type, style, law, chapter, paragraph, html, text, sfs from "+TABLE_NAME+" where _id between ? and ? and article=? and weight in (1,2,5) order by _id desc limit 1";
	    	String[] args = new String[] {String.valueOf(fromID), String.valueOf(toID), String.valueOf(elem.article)};
	    	ArrayList<BookElement> result = bookElementsWithQuery(query, args);
	        if (!result.isEmpty()) {
	            name = articleNameForElement(result.get(0));
	        } else {
	            name = articleNameForElement(elem, false, false);
	        }
	    } else {
	    	String query = "select chapter from "+TABLE_NAME+" where _id between ? and ? and article=? and type=3 and weight in (1,2,5) order by _id desc limit 1";
	    	String[] args = new String[] {String.valueOf(fromID), String.valueOf(toID), String.valueOf(elem.article)};
	    	String chapter = stringWithQuery(query, args);
	        if (chapter != null && chapter.length() > 0) {
	        	chapter = chapter.replace(" ", " ") + " kap.";
	        	name = articleNameForElement(elem, false, false) + " " + chapter + "\n" + lawNameForElement(elem);
	        } else {
	        	name = articleNameForElement(elem, false, false) + "\n" + lawNameForElement(elem);
	        }
	    }
	    return name;
	}
	
	public String lawSFSForElement (BookElement elem, boolean showChapter, boolean showParagraph) {
		if (elem == null) {return "";} 
	    int lawID = elem.law;
	    // elem.setTitle("");
	    String lawName = "";
	    if (elem.title.length() > 0) {
	        lawName = elem.title;
	    } else {
	        String sql = "select text from "+TABLE_NAME+" where law=? and type = 1 order by _id limit 1";
	        String[] args = new String[] {String.valueOf(lawID)};
	        lawName = stringWithQuery(sql, args);
	    }
	    
	    String retStr = lawName;
	    String chapterParagraphString = chapterParagraphStringForElement(elem , showChapter , showParagraph);
	    retStr += " " + chapterParagraphString;
	    return retStr;
	}

	public String lawSFSForElement (BookElement elem) {
	    return lawSFSForElement(elem, true, true);
	}

	public String lawSFSForID (int ID) {
	    BookElement elem = elementWithID(ID);
	    return lawSFSForElement(elem);
	}
	
	/* PWA: Use default Collection.addAll()
	private void addObjectsFromArray (ArrayList<BookElement> fromArray, ArrayList<BookElement> toArray) {
		for (BookElement e: fromArray) {
			toArray.add(e);
		}
	}
	*/
	
	public ArrayList<BookElement> searchLaw (String law, String chapter, String paragraph) {
	    
	    if (law.length() < 1) {
	    	return new ArrayList<BookElement>();  // Empty array
	    }
	    
	    law = law.toLowerCase();
	    law = law.substring(0, 1).toUpperCase() + law.substring(1);
	    String sql;
	    ArrayList<BookElement> tmpArray;
	    ArrayList<BookElement> array = new ArrayList<BookElement>();
	    
	    if (chapter.length() == 0 && paragraph.length() == 0) {
			// PWA: 2017-01-08: Changed the ordering of all 3 sql
	        // Search SFS
	        sql = "select ID, 0, 0, 0, 0, law, '', '', '', sfs, sfs from "+TABLE_ABBR+" where sfs like '"+law+"%' collate nocase order by sfs asc limit ?";
	        tmpArray = bookElementsWithQuery(sql, new String[] {String.valueOf(SEARCH_LAW_LIMIT)}, TABLE_ABBR, BookElement.BOOK_TITLE);
	        if (!tmpArray.isEmpty()) array.addAll(tmpArray); // addObjectsFromArray(tmpArray, array);
	        
	        // Search Abbreviation
	        sql = "select ID, 0, 0, 0, 0, law, '', '', '', abbr.abbr, abbr.abbr from "+TABLE_ABBR+" where abbr.abbr like '"+law+"%' collate nocase order by abbr.abbr asc limit ?";
	        tmpArray = bookElementsWithQuery(sql, new String[] {String.valueOf(SEARCH_LAW_LIMIT)}, TABLE_ABBR, BookElement.BOOK_ABBR);
	        if (!tmpArray.isEmpty()) array.addAll(tmpArray); // addObjectsFromArray(tmpArray, array);
	        
	        // Search short form
	        sql = "select ID, 0, 0, 0, 0, law, '', '', '', short, short from "+TABLE_ABBR+" where short like '"+law+"%' collate nocase order by short asc limit ?";
	        tmpArray = bookElementsWithQuery(sql, new String[] {String.valueOf(SEARCH_LAW_LIMIT)}, TABLE_ABBR, BookElement.BOOK_SHORT);
	        if (!tmpArray.isEmpty()) array.addAll(tmpArray); // addObjectsFromArray(tmpArray, array);
	        
	        // Check limit
	        while (array.size() > SEARCH_LAW_LIMIT)
	            array.remove(array.size()-1);
	        
	    } else {
	        // Search SFS
	        sql = "select ID, 0, 0, 0, 0, law, '', '', '', sfs, sfs from "+TABLE_ABBR+" where sfs = '"+law+"' collate nocase order by ID asc limit ?";
	        tmpArray = bookElementsWithQuery(sql, new String[] {String.valueOf(1)}, TABLE_ABBR, BookElement.BOOK_TITLE);
	        
	        if (tmpArray.isEmpty()) {
	            // Search Abbreviation
	            sql = "select ID, 0, 0, 0, 0, law, '', '', '', abbr.abbr, abbr.abbr from "+TABLE_ABBR+" where abbr.abbr = '"+law+"' collate nocase order by ID asc limit ?";
	            tmpArray = bookElementsWithQuery(sql, new String[] {String.valueOf(1)}, TABLE_ABBR, BookElement.BOOK_ABBR);
	        }
	        
	        // Search short form
	        if (tmpArray.isEmpty()) {
	            sql = "select ID, 0, 0, 0, 0, law, '', '', '', short, short from "+TABLE_ABBR+" where short = '"+law+"' collate nocase order by ID asc limit ?";
	            tmpArray = bookElementsWithQuery(sql, new String[] {String.valueOf(1)}, TABLE_ABBR, BookElement.BOOK_SHORT);
	        }
	        
	        array.addAll(tmpArray); // addObjectsFromArray(tmpArray, array);
	    }
	    
	    if (array.isEmpty() || array.size() > 1) return array; // Nothing found or more than one found
	    
	    // Only one law found at this point, get the chapter and paragraphs
	    BookElement elem = array.get(0);
	    int lawID = elem.law;
	    String lawName = elem.text;
	    
	    // Create the new SQL
	    String sql1 = "select _id, article, weight, type, style, law, chapter, paragraph, html, '"+lawName+"', '"+lawName+"' from "+TABLE_NAME+" where law=? and type in (3,6)";
	    
	    String sql2 = "";
	    if (chapter.length() > 0) {
	        if (paragraph.length() > 0) {
	            sql2 = " and chapter='"+chapter+"'";
	        } else {
	            sql2 = " and (chapter='"+chapter+"' or (chapter='' and paragraph='"+chapter+"'))";
	        }
	    }
	    
	    String sql3 = "";
	    if (paragraph.length() > 0) {
	        sql3 = " and paragraph='"+paragraph+"'";
	    }
	    
	    sql = sql1+sql2+sql3+" order by _id asc limit ?";
	    String[] args = new String[] {String.valueOf(lawID), String.valueOf(SEARCH_LAW_LIMIT)};
	    
	    // PWA: 2015-08-04: Added/replaced
		// return bookElementsWithQuery(sql, args, lawName);
	    tmpArray = bookElementsWithQuery(sql, args, lawName);
	    if (tmpArray.size() > 0) return tmpArray;
	    return array;
	}

	public BookElement flipElementForID (int ID) {
	    if (this.getReadableDatabase() == null) return null;
		String sql = "select _id, article, weight, type, style, law, chapter, paragraph, html, text, sfs from "+TABLE_NAME+" where type in (1,3) and weight in (1,2,3,5) and _id <= ? order by _id desc limit 1";
		String[] args = new String[] {String.valueOf(ID)};
		ArrayList<BookElement> array = bookElementsWithQuery(sql, args);
		if (array.isEmpty())
	        return null;
	    return array.get(0);
	}

	public BookElement previousFlipElementForID (int ID) {
		if (this.getReadableDatabase() == null) return null;
		String sql = "select _id, article, weight, type, style, law, chapter, paragraph, html, text, sfs from "+TABLE_NAME+" where type in (1,3) and weight in (1,2,3,5) and _id < ? order by _id desc limit 1";
		String[] args = new String[] {String.valueOf(ID)};
		ArrayList<BookElement> array = bookElementsWithQuery(sql, args);
		if (array.isEmpty())
	        return null;
	    return array.get(0);
	}

	public BookElement nextFlipElementForID (int ID) {
		if (this.getReadableDatabase() == null) return null;
		String sql = "select _id, article, weight, type, style, law, chapter, paragraph, html, text, sfs from "+TABLE_NAME+" where type in (1,3) and weight in (1,2,3,5) and _id > ? order by _id asc limit 1";
		String[] args = new String[] {String.valueOf(ID)};
		ArrayList<BookElement> array = bookElementsWithQuery(sql, args);
		if (array.isEmpty())
	        return null;
	    return array.get(0);
	}

	public BookElement elementForBookPosition (double pos) {
	    int totalElements = getElementsInDatabase();
	    assert(START_ID_OF_BIHANG < totalElements);
	    int ID = 1;
	    if (pos < START_POS_OF_BIHANG) {
	        ID = (int)(pos / START_POS_OF_BIHANG * START_ID_OF_BIHANG);
	        if (ID >= START_ID_OF_BIHANG)
	            ID = START_ID_OF_BIHANG - 1;
	        else if (ID < 1)
	            ID = 1;
	    } else {
	        ID = (int)((pos - START_POS_OF_BIHANG) / (1.0 - START_POS_OF_BIHANG) * (totalElements - START_ID_OF_BIHANG) + START_ID_OF_BIHANG);
	        if (ID < START_ID_OF_BIHANG)
	            ID = START_ID_OF_BIHANG;
	        if (ID > totalElements)
	            ID = totalElements - 1;
	    }
	    // BookElement * elem = [self elementWithID:ID];
	    BookElement elem = flipElementForID(ID);
	    return elem;
	}

	public double positionForID (int ID) {
	    int totalElements = getElementsInDatabase();
	    double pos = 0.0f;
	    if (ID < START_ID_OF_BIHANG) {
	        pos = (double)ID / (double)START_ID_OF_BIHANG * START_POS_OF_BIHANG;
	        if (pos >= START_POS_OF_BIHANG)
	            pos = START_POS_OF_BIHANG - 0.001;
	    } else {
	        pos = (double)(ID - START_ID_OF_BIHANG) / (double)(totalElements - START_ID_OF_BIHANG) * (1.0 - START_POS_OF_BIHANG) + START_POS_OF_BIHANG;
	        if (pos < START_POS_OF_BIHANG)
	            pos = START_POS_OF_BIHANG;
	        if (pos > 0.99999f)
	            pos = 0.99999f;
	    }
	    return pos;
	}

	public double positionForElement (BookElement element) {
	    if (element != null)
	        return positionForID(element.ID);
	    return 0.0;
	}

    private synchronized ArrayList<BookElement> bookElementsWithQuery2 (String query, String[] args) {
        ArrayList<BookElement> queryResults = new ArrayList<BookElement>();
        Cursor cursor = null;
        try {
            cursor = this.getReadableDatabase().rawQuery(query, args);
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    do {
                        BookElement bookElement = new BookElement();
                        bookElement.ID = cursor.getInt(0);
                        bookElement.article = cursor.getInt(1);
                        bookElement.weight = cursor.getInt(2);
                        bookElement.type = cursor.getInt(3);
                        bookElement.style = cursor.getInt(4);
                        bookElement.law = cursor.getInt(5);
                        bookElement.chapter = cursor.getString(6);
                        bookElement.paragraph = cursor.getString(7);
                        try {
                            bookElement.html = crypt2(cursor.getString(8));
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                        bookElement.text = cursor.getString(9);
                        bookElement.title = cursor.getString(10);
                        queryResults.add(bookElement);
                    } while(cursor.moveToNext());
                }
            }
        } finally {
            if (cursor != null) {
                cursor.close();
                    ////Log.d("DB", "Cursor closed for query - "+query);
            }
        }
        return queryResults;
    }

	public int idForOldLawbookId(int oldId) {
		if (this.getReadableDatabase() == null) { return 0; }

		// Get old BookElem info
		String sql = "select _id, article, weight, type, style, law, chapter, paragraph, '', '', '' from srl3_2017 where _id = ?";
		String[] args = new String[] {String.valueOf(oldId)};
		ArrayList<BookElement> array = bookElementsWithQuery2(sql, args);
		if (array.isEmpty())
			return 0;
		BookElement oldElem = array.get(0);

		// Get law remapping
		sql = "select newlaw from laws_migration where oldlaw = ?";
		args = new String[] {String.valueOf(oldElem.law)};
		int newLaw = integerWithQuery(sql, args);
		if (newLaw == 0) return 0;

		// Find the new BookElem info
		sql = "select _id, article, weight, type, style, law, chapter, paragraph, '', '', '' from "+TABLE_NAME+" where law=? and chapter=? and paragraph=? and type=? and weight=? and style=? order by _id desc";
		args = new String[] {String.valueOf(newLaw), oldElem.chapter, oldElem.paragraph, String.valueOf(oldElem.type), String.valueOf(oldElem.weight), String.valueOf(oldElem.style)};
		array = bookElementsWithQuery2(sql, args);
		if (array.size() > 0) {
			BookElement newElem = array.get(0);
			return newElem.getId();
		}

		// Find the new BookElem info, search with fewer terms (1)
		sql = "select _id, article, weight, type, style, law, chapter, paragraph, '', '', '' from "+TABLE_NAME+" where law=? and chapter=? and paragraph=? and type=? order by _id desc";
		args = new String[] {String.valueOf(newLaw), oldElem.chapter, oldElem.paragraph, String.valueOf(oldElem.type)};
		array = bookElementsWithQuery2(sql, args);
		if (array.size() > 0) {
			BookElement newElem = array.get(0);
			return newElem.getId();
		}

		// Find the new BookElem info, search with even fewer terms (2)
		sql = "select _id, article, weight, type, style, law, chapter, paragraph, '', '', '' from "+TABLE_NAME+" where law=? and chapter=? and paragraph=? order by _id desc";
		args = new String[] {String.valueOf(newLaw), oldElem.chapter, oldElem.paragraph};
		array = bookElementsWithQuery2(sql, args);
		if (array.size() > 0) {
			BookElement newElem = array.get(0);
			return newElem.getId();
		}

		// Find the new BookElem info, search with even fewer terms (3)
		sql = "select _id, article, weight, type, style, law, chapter, paragraph, '', '', '' from "+TABLE_NAME+" where law=? and chapter=? order by _id desc";
		args = new String[] {String.valueOf(newLaw), oldElem.chapter};
		array = bookElementsWithQuery2(sql, args);
		if (array.size() > 0) {
			BookElement newElem = array.get(0);
			return newElem.getId();
		}

		// Find the new BookElem info, search with even fewer terms (4)
		sql = "select _id, article, weight, type, style, law, chapter, paragraph, '', '', '' from "+TABLE_NAME+" where law=? order by _id desc";
		args = new String[] {String.valueOf(newLaw)};
		array = bookElementsWithQuery2(sql, args);
		if (array.size() > 0) {
			BookElement newElem = array.get(0);
			return newElem.getId();
		}

		return 0;
	}
	
	private String crypt2(String string) throws UnsupportedEncodingException {
		byte[] byteStr = null;
		try {
			byteStr = string.getBytes("UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		int n = byteStr.length;
		int i = 0;
		while (n > 0) {
			n--;
			if (byteStr[i] >= 32 && byteStr[i] <=126) {
				int c = 126 + n * 19;
				byteStr[i] = (byte) (((c - (byteStr[i] - 32)) % (126 - 32)) + 32);
			}
			i++;
		}
		String returnStr = new String(byteStr,"UTF-8");
		return returnStr;
	}	
}
