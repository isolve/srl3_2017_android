package se.isolve.srl3;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Point;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.VideoView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import se.isolve.srl3.NotesActivity.NoteAdapter.Holder;
import se.isolve.srl3.database.BookDB;
import se.isolve.srl3.database.BookElement;
import se.isolve.srl3.reader.BookmarkHistoryItem;
import se.isolve.srl3.reader.BookmarkHistoryNotesManager;
import se.isolve.srl3.reader.Pages;
import utils.SRLUtils;

public class NotesActivity extends SRLMenuActivity implements OnCompletionListener, OnPreparedListener, OnItemClickListener, OnMenuItemClickListener{
	private ArrayList<BookElement> searchArray = new ArrayList<BookElement>();
	private ArrayList<String> notesArray = new ArrayList<String>();
	private ArrayList<BookmarkHistoryItem> bookmarkHistoryItems = new ArrayList<BookmarkHistoryItem>();
	private BookDB mDbHelper;
	private ListView lvNotes;
	private ImageView imgNote;
	private int width = 0;
	private int height = 0;
	private  LinearLayout mListLayout;
	private LinearLayout llNote;
	private LinearLayout llWrapper;
	private TaskGetList task = null;
	private CountDownTimer mSearchTimer;
	private ImageView imgIcon;
	private VideoView mVV;
	private BookElement book;
	private int VIDEO_STATE = 0;
	private static final int VIDEO_P2_IS_PLAYING = 4;
	private static final int VIDEO_P3_IS_PLAYING = 2;
	private Integer tag;
	private BookmarkHistoryItem bookmarkHistoryItem;
	
	private String NameFromList = "list";
	private int extraInt;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.bookmark);
		Display display = getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		width = size.x;
		height = size.y;
		imgIcon = (ImageView)findViewById(R.id.img_iconbm);
		imgIcon.setImageResource(R.drawable.item6);
		lvNotes = (ListView) findViewById(R.id.lv_bookmark);
		lvNotes.setOnItemClickListener(this);
		imgNote = (ImageView) findViewById(R.id.imgBookmark);
		llWrapper = (LinearLayout)findViewById(R.id.ll_notes);
		TextView title = (TextView)findViewById(R.id.lbh_title);	
		title.setTypeface(SRL3ApplicationClass.getMyriadPro());
		title.setText("Anteckningar");
		
		imgNote.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(NotesActivity.this, ListGuide.class);
				startActivity(intent);
				overridePendingTransition(R.anim.your_left_to_right, R.anim.your_right_to_left);
//				NotesActivity.this.finish();
			}
		});
		llNote = (LinearLayout)findViewById(R.id.llBookmark);
		double f = 0.2;
		int h = (int)(height * f);
		llNote.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,h));
		mListLayout = (LinearLayout)findViewById(R.id.layout_bookmark);
		double widthMarginRatio = 0.14;
		int w = (int) (width * widthMarginRatio); // getResources().getDimension(R.dimen.marginbook);
		LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(width - w, LayoutParams.MATCH_PARENT);
		layoutParams.gravity = Gravity.CENTER;
		mListLayout.setLayoutParams(layoutParams);
		mVV = (VideoView)findViewById(R.id.myvideoview_note);
		mVV.setOnCompletionListener(this);
		mVV.setOnPreparedListener(this);
		extraInt = getIntent().getIntExtra(NameFromList, 0);
		if (extraInt == 1) {
			initSplashVideo("join3");
			VIDEO_STATE = VIDEO_P3_IS_PLAYING;
		} else {
			mVV.setVisibility(View.GONE);
			llWrapper.setVisibility(View.VISIBLE);
			setView();
		}
	}
	
	private void setView(){
		if (mSearchTimer != null) {
			mSearchTimer.cancel();

			// //Log.e(LOG_TAG, "Timer was cancelled");
		}
		mSearchTimer = new CountDownTimer(100, 100) {
			public void onTick(long millisUntilFinished) {/* do nothing */
			}

			public void onFinish() {
				if (task != null
						&& task.getStatus().equals(
								AsyncTask.Status.RUNNING)) {
					task.cancel(true);
				}
				
				if (searchArray.size() == 0) {
					task = new TaskGetList();
					task.execute("");
				}
				
				// //Log.e(LOG_TAG, "Done timer starting search with: " +
				// filterText.getText().toString());
				mSearchTimer = null;
			}
		}.start();
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		mDbHelper = new BookDB(this);
		if (extraInt == 1) {
			new Thread() {
				public void run() {
					mVV.start();
					extraInt = 0;
				};
			}.start();
		} else {
			initSplashVideo("join3");
			mVV.start();
			VIDEO_STATE = VIDEO_P3_IS_PLAYING;
		}
	}
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
	}
	 
	private class CustomComparator implements Comparator<BookmarkHistoryItem> {
	    @Override
	    public int compare(BookmarkHistoryItem item1, BookmarkHistoryItem item2) {
	        return item1.getDate().compareTo(item2.getDate());
	    }
	}
	
	class TaskGetList extends AsyncTask<String, Void, Integer>{
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			searchArray.clear();
			notesArray.clear();
		}
		@Override
		protected Integer doInBackground(String... params) {
			mDbHelper.createDatabase();
			//searchArray = mDbHelper.getRandomParagraphs(8);
			bookmarkHistoryItems = new ArrayList<BookmarkHistoryItem>(BookmarkHistoryNotesManager.getInstance().getNotes());
			
			Collections.sort(bookmarkHistoryItems, new CustomComparator());
			Collections.reverse(bookmarkHistoryItems);
			
			for(BookmarkHistoryItem item : bookmarkHistoryItems) {
				if (item.text != null && (item.text.length() > 0)) {
					BookElement element = mDbHelper.elementWithID(item.ID);
					searchArray.add(element);
					notesArray.add(item.text);
				}
			}
			mDbHelper.close();
			return null;
		}
		@Override
		protected void onPostExecute(Integer result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			setAdapter();
		}
		
	}
	
	private void setAdapter() {
		ArrayList<String> listStr = new ArrayList<String>();
		ArrayList<Integer> listPoss = new ArrayList<Integer>();
		if (searchArray == null)
			return;
		for (BookElement e : searchArray) {
			//listStr.add(labelForElement(e));
			String articleName = mDbHelper.articleNameForElement(e);
			listStr.add(articleName);
			float pos = (float) mDbHelper.positionForElement(e);
			// double indexPosX = ( pos * 0.845 )*100;
			int indexPosX = (int) (pos * 1000);
			//Log.i("indexPosX", "indexPosX " + indexPosX);
			//Log.i("PosX", "PosX " + pos);
			listPoss.add(indexPosX);
		}
		if (searchArray.isEmpty()) {
			BookElement noItem = new BookElement();
			noItem.ID = -1;
			listStr.add(getResources().getString(R.string.message_no_notes));
			listPoss.add(-1);
			searchArray.add(noItem);
		} else {
			lvNotes.setOnItemLongClickListener( new AdapterView.OnItemLongClickListener
					(){
				public boolean onItemLongClick(AdapterView<?> av, View v, int pos, long id) {
					showPopup(v);
					return true;
				}
			});
		}
		NoteAdapter adapter = new NoteAdapter(NotesActivity.this, listPoss, listStr, notesArray);
		lvNotes.setAdapter(adapter);
	}
	
	public void showPopup(View v) {
	    PopupMenu popup = new PopupMenu(this, v);
	    Holder h = (Holder) v.getTag();
	    tag = (Integer) h.pos;
		BookElement bookie = searchArray.get(tag);
		bookmarkHistoryItem = new BookmarkHistoryItem();
		bookmarkHistoryItem.ID = bookie.ID;
	    MenuInflater inflater = popup.getMenuInflater();
	    popup.setOnMenuItemClickListener(this);
	    inflater.inflate(R.menu.delete_item_menu, popup.getMenu());
	    popup.show();
	}
	
	@Override
	public boolean onMenuItemClick(MenuItem item) {
	    switch (item.getItemId()) {
	        case R.id.menu_ta_bort:
//				BookmarkHistoryNotesManager.getInstance().deleteNote(bookmarkHistoryItem);
	        	BookmarkHistoryItem deletedItem = BookmarkHistoryItem.getItem(bookmarkHistoryItem.ID, 0, SRLUtils.getCurrentTimeStamp(), null, "", true);
				BookmarkHistoryNotesManager.getInstance().addNoteToArray(deletedItem, true);
				searchArray.remove(tag.intValue());
				notesArray.remove(tag.intValue());
				setAdapter();
	            return true;
	        case R.id.menu_ta_bort_alla:
	        	BookmarkHistoryNotesManager.getInstance().deleteAllNotes();		
				searchArray.clear();
				notesArray.clear();
				setAdapter();
	            return true;
	        default:
	            return false;
	    }
	}
	
	private String labelForElement(BookElement elem) {
//	    String lawName = "";
	    String title = elem.getTitle();
	    if (title != null && !title.trim().equalsIgnoreCase("")) {
	        return title;
	    }
	    StringBuffer retStr = new StringBuffer();
	    if (elem.getChapter() != null && !elem.getChapter().trim().equalsIgnoreCase(""))
	        retStr.append(elem.getChapter()).append(" kap. ");
	    if (elem.getParagraph() != null && !elem.getParagraph().trim().equalsIgnoreCase(""))
	        retStr.append(elem.getParagraph()).append(" § ");
	    return retStr.toString();
	}
	
	protected void initSplashVideo(String name) {
		int res = this.getResources().getIdentifier(name, "raw",
				getPackageName());
		if (!playFileRes(res))
			return;
	}

	private boolean playFileRes(int fileRes) {
		if (fileRes == 0) {
			stopPlaying();
			return false;
		} else {
			mVV.setVideoURI(Uri.parse("android.resource://" + getPackageName()
					+ "/" + fileRes));

			return true;
		}
	}

	public void stopPlaying() {
		mVV.stopPlayback();
	}

	@Override
	public void onPrepared(MediaPlayer mp) {
		// TODO Auto-generated method stub
		mp.setLooping(false);
	}

	@Override
	public void onCompletion(MediaPlayer mp) {
		// TODO Auto-generated method stub
		if(VIDEO_STATE == VIDEO_P2_IS_PLAYING){
			Intent intent = new Intent(NotesActivity.this, Pages.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			intent.putExtra(BookElement.BOOK_ID, book.ID);
			startActivity(intent);
			overridePendingTransition (0, 0);
			extraInt = 0;
			return;			
		} else if (VIDEO_STATE == VIDEO_P3_IS_PLAYING) {
			mVV.setVisibility(View.GONE);
			llWrapper.setAlpha(1);
			llWrapper.setVisibility(View.VISIBLE);
			setView();
			return;
		}
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		if (searchArray != null && searchArray.size() > 0){
			book = searchArray.get(arg2);
			if (book.ID > 0) {
				llWrapper.animate().alpha(0f).setDuration(50);
				mVV.setVisibility(View.VISIBLE);
				int res = NotesActivity.this.getResources().getIdentifier("join2", "raw", getPackageName());
				mVV.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" + res));
				mVV.start();
				this.VIDEO_STATE = VIDEO_P2_IS_PLAYING;
			}
		}
	}

	public class NoteAdapter extends BaseAdapter {
		List<String> result;
		Context context;
		List<Integer> listPos;
		List<String> listNotes;

		public NoteAdapter(Activity activity, List<Integer> listPos,
				List<String> objects, List<String>notes) {
			result = objects;
			context = activity;
			this.listPos = listPos;
			listNotes = notes;
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return result.size();
		}

		@Override
		public Object getItem(int arg0) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return arg0;
		}

		public class Holder {
			TextView tvOrder;
			SeekBar bookmarkBar;
			TextView notesTextView;
			Integer pos;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO Auto-generated method stub
			Holder holder = new Holder();
			if (convertView == null) {
				LayoutInflater inflater = ((Activity) context).getLayoutInflater();
				convertView = inflater.inflate(R.layout.anteckningar_list_row, null);
				holder.tvOrder = (TextView) convertView.findViewById(R.id.twOrder);
				holder.bookmarkBar = (SeekBar) convertView.findViewById(R.id.bookmarkBar);
				holder.notesTextView = (TextView) convertView.findViewById(R.id.anteckningar_notes_text_view);
				convertView.setTag(holder);

			} else {
				holder = (Holder) convertView.getTag();
			}
			holder.pos = position;
			holder.tvOrder.setText(result.get(position));
			Integer listPosition = listPos.get(position);
			if (listPosition >= 0) { 
				holder.notesTextView.setText(listNotes.get(position));
				holder.bookmarkBar.setProgress(listPosition);
				holder.bookmarkBar.setEnabled(false);
			} else {
				holder.bookmarkBar.setVisibility(View.GONE);
				convertView.setEnabled(false);
				convertView.setOnClickListener(null);
			}
			Typeface font1 = Typeface.createFromAsset(context.getAssets(), "html/font/MyriadPro-Regular.otf");
			holder.tvOrder.setTypeface(font1);
			holder.notesTextView.setTypeface(SRL3ApplicationClass.getNoteWorthy());
			return convertView;
		}

	}
}

