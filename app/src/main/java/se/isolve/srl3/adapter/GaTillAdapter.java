package se.isolve.srl3.adapter;

import java.util.List;

import se.isolve.srl3.R;
import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.SeekBar;
import android.widget.TextView;



public class GaTillAdapter extends BaseAdapter {
	List<String> result;
	Context context;
	List<Integer> listPos;
	Typeface font;

	public GaTillAdapter(Activity activity, List<Integer> listPos, List<String> objects) {
		context = activity;
		font = Typeface.createFromAsset(context.getAssets(), "html/font/MyriadPro-Regular.otf");
		result = objects;
		this.listPos = listPos;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return result.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

	public class Holder {
		TextView tvOrder;
		SeekBar bookmarkBar;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		Holder holder = new Holder();
		if (convertView == null) {
			LayoutInflater inflater = ((Activity) context).getLayoutInflater();
			convertView = inflater.inflate(R.layout.list_item, null);
			holder.tvOrder = (TextView) convertView.findViewById(R.id.twOrder);
			holder.bookmarkBar = (SeekBar) convertView.findViewById(R.id.bookmarkBar);
			convertView.setTag(holder);

		} else {
			holder = (Holder) convertView.getTag();
		}
		holder.tvOrder.setText(result.get(position));
		holder.bookmarkBar.setProgress(listPos.get(position));
		holder.bookmarkBar.setEnabled(false);
		//holder.tvOrder.setTypeface(font1);
		holder.tvOrder.setTypeface(font);
		return convertView;
	}


}

