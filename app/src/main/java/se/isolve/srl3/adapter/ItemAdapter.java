package se.isolve.srl3.adapter;

import se.isolve.srl3.R;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ItemAdapter extends BaseAdapter {
	String[] result;
	Context context;
	int[] imageId;

	public ItemAdapter(Activity activity, String[] prgmNameList,
			int[] prgmImages) {
		result = prgmNameList;
		context = activity;
		imageId = prgmImages;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return imageId.length;
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

	public class Holder {
		TextView tv;
		ImageView img;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		Holder holder = new Holder();
		if (convertView == null) {
			LayoutInflater inflater = ((Activity) context).getLayoutInflater();
			convertView = inflater.inflate(R.layout.drawer_list_item, null);
			holder.img = (ImageView) convertView.findViewById(R.id.imgItem1);
			holder.tv = (TextView) convertView.findViewById(R.id.tvItem1);
			convertView.setTag(holder);

		} else {
			holder = (Holder) convertView.getTag();
		}
		holder.tv.setText(result[position]);
		holder.img.setImageResource(imageId[position]);
		return convertView;
	}

}
