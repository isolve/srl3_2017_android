package se.isolve.srl3;

import se.isolve.introview.IntroViewActivity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

public class AboutActivity extends SRLMenuActivity {

	private WebView wvAbout;
	private LinearLayout llAbout;
	private int width = 0;
	private int height = 0;
	private static final String ABOUT_LOCATION = "file:///android_asset/html/omappen.xhtml";
	private static final String INTRO_URL = "intro://";
	private String NameFromList = "list";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.about);

		int extraInt = getIntent().getIntExtra(NameFromList, 0);
		initViews();
		llAbout.setVisibility(View.VISIBLE);
	}
	
	public void initViews() {
		TextView mainHeader = (TextView) findViewById(R.id.lbh_title);
		mainHeader.setTypeface(SRL3ApplicationClass.getMyriadPro());
		Display display = getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		width = size.x;
		height = size.y;
		ImageView imgAbout = (ImageView) findViewById(R.id.imgAbout);
		imgAbout.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(AboutActivity.this, ListGuide.class);
				startActivity(intent);
				overridePendingTransition(R.anim.your_left_to_right, R.anim.your_right_to_left);
				// AboutActivity.this.finish();
			}
		});
		llAbout = (LinearLayout) findViewById(R.id.llAbout);

		double f = 0.2;
		int h = (int) (height * f);
		llAbout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, h));

		LinearLayout layout_about = (LinearLayout) findViewById(R.id.layout_about);
		double widthMarginRatio = 0.14;
		int w = (int) (width * widthMarginRatio); // getResources().getDimension(R.dimen.marginbook);
		LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(width - w, LayoutParams.MATCH_PARENT);
		layoutParams.gravity = Gravity.CENTER;
		layout_about.setLayoutParams(layoutParams);
		
		WebView wvAbout = (WebView) findViewById(R.id.wv_about);
		wvAbout.setBackgroundColor(Color.argb(1, 0, 0, 0));
		WebSettings settings = wvAbout.getSettings();
		settings.setJavaScriptEnabled(true);
		wvAbout.setWebViewClient(new WebViewClient () {
			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				// Default starting page
				if (url.equals(ABOUT_LOCATION)) {
					return false;
				} else if (url.startsWith(INTRO_URL)) {
					// Open Intro
					Intent intent = new Intent(getApplicationContext(), IntroViewActivity.class);
					startActivity(intent);
					finish();
				} else {
					Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
	                startActivity(i);
				}
				// Add support for Intro and External support link
				return true;
			}
		});
		wvAbout.loadUrl(ABOUT_LOCATION);
	}
}
