package se.isolve.srl3;

import se.isolve.srl3.reader.BookmarkHistoryNotesManager;
import utils.LoginHandler;
import utils.LoginHandler.LoginCallBack;
import utils.SRLSynchronize;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;


public class SRL3ApplicationClass extends Application implements LoginCallBack{
	private static String TAG = "SRL3ApplicationClass";
     private static Context sContext;

     boolean isBackground = false, loggedIn = false;
     
    @Override
    public void onCreate() {
        super.onCreate();
        sContext = getApplicationContext();
        initSingletons();
        
        this.registerActivityLifecycleCallbacks(new ActivityLifecycleCallbacks() {
            @Override
            public void onActivityStopped(Activity activity) {
            	Log.i(TAG, "App activity stopped");
            	//SRLSynchronize.sendLatestNotes(getApplicationContext());
            }

            @Override
            public void onActivityStarted(Activity activity) {
            	Log.i(TAG, "App activity started");
            }

            @Override
            public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
            	Log.i(TAG, "App activity save instance state");
            }

            @Override
            public void onActivityResumed(Activity activity) {
            	if (LoginHandler.getInstance().isUserLoggedIn()) {
            		if (SRLSynchronize.getInstance() != null) {
            	SRLSynchronize.getInstance().checkSyncTimestamp(getContext());
            	if (!LoginHandler.getInstance().checkSessionTimestamp()) {
            		loginInBackground();
            	}
            	}
            	}
            	Log.i(TAG, "App activity resumed");
            	//SRLSynchronize.getLatestSyncedNotes(getApplicationContext());
            }

            @Override
            public void onActivityPaused(Activity activity) {
            	Log.i(TAG, "App activity paused");
            }

            @Override
            public void onActivityDestroyed(Activity activity) {
             	Log.i(TAG, "App activity destroyed");
            }

            @Override
            public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
                 Log.i(TAG, "App entered foreground");
            }
        });
    }
    
    protected void initSingletons() {
        // Initialize singletons
        BookmarkHistoryNotesManager.initInstance();
        LoginHandler.initInstance(getContext());
    }

    public static Context getContext() {
        return sContext;
    }

    public static Typeface getMyriadPro() {
    	Typeface tfMyriadPro = Typeface.createFromAsset(SRL3ApplicationClass.getContext().getAssets(),
				"html/font/MyriadPro-Regular.otf");
    	return tfMyriadPro;
    }
    
    public static Typeface getNoteWorthy() {
    	Typeface tfMyriadPro = Typeface.createFromAsset(SRL3ApplicationClass.getContext().getAssets(),
				"html/font/noteworthy.ttf");
    	return tfMyriadPro;
    }
    
    private void loginInBackground() {
    	LoginHandler.getInstance().setLoginCallbackListener(this);
    	if	(LoginHandler.getInstance().isUserLoggedIn()) {
    	LoginHandler.getInstance().checkLogin(null, null);
    	}
    	
    	else {
    		LoginHandler.getInstance().logout();
    		Intent loginIntent = new Intent(getContext(), SplashScreen.class);
    		loginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    		startActivity(loginIntent);
    	}
    }
    
    
	@Override
	public void onLoginQueryCompleted(String arg) {
		if	(arg.equalsIgnoreCase(getContext().getResources().getString(R.string.connection_to_server_error))) {
			Intent loginIntent = new Intent(getContext(), SplashScreen.class);
			loginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    		startActivity(loginIntent);
		}
		else {
		if (Character.toString(arg.charAt(0)).equalsIgnoreCase("1")) {
		}
		else if (arg.equalsIgnoreCase("-1")) {
			Toast.makeText(SRL3ApplicationClass.getContext(), "Felaktig e-post eller lösenord", Toast.LENGTH_LONG).show();
			Intent loginIntent = new Intent(getContext(), SplashScreen.class);
			loginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    		startActivity(loginIntent);
		}
		else if (arg.equalsIgnoreCase("-2")) {
			Toast.makeText(SRL3ApplicationClass.getContext(), "Kontot saknar behörighet till denna lagbok", Toast.LENGTH_LONG).show();
			Intent loginIntent = new Intent(getContext(), SplashScreen.class);
			loginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    		startActivity(loginIntent);
		}
		else if (arg.equalsIgnoreCase("-3")) {
			Toast.makeText(SRL3ApplicationClass.getContext(), "Problem med inloggningstjänsten. Vänligen prova igen senare.", Toast.LENGTH_LONG).show();
			Intent loginIntent = new Intent(getContext(), SplashScreen.class);
			loginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    		startActivity(loginIntent);
		}
		else if (arg.equalsIgnoreCase("-4")) {
			Toast.makeText(SRL3ApplicationClass.getContext(), "För många öppna sessioner. Vänligen logga ut från en aktiv session.", Toast.LENGTH_LONG).show();
			Intent loginIntent = new Intent(getContext(), SplashScreen.class);
			loginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    		startActivity(loginIntent);
		}
		}
	}
}
