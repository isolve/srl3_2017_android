package se.isolve.srl3.reader;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import se.isolve.lib.pagebased.SRLViewPager;
import se.isolve.srl3.ChooserPageScreen;
import se.isolve.srl3.ListGuide;
import se.isolve.srl3.R;
import se.isolve.srl3.SplashScreen;
import se.isolve.srl3.database.BookDB;
import se.isolve.srl3.database.BookElement;
import se.isolve.srl3.reader.PageFragment.PageFragmentListener;
import se.isolve.srl3.reader.PagesTOCHandler.TOCEventListener;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
//import android.util.Log;

public class Pages extends FragmentActivity implements TOCEventListener, PageFragmentListener {
	
//	private CsWebView web1,web2,web3;
	private TextView mTextTop;
//	SizeChangedObserver sizeChanged;

//	private String[] mFile;
	
//	private CurlView mCurlView;
//	private RelativeLayout frmWeb1,frmWeb2,frmWeb3;
//	FrameLayout frmInner;
	
	public static int isBack = 1;
	public static int isNext = 2;
	public static int flag = isBack;
	private Button btnCloseCV;
	private BookDB bookDB;
	private BookElement mBookElement;
	private LinearLayout header;
	private RelativeLayout parentView;
	private ListView tocList;
//	private AutoFitTextView title1, title2, title3;
	//MyWebViewClient webViewClient1, webViewClient2, webViewClient3;
	public static boolean isKitKat;
	private String NameFromList = "list";
	PagesTOCHandler tocHandler;
	boolean initial = false;
	private SRLViewPager pager;
	private PagerAdapter pagerAdapter;
	
	private ArrayList<PageFragment> pages = new ArrayList<PageFragment>();
	
	private int ID, article;
	private PageFragment page;
	
	ArrayList<Integer> web1Hash = new ArrayList<Integer>(),
			web2Hash = new ArrayList<Integer>(),
			web3Hash = new ArrayList<Integer>();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.page);
		
		initial = true;
	}
	
	@Override
	public void onResume() {
		super.onResume();
		long timestamp = getTimestamp();
		Date date = new Date();
		long timestampNow = date.getTime();
		if (timestampNow - timestamp > 86400000) {
			logout();
		}
	}

	@Override
	public void onStart(){
	    super.onStart();
	    
	    if (initial) {
	    	// Open database.
		    bookDB = new BookDB(this);
			bookDB.createDatabase();
			
			ID = getIntent().getIntExtra(BookElement.BOOK_ID, 1);
			article = bookDB.articleForID(ID);
			
			mBookElement = bookDB.elementWithID(ID);
			
			// get controls
			header = (LinearLayout) findViewById(R.id.lloptionCV);
			
			parentView = (RelativeLayout) findViewById(R.id.pagesParentView);
			
			tocList = (ListView) findViewById(R.id.pages_toc);
			tocList.setVisibility(View.GONE);
			tocHandler = new PagesTOCHandler(this, tocList, bookDB);
			tocHandler.setTOCEventListener(this);
			
			this.generatePages();
			
			pager = (SRLViewPager) findViewById(R.id.pages_pager);
			pager.setPageMargin(10);
			pager.setPageMarginDrawable(R.color.gray);
			pagerAdapter = new PagerAdapter(this.getSupportFragmentManager());
			pager.setAdapter(pagerAdapter);
			
			if ((article >= 1) && (article <= BookDB.NUMBER_OF_ARTICLES)) {
				pager.setCurrentItem(article - 1);
			}
		
			pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
				
				@Override
				public void onPageSelected(int pos) {
					// TODO Auto-generated method stub
					//Log.d("Pager", "Selected page at position - "+pos);
					article = pos + 1;
					pagerAdapter.getItem(pos).shouldLoadFirstID = true;
				}
				
				@Override
				public void onPageScrolled(int pos, float positionOffset, int positionOffsetPixels) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onPageScrollStateChanged(int state) {
					// TODO Auto-generated method stub
					
				}
			});
			
//			mCurlView = (CurlView) findViewById(R.id.curl);
//			frmWeb1 = (RelativeLayout) findViewById(R.id.frmWeb1);
//			frmWeb2 = (RelativeLayout) findViewById(R.id.frmWeb2);
//			frmWeb3 = (RelativeLayout) findViewById(R.id.frmWeb3);
//			
//			frmInner = (FrameLayout) findViewById(R.id.frmMain);
			
//			web1 = (CsWebView) findViewById(R.id.web1);
//			web2 = (CsWebView) findViewById(R.id.web2);
//			web3 = (CsWebView) findViewById(R.id.web3);
			
			// Get the ID of the law to be loaded.
//			web3.ID = getIntent().getIntExtra(BookElement.BOOK_ID, 1);
//			web3.article = bookDB.articleForID(web3.ID);
			
//			mTextTop = (TextView) findViewById(R.id.fixKitkat);
//			mTextTop.setOnTouchListener(this);
//			mTextTop.setOnLongClickListener(this);

			//mFile = getResources().getStringArray(R.array.book_list);
//			web3.setCallback(this);
//			web2.setCallback(this);
//			web1.setCallback(this);
//			
//			web3.setOnScrollChangedCallback(this);
//			web2.setOnScrollChangedCallback(this);
//			web1.setOnScrollChangedCallback(this);
//			
//			web3.addJavascriptInterface(new CsJavaScriptHandler(), "Android");
//			web2.addJavascriptInterface(new CsJavaScriptHandler(), "Android");
//			web1.addJavascriptInterface(new CsJavaScriptHandler(), "Android");
//			
//			sizeChanged = new SizeChangedObserver();
//			mCurlView.setSizeChangedObserver(sizeChanged);
//			mCurlView.setBitmapProvider(new BitmapProvider());
//			mCurlView.set2PagesLandscape(false);
			
//			hidenTopViewIfNotKitKat();
			
			btnCloseCV = (Button)findViewById(R.id.btnCloseCV);
			
			btnCloseCV.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					
					String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
					BookmarkHistoryItem bookHistoryItem = new BookmarkHistoryItem();
					bookHistoryItem.ID = ID;
					bookHistoryItem.date = date;
					if	(ID > 0) {
					BookmarkHistoryNotesManager.getInstance().insertHistoryItem(bookHistoryItem);
					}
					Intent intent = new Intent(getApplicationContext(),
							ListGuide.class);
					intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
					intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					intent.putExtra(NameFromList, 1);
					startActivity(intent);
					overridePendingTransition(R.anim.your_left_to_right, R.anim.your_right_to_left);
//					finish();
				}
			});
		 
			//////Log.d("Pages", "Current page - "+(mBookElement.getArticle()-1)+", and ID - "+ID);
//			mCurlView.setCurrentIndex(web3.article-1);
//			mCurlView.setCurrentIndex(mBookElement.getArticle()-1);
			
			
			header.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					////Log.d("Pages", "Hide toc");
					tocHandler.hideTOC();
				}
			});
		    
//		  loadInitialArticles(article);
		  
		  // Set up the context menus for the web views.
		  //setContextMenus();
		  
		  initial = false;
	    }
	}
	
	private void generatePages () {
		for (int i = 0; i<BookDB.NUMBER_OF_ARTICLES; i++) {
			page = new PageFragment(Pages.this, Pages.this, bookDB, i);
			if (i == (article - 1)) {
				page.ID = ID;
				page.article = article;
				page.shouldLoadFirstID = false;
			}
			page.setPageFragmentListener(this);
			page.loaded = true;
			pages.add(page);
		}
	}
	
	private class PagerAdapter extends FragmentStatePagerAdapter {

		public PagerAdapter (FragmentManager fm) {
			super(fm);
		}
		
		@Override
		public PageFragment getItem(int pos) {
			// TODO Auto-generated method stub
			int prev = pos-1, next = pos+1;
			PageFragment page = pages.get(pos), prevPage = null, nextPage = null;
			
			if (prev >= 0) {
				prevPage = pages.get(prev);
				prevPage.loaded = false;
			}
			
			if (next < BookDB.NUMBER_OF_ARTICLES) {
				nextPage = pages.get(next);
				nextPage.loaded = false;
			}
			
			page.loaded = false;
			
			return page;
		}
	
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return BookDB.NUMBER_OF_ARTICLES;
		}
		
	}

	@Override
	public void OnTOCItemSelected(int ID) {
		// TODO Auto-generated method stub
		PageFragment page = this.pages.get(article-1); 
		page.webView.setVisibility(View.INVISIBLE);
		page.displayArticleAtIndex(article-1, ID);
		tocHandler.hideTOC();
	}

	@Override
	public void pageLoaded(int position) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void titleClicked(int article, int tocID) {
		// TODO Auto-generated method stub
		tocHandler.showTOCForArticle(article, tocID);
	}
	
	@Override
	public boolean onKeyUp(int keyCode, KeyEvent msg) {

	     switch(keyCode) {
	     case(KeyEvent.KEYCODE_BACK):
	    	 	handleNavBack();
				return true;    
	     }
	     return false;
	}
	
	private void handleNavBack () {
		if (Pages.this.isTaskRoot()) {
			// If list-guide is at the root of the activity stack, then open the Chooser page screen.
			Intent intent = new Intent(this, ChooserPageScreen.class);
          	intent.putExtra(NameFromList, 1);
			startActivity(intent);
			overridePendingTransition(0, 0);
			finish();
		} else {
			finish();
			overridePendingTransition(0, 0);
		}
	}
	
	private long getTimestamp() {
		SharedPreferences getTS = getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
		return getTS.getLong("timestamp", 0);
	}
	
	public void logout() {
		SharedPreferences pref = getApplicationContext().getSharedPreferences("LOGIN", 0);
		Editor editor = pref.edit();
		editor.remove("username");
		editor.remove("password");
		editor.remove("udid");
		editor.remove("timestamp");
		editor.commit();
		Intent intent  = new Intent(getBaseContext(), SplashScreen.class);
		startActivity(intent);
		finish();
	}
	
//	@Override
//	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {  
//
//	    // Get the list
//	    CsWebView web = (CsWebView)v;
//	    
//	    //Log.d("Pages", "Created context menu.");
//	}
	
//	private void hidenTopViewIfNotKitKat()
//	{
//		if(Build.VERSION.SDK_INT < 19)
//		{
//			mTextTop.setVisibility(View.GONE);
//		}else
//		{
//			isKitKat= true;
//			mTextTop.setOnTouchListener(this);
//			mTextTop.setOnLongClickListener(this);
//		}
//	}
	
//	private String stringFromInputStream (InputStream stream) {
//		InputStreamReader is=new InputStreamReader(stream);
//		BufferedReader br=new BufferedReader(is);
//		String read = "";
//		StringBuffer sb = new StringBuffer(read);
//		try {
//			while((read = br.readLine()) != null) {
//			    sb.append(read);
//			}
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		return sb.toString();
//	}
	
//	private void loadInitialArticles (int startingArticle) {
//		// The Activity should first load the article specified by "startingArticle" and then load the previous and next articles.
//		if (startingArticle > 1) {
//			ArrayList<BookElement> arr = bookDB.elementsWithArticle(startingArticle - 1);
//			if (arr != null && arr.size()>0) {
//				updateTitleForWebView(arr.get(0).ID, web1);
//				web1.ID = arr.get(0).ID;
//				web1.article = startingArticle - 1;
//				
//				displayArticle(startingArticle - 1, web1, arr.get(0).ID);
//			}
//		}
//		if (startingArticle < BookDB.NUMBER_OF_ARTICLES) {
//			ArrayList<BookElement> arr = bookDB.elementsWithArticle(startingArticle + 1);
//			if (arr != null && arr.size()>0) {
//				updateTitleForWebView(arr.get(0).ID, web2);
//				web2.ID = arr.get(0).ID;
//				web2.article = startingArticle + 1;
//				
//				displayArticle(startingArticle + 1, web2, arr.get(0).ID);
//			}
//		}
//		
//		updateTitleForWebView(web3.ID, web3);
//		displayArticle(startingArticle, web3);
//	}
//	
//	private void updateArticleHash (CsWebView wv, BookElement elem) {
//		if (wv.getId() == R.id.web1) {
//			web1Hash.add(elem.ID);
//			return;
//		} else if (wv.getId() == R.id.web2) {
//			web2Hash.add(elem.ID);
//			return;
//		} else if (wv.getId() == R.id.web3) {
//			web3Hash.add(elem.ID);
//			return;
//		}
//	}
//	
//	private ArrayList<Integer> getArticleHash (CsWebView wv) {
//		if (wv.getId() == R.id.web1) {
//			return web1Hash;
//		} else if (wv.getId() == R.id.web2) {	
//			return web2Hash;
//		} else if (wv.getId() == R.id.web3) {
//			return web3Hash;
//		}
//		
//		return null;
//	}
//	
//	private void displayArticle (int tempArticle, CsWebView wv, int id) {
//
//		LoadDataTask load = new LoadDataTask(wv, tempArticle, id);
//		load.execute("");
//		//wv.loadDataWithBaseURL("file:///android_asset/html/", finalHtml, "text/html", "UTF-8", null);
//	}
//	
//	private void displayArticle (int tempArticle, CsWebView wv) {
//		this.displayArticle(tempArticle, wv, wv.ID);
//	}
//	
//	private class LoadDataTask extends AsyncTask <String, Void, String> {
//		
//		CsWebView wv;
//		int tempArticle;
//		int id;
//		
//		LoadDataTask (CsWebView web, int article, int ID) {
//			this.wv = web;
//			this.id = ID;
//			this.tempArticle = article;
//		}
//		
//		@Override
//		protected String doInBackground(String[] arg0) {
//			// TODO Auto-generated method stub
//			StringBuilder sb = new StringBuilder();
//			sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>");
//			sb.append("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.1//EN\" \"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">");
//			sb.append("<html xmlns=\"http://www.w3.org/1999/xhtml\">");
//			sb.append("<head>");
//			sb.append("<title>SRL2_Balk_8</title>");
//			sb.append("<link href=\"css/idGeneratedStyles.css\" rel=\"stylesheet\" type=\"text/css\" />");
//			sb.append("<link href=\"css/annotator.min.css\" rel=\"stylesheet\" type=\"text/css\" />");
//			sb.append("<link href=\"css/annotator.touch.css\" rel=\"stylesheet\" type=\"text/css\" />");
//			sb.append("<script src=\"js/jquery.min.js\"></script>");
//			sb.append("<script src=\"js/rangy-core.js\"></script>");
//			sb.append("<script src=\"js/rangy-cssclassapplier.js\"></script>");
//			sb.append("<script src=\"js/android.selection.js\"></script>");
//			sb.append("<script src=\"js/rangy-serializer.js\"></script>");	
//			sb.append("</head>");
//			sb.append("<body xml:lang=\"sv-SE\">");
//			sb.append("<div id=\"content\" class=\"Basic-Text-Frame\">");
//		    
//		    if (bookDB == null) return null; // Database object needs to be set before calling this
//		    
//		    // Add initial page content
//		    ArrayList<BookElement> elemsArray;
//		    BookElement elem;
//		    String noteHtml = null;
//		    if (id > 0) {
//		    	////Log.d("Pages", "Getting elements for id - "+id+" and article - "+tempArticle);
//		        elemsArray = bookDB.elementsWithArticleContainingID(tempArticle, id);
//		        elem = elemsArray.get(0);
////		        if (isFirstOpenedPage) {
//		        	wv.startID = elem.ID;
//			        if (wv.startID == id) // Beginning of tempArticle - no more above
//			        	wv.startID = 0;
////		        }
//		    } else {
//		        elemsArray = bookDB.firstElementsOfArticle(tempArticle);
////		        if (isFirstOpenedPage) {
//		        wv.startID = 0;
////		        }
//		    }
//		    //String html = article_head;
//		    String html = "";
//		    for (BookElement elm : elemsArray) {
//		    	//////Log.d("Pages", "Should print - \n"+elm.html);
//		        html += elm.html;
//		        updateArticleHash(wv, elm);
//		        
//		        noteHtml = htmlForNoteWithID(elm.ID);
//		        if (noteHtml != null)
//		            html += noteHtml;
//		    }
//		    elem = elemsArray.get(elemsArray.size()-1);
////		    if (isFirstOpenedPage) {
//		    	////Log.e("Pages", "Setting end ID to - "+elem.ID);
//		    	wv.endID = elem.ID;
////	        }
//		    
//		    sb.append(html);
//		    
//		    sb.append("</div>");
//			sb.append("</body>");
//			sb.append("</html>");
//
//			String finalHtml = sb.toString();
//			
//			return finalHtml;
//		}
//		
//		@Override
//		protected void onPostExecute(String result) {
//			// TODO Auto-generated method stub
//			super.onPostExecute(result);
//			
//			wv.setTag("initialPage");
//			
//			wv.loadDataWithBaseURL("file:///android_asset/html/", result,
//					"text/html", "UTF-8", null);
//		}
//	}
//	
//	public String htmlForNoteWithID (int ID) {
//	    String html = null;
////	    BookmarkHistoryItem * item = [self.bookmarkHistory getNoteWithID:ID];
////	    if (item && item.text && item.text.length)
////	        html = [NSString stringWithFormat:@"<p id=\"e%d\" class=\"NJNote\">%@</p>", (int)ID, item.text];
//	    return html;
//	}
//
//	
//	private class SizeChangedObserver implements CurlView.SizeChangedObserver {
//		@Override
//		public void onDrawed(final int isPageChanged) {
//			// TODO Auto-generated method stub
//			if (!CsWebView.isVisible) {
//				runOnUiThread(new Runnable() {
//					@Override
//					public void run() {
//						// TODO Auto-generated method stub
//						if ((mCurlView.getCurrentIndex() >= 0 && mCurlView
//								.getCurrentIndex() < BookDB.NUMBER_OF_ARTICLES)) {
//							if (isPageChanged != 0) {
//								if(isPageChanged == isNext)
//								{
//									onNext();
//								}else
//								{
//									onBack();
//								}
//							}
//							CurlView.isTransparent = true;
//							hideSurfaceView();
//						}
//					}
//				});
//			}
//		}
//
//		@Override
//		public void sendEvent(MotionEvent me) {
//			// TODO Auto-generated method stub
//			WebView web = (WebView)((ViewGroup)frmInner.getChildAt(2)).getChildAt(1); 
//			web.onTouchEvent(me);
//		}
//	}
//	
//	private void onNext()
//	{
//		View v;
//		if(flag==isBack)
//		{
//			v = frmInner.getChildAt(1);
//		}else
//		{
//			v = frmInner.getChildAt(0);
//		}
//		frmInner.removeView(v);
//		frmInner.addView(v);
//		int index =mCurlView.getCurrentIndex(); 
//		if(index+2<=BookDB.NUMBER_OF_ARTICLES)
//		{
//			CsWebView web = (CsWebView)((ViewGroup)frmInner.getChildAt(0)).getChildAt(1); 
//			////Log.d("PAGES", "Gonna load about:blank - NEXT");
//			// Clear all data connected to the webview.
//			this.getArticleHash(web).clear();
//			web.setTag("about:blank");
//			web.loadUrl("about:blank");
//		}
//		flag = isNext;
//	}
//	
//	private void onBack()
//	{
//		View v;
//		if(flag==isNext)
//		{
//			v = frmInner.getChildAt(1);
//		}else
//		{
//			v = frmInner.getChildAt(0);
//		}
//		frmInner.removeView(v);
//		frmInner.addView(v);
//		int index =mCurlView.getCurrentIndex(); 
//		if(index-1>= 0)
//		{	
//			CsWebView web = (CsWebView)((ViewGroup)frmInner.getChildAt(0)).getChildAt(1);
//			////Log.d("PAGES", "Gonna load about:blank - BACK");
//			// Clear all data connected to the webview.
//			this.getArticleHash(web).clear();
//			web.setTag("about:blank");
//			web.loadUrl("about:blank");
//		}
//		flag = isBack;
//	}
//	
//	private View getNextView()
//	{
//		ViewGroup v;
//		if(flag==isNext)
//		{
//			v =  (ViewGroup)frmInner.getChildAt(0);
//		}else
//		{
//			v =  (ViewGroup)frmInner.getChildAt(1);
//		}
//		return v;
//	}
//	
//	private View getBackView()
//	{
//		ViewGroup v;
//		if(flag==isBack)
//		{
//			v =  (ViewGroup)frmInner.getChildAt(0);
//		}else
//		{
//			v =  (ViewGroup)frmInner.getChildAt(1);
//		}
//		return v;
//	}
//	
//	Bitmap b;
//	private class BitmapProvider implements CurlView.BitmapProvider {
//		@Override
//		public int getBitmapCount() {
//			return BookDB.NUMBER_OF_ARTICLES;
//		}
//
//		@Override
//		public Bitmap getBitmap(int width, int height, final int index) {
//			final CountDownLatch countDown = new CountDownLatch(1);
//			if (index > mCurlView.getCurrentIndex()) {
//				b = pictureDrawable2Bitmap(getNextView());
//			}else if (index < mCurlView.getCurrentIndex()) {
//				b = pictureDrawable2Bitmap(getBackView());
//			} else {
//				b = pictureDrawable2Bitmap(frmInner.getChildAt(2));
//			}
//			return b;
//		}
//	}
//
//	private Bitmap pictureDrawable2Bitmap(final View view) {
//		//Log.i("Pages", "Gonna draw on canvas 1");
//		Bitmap bitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(),
//				Bitmap.Config.ARGB_8888);
//		final Canvas canvas = new Canvas(bitmap);
//
////		Pages.this.runOnUiThread(new Runnable() {
////			@Override
////            public void run() {
//        		if (canvas != null || view != null) {
//        			//Log.i("Pages", "Gonna draw on canvas");
//        			view.draw(canvas);
//        		}
////            }
////        });
//		return bitmap;
//	}
//	
//	private void showSurfaceView() {
//		////Log.e("Pages", "Current page changed");
//		mCurlView.currentPageChanged();
//		mCurlView.setAlpha(0f);
//		mCurlView.animate().alpha(1f).setDuration(50);
//	}
//
//	private void hideSurfaceView() {
//		mCurlView.setAlpha(1f);
//		mCurlView.animate().alpha(0f).setDuration(50);
//		CsWebView.isVisible = true;
//		
//	}
//
//	@Override
//	public void showOnTop(boolean isTop, float width) {
//		// TODO Auto-generated method stub
//		if (isTop) {			
//			if(width>0 && mCurlView.getCurrentIndex()==0)
//			{
//				CsWebView.isVisible = true;
//				CurlView.isTransparent = true;
//				return;
//			}
//			showSurfaceView();
//		} else {
//			hideSurfaceView();
//		}
//	}
//
//	@Override
//	public void hideOnTop(boolean isHide) {
//		// TODO Auto-generated method stub
//		if(isHide)
//		{
//			mCurlView.setVisibility(View.GONE);
//		}else
//		{
//			mCurlView.setVisibility(View.VISIBLE);
//		}
//	}
//	
//	@Override
//	public void sendEvent(MotionEvent event) {
//		// TODO Auto-generated method stub
//		event.setAction(MotionEvent.ACTION_DOWN);
//		mCurlView.onTouchEvent(event);
//	}
//
//	@Override
//	public void realPage(WebView wv) {
//		// TODO Auto-generated method stub
//		
//		CsWebView web = (CsWebView)wv;
//		
//		int index = -1;
//		if(flag == isBack)
//		{
//			////Log.d("Pages","realPage(), index before loading previous - "+mCurlView.getCurrentIndex());
//			index = mCurlView.getCurrentIndex();
//		}else
//		{
//			////Log.d("Pages","realPage(), index before loading next - "+mCurlView.getCurrentIndex());
//			index = mCurlView.getCurrentIndex() + 2; 
//		}
//		
//		////Log.d("Pages","realPage() - Should load index - "+index);
//		
//		if(index >= 0 && index <BookDB.NUMBER_OF_ARTICLES)
//		{
//			////Log.d("PAGES", "Loading file at index - "+index);
//			web.setTag(index);
//			web.article = index;
//			////Log.e("Pages", "Setting endID to 0");
//			web.endID = 0;
//			ArrayList<BookElement> arr = bookDB.elementsWithArticle(index);
//			web.ID = arr.get(0).ID;
//			
//			switch (web.getId()) {
//			case R.id.web1:
//				////Log.d("PAGES", "Loading for web1");
//				break;
//			case R.id.web2:
//				////Log.d("PAGES", "Loading for web2");
//				break;
//			case R.id.web3:
//				////Log.d("PAGES", "Loading for web3");
//				break;
//			default:
//				break;
//			}
//			
//			this.getArticleHash((CsWebView)web).clear();
//			web.loadUrl("about:blank");
//			web.clearCache(true);
//			web.clearHistory();
//			this.updateTitleForWebView(web.ID, (CsWebView)web);
//			displayArticle(web.article, (CsWebView)web);
//			//web.loadUrl(mFile[index]);
//		}
//	}
//
//	@Override
//	public boolean onTouch(View v, MotionEvent event) {
//		// TODO Auto-generated method stub
//		////Log.d("Pages", "On Touch");
//		if ((event.getAction() == MotionEvent.ACTION_DOWN) && (v.equals(title1) || v.equals(title2) || v.equals(title3))) {
//			////Log.d("Pages", "title touched");
//		} else {
//			mCurlView.onTouchEvent(event);
//		}
//		return true;
//	}
//
//	private boolean isPointInsideView(float x, float y, View view){
//	    int location[] = new int[2];
//	    view.getLocationOnScreen(location);
//	    int viewX = location[0];
//	    int viewY = location[1];
//
//	    //point is inside view bounds
//	    if(( x > viewX && x < (viewX + view.getWidth())) &&
//	            ( y > viewY && y < (viewY + view.getHeight()))){
//	        return true;
//	    } else {
//	        return false;
//	    }
//	}
//	
//	@Override
//	public boolean onLongClick(View v) {
//		// TODO Auto-generated method stub
//		return true;
//	}
//	
//	@Override
//	public void onBackPressed() {
//		// TODO Auto-generated method stub
//		super.onBackPressed();
//		// Check if the activity is the root of the stack.
//		if (this.isTaskRoot()) {
//			// If list-guide is at the root of the activity stack, then open the Chooser page screen.
//			openChooser();
//		}
//	}
//	
//	public void openChooser () {
//		Intent intent = new Intent(Pages.this, ChooserPageScreen.class);
//		intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
//		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//		startActivity(intent);
//		this.finish();
//	}
//
//	@Override
//	public void runScript(WebView wv) {
//		// TODO Auto-generated method stub
//		
//		CsWebView web = (CsWebView)wv;
//		
//		// When the webview is populated using the displayArticle() 
//		// function, the webview position needs to be set at the position
//		// of the selected element.
//		this.gotoElementWithID(web.ID, (CsWebView) web, 60);
//		
//		// Enable webview scrolling and its scroll callbacks. 
//		// This is in case of "prepending" content where there 
//		// is risk of loading more than necessary and it ends up 
//		// setting the webview position to an wrong one.
//		enableWebViewScroll();
//	}
//	
//	private void gotoElementWithID(int id, CsWebView web) {
//		this.gotoElementWithID(id, web, 0);
//	}
//	
//	private void gotoElementWithID(int id, CsWebView web, int offset) {
////		String url = "javascript:(function () {" +
////	    		"var elem = document.getElementById('e"+id+"');" +
////	    		"var x = 0;" +
////	    		"var y = 0;" +
////	    		"while (elem != null) {" +
////	    		"x += elem.offsetLeft;" +
////	    		"y += elem.offsetTop;" +
////	    		"elem = elem.offsetParent;" +
////	    		"}" +
////	    		"window.scrollTo(x, y);" +
////	    		"})()";
//		
//		String url2 = "javascript:var p=$('#e"+web.ID+"').position().top - "+offset+"; $(window).scrollTop(p);";
////		if (web.getId() == R.id.web3) {
//			////Log.d("Pages", "Should scroll to position - "+ id);
//			web.loadUrl(url2);
////		}
//	}
//
//	@Override
//	public void updateTOCTitle(String id) {
//		// TODO Auto-generated method stub
//		
//	}
//
//	@Override
//	public Activity getActivity() {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//	@Override
//	public void onScroll(CsWebView wv, int l, int t) {
//		// TODO Auto-generated method stub
//		float progress = calculateProgress(wv);
////		////Log.d("Pages", currentWebView(wv) + " scrolling with progress - "+progress);
//		if (progress < 0) {
//			////Log.d("Pages", currentWebView(wv) + " scrolling - Reached top");
////			if (!loadingContent) {
//				////Log.d("Pages", "loading top content");
//				appendContent(wv, true);
////			}
//		} else if (progress >= 2) {
//			////Log.d("Pages", currentWebView(wv) + " scrolling - Reached bottom");
////			if (!loadingContent) {
//				appendContent(wv, false);
////			}
//		}
//	}
//	
//	private void appendContent (final CsWebView wv, boolean toTop) {
//		if (toTop) {
//			if (wv.startID > 0) {
//				// Append elements to the top.
//				if (!wv.loadingContent) {
//					////Log.d("Pages", "Loading top content for article - "+ wv.article);
//					
//					// Disable scrolling on the web view by overriding touch events (and also the scroll events callback to be certain)
//					// and then load the top content. Enable the scroll callbacks and touch events again in the runScript() callback function.
//					disableWebViewScroll(); 
//					
//					wv.ID = wv.startID;
//					// Clear all previously loaded ID's.
//					this.getArticleHash(wv).clear();
//					this.displayArticle(wv.article, wv);
//				}
//			} else {
//				////Log.d("Pages", "No elements to load.");
//			}							
//		} else {
//			if (wv.endID > 0) {
//				int a = mCurlView.getCurrentIndex()+1;
//				ArrayList<BookElement> elementArray = bookDB.elementsWithArticleStartingWithID(wv.article, wv.endID + 1);
//				if (elementArray == null || elementArray.size() == 0) {
//					////Log.d("Pages", "Not loading bottom content for article - "+ wv.article + " and endID - "+ wv.endID);
//					wv.endID = 0;
//			        return;
//			    } else {
//			    	////Log.d("Pages", "endID < 0");
//			    }
//				////Log.d("Pages", "Loading bottom content for article - "+ wv.article);
////				loadingContent = true;
//			    BookElement element;
//			    String js = "";
//			    String noteHtml;
//			    
//			    String html = "";
//			    
//			    for (BookElement elem: elementArray) {						    	
//			    	// Check if the article has already been loaded in the webview. If not, add it and avoid repetitions.
//			    	if (!this.getArticleHash(wv).contains(elem.ID)) {
//			    		html += elem.html.replace("\\", "\\\\\\");
//			    	} else {
//			    		////Log.d("Article Hash", "Already loaded element with ID - "+elem.ID);
//			    	}
//			    	
//			        //js = "javascript:$(\"#content\").append(\""+html+"\");";
//			       
////			        noteHtml = [self htmlForNoteWithID:elem.ID];
////			        if (noteHtml) {
////			            js = [NSString stringWithFormat:@"$(\"#content\").append(\"%@\");", [noteHtml stringByReplacingOccurrencesOfString:@"\"" withString:@"\\\""]];
////			            [self.webview stringByEvaluatingJavaScriptFromString:js];
////			        }
//			    }
//			    
//			    js = "javascript:var FunctionOne = function () {" +			        					    
//		        		"document.getElementsByTagName('div')[0].innerHTML = document.getElementsByTagName('div')[0].innerHTML + '"+html+"';" +
//		        		"return 'blah';" +	
//		        		"};" +
//		        		"var FunctionTwo = function () {" +
//		        		//"Android.setJSDone();" +
//		        		"};" +
//		        		"FunctionOne().done(FunctionTwo());";
//			    
//			   	wv.loadUrl(js);
//			   	element = elementArray.get(elementArray.size()-1);
//			   	wv.endID = element.ID;
//			}
//		}
//	}
//		
//	private void removeScrollCallbacks () {
//		web3.setOnScrollChangedCallback(null);
//		web2.setOnScrollChangedCallback(null);
//		web1.setOnScrollChangedCallback(null);
//	}
//	
//	private void setScrollCallbacks () {
//		web3.setOnScrollChangedCallback(this);
//		web2.setOnScrollChangedCallback(this);
//		web1.setOnScrollChangedCallback(this);
//	}
//	
//	private void disableWebViewScroll () {
//		this.removeScrollCallbacks();
//		// Disable the webView scroll by overlaying a view which would override the webView's touch events.
//		LinearLayout linearLayout = new LinearLayout(this);
//		linearLayout.setTag("overlay_view");
//		//linearLayout.setBackgroundResource(R.color.white_transparent);
//		RelativeLayout.LayoutParams relativeParams = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
//		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
//
//		linearLayout.setOnTouchListener(new View.OnTouchListener() {
//			
//			@Override
//			public boolean onTouch(View arg0, MotionEvent arg1) {
//				// TODO Auto-generated method stub
//				return true;
//			}
//		});
//		
//		parentView.addView(linearLayout, relativeParams);
//	}
//	
//	private void enableWebViewScroll () {
//		this.setScrollCallbacks();
//		for (int i = 0; i < parentView.getChildCount(); i++) {
//			View child = parentView.getChildAt(i);
//			if (child.getTag() != null) {
//				if (child.getTag().equals("overlay_view")) {
//					// Remove the overlay view.
//					////Log.d("Pages", "Removing overlay view and enabling webview scroll.");
//					parentView.removeView(child);
//				}
//			}
//		}
//	}
//	
//	private String currentWebView (CsWebView wv) {
//		
//		if (wv.getId() == R.id.web1) {
//			return "web1";
//		} else if (wv.getId() == R.id.web2) {
//			return "web2";
//		} else if (wv.getId() == R.id.web3) {
//			return "web3";
//		}
//		
//		return "";
//	}
//	
//	private CsWebView webViewForId (int id) {
//		if (id == R.id.web1) {
//			return web1;
//		} else if (id == R.id.web2) {
//			return web2;
//		} else if (id == R.id.web3) {
//			return web3;
//		}
//		
//		return null;
//	}
//	
//	private float calculateProgress(WebView content) {
//		float contentHeight = (float) Math.floor(content.getContentHeight() * content.getScaleY());
//		float currentScrollPosition = content.getScrollY();
//		float displayedContentHeight = content.getMeasuredHeight();
//		float effectiveHeight = (contentHeight - displayedContentHeight);
//		float percentWebview = currentScrollPosition / effectiveHeight;
//	    return percentWebview;
//	}
//
//	private void updateTitleForWebView (int topElementID, CsWebView web) {
//		BookElement elem = bookDB.elementWithID(topElementID);
//    	
//    	String title = "";
//    	
//    	if (elem != null) {
//    		web.tocID = topElementID;
//    		
//    		if (elem.weight == 1 || elem.weight == 2 || elem.weight == 5) { // top level
//                //self.tocLabel.font = [UIFont fontWithName:@"MyriadPro-Regular" size:24];
//                //self.tocLabel.numberOfLines = 1;
//                title = bookDB.articleNameForElement(elem);
//            } else {
//                //self.tocLabel.font = [UIFont fontWithName:@"MyriadPro-Regular" size:16];
//                //self.tocLabel.numberOfLines = 2;
//                title = bookDB.articleNameForElement(elem) + "\n" + bookDB.lawNameForElement(elem);                    
//            }
//    	} else {
//    		web.tocID = 0;
//    	}   
// 
//    	if (web != null) {
//    		updateArticleTitle(web, title);
//    	}
//	}
//	
//	private void updateArticleTitle (final CsWebView web,final String title) {
//		runOnUiThread(new Runnable() {
//            public void run() {
//            	AutoFitTextView tv = getTitleTextViewForWebView(web);
//            	if (tv != null) {
//            		tv.setText(title);
//            	}
//            }
//        });
//	}
//	
//	public void onJSRequestCompleted(final CsWebView webView) {
//		// TODO Auto-generated method stub
//		
//		runOnUiThread(new Runnable() {
//            public void run() {
//            	////Log.d("Pages - JS", currentWebView(webView) + "Javascript request finished");
//            }
//        });
//	}
//	
//	private AutoFitTextView getTitleTextViewForWebView (CsWebView wv) {
//		if (wv.getId() == R.id.web1) {
//			return title1;
//		} else if (wv.getId() == R.id.web2) {
//			return title2;
//		} else if (wv.getId() == R.id.web3) {
//			return title3;
//		}
//		
//		return null;
//	}
//	
//	public class CsJavaScriptHandler {
//        public CsJavaScriptHandler()  {
//        }
//        
//        @JavascriptInterface 
//        public void setJSDone () {
//        	////Log.d("Pages - CSW", "JS request completed");
//        	//onJSRequestCompleted(wv);
//        	enableWebViewScroll();
//        }
//        
//        @JavascriptInterface
//        public void updateTitle (String idString, int webViewID) {
//        	////Log.d("Pages - CSW", "Current top ID - "+idString);
//        	
//        	int topElementID = Integer.parseInt(idString.replace("e", ""));
//        	CsWebView web = webViewForId(webViewID);   
//        	
//        	updateTitleForWebView(topElementID, web);
//        }
//    }
//
//	@Override
//	public void OnTOCItemSelected(int ID) {
//		// TODO Auto-generated method stub
//		
//		int tempArticle = bookDB.articleForID(ID);
//		CsWebView web = web3;
//		if (web1.article == tempArticle) {
//			web = web1;
//		} else if (web2.article == tempArticle) {
//			web = web2;
//		}
//		
//		web.loadUrl("about:blank");
//		web.clearHistory();
//		web.clearCache(true);		
//		
//		if (!web.loadingContent) {
////			////Log.d("Pages", "Loading top content for article - "+ wv.article);
//			
//			// Disable scrolling on the web view by overriding touch events (and also the scroll events callback to be certain)
//			// and then load the top content. Enable the scroll callbacks and touch events again in the runScript() callback function.
//			disableWebViewScroll(); 
//			web.ID = ID;
//			web.setTag("initialPage");
//			this.updateTitleForWebView(ID, web);
//			this.displayArticle(tempArticle, web);
//			tocHandler.hideTOC();
//		}
//	}
//
//	@Override
//	public void touchedPoint(CsWebView web, float x, float y) {
//		// TODO Auto-generated method stub
//		
//		switch (web.getId()) {
//		
//		case R.id.web1:
//			if (this.isPointInsideView(x, y, title1)) {
//				////Log.d("Pages", "Title 1 selected");
//				tocHandler.showTOCForArticle(web1.article);
//			}
//			break;
//			
//		case R.id.web2:
//			if (this.isPointInsideView(x, y, title2)) {
//				////Log.d("Pages", "Title 2 selected");
//				tocHandler.showTOCForArticle(web2.article);
//			}
//			break;
//		
//		case R.id.web3:
//			if (this.isPointInsideView(x, y, title3)) {
//				////Log.d("Pages", "Title 3 selected");
//				tocHandler.showTOCForArticle(web3.article);
//			}
//			break;
//		
//		default:
//			break;
//		}
//	}
}


// Code to dynamically prepend content to the page.

//loadingContent = true;
//int returnLocation = startID; // Once the elements before the current startID are loaded, the webview position must be reset to returnLocation.
//
//startID -= 50;
//if (startID < 0) {
//	startID = 0; 
//}
//
//ArrayList <BookElement> elementsToLoad = bookDB.elementsWithArticleBetweenIDs(article, startID, returnLocation - 1);
//
//if (!elementsToLoad.isEmpty()) {
//	String html = "";
//	for (BookElement elem: elementsToLoad) {						    	
//    	// Check if the article has already been loaded in the webview. If not, add it and avoid repetitions.
//    	if (!this.getArticleHash(wv).contains(elem.ID)) {
//    		html += elem.html.replace("\\", "\\\\\\");
//    	} else {
//    		////Log.d("Article Hash", "Already loaded element with ID - "+elem.ID);
//    	}
//    	
//        //js = "javascript:$(\"#content\").append(\""+html+"\");";
//       
////        noteHtml = [self htmlForNoteWithID:elem.ID];
////        if (noteHtml) {
////            js = [NSString stringWithFormat:@"$(\"#content\").append(\"%@\");", [noteHtml stringByReplacingOccurrencesOfString:@"\"" withString:@"\\\""]];
////            [self.webview stringByEvaluatingJavaScriptFromString:js];
////        }
//    }
//	
//	int prevX = wv.getScrollX(), prevY = wv.getScrollY();
//	
//	String js = "javascript:var FunctionOne = function () {" +			        					    
//    		"document.getElementsByTagName('body')[0].innerHTML = '"+html+"' + document.getElementsByTagName('body')[0].innerHTML;" +
//    		"return 'blah';" +	
//    		"};" +
//    		"var FunctionTwo = function () {" +
//    		"Android.scrollWebViewToPosAfterJSDone("+wv+", "+prevX+", "+prevY+");" +
//    		"};" +
//    		//"FunctionOne().done(FunctionTwo());" +
//    		"FunctionOne();";
//	
//	wv.loadUrl(js);
//	
//	this.gotoPosition(wv, prevX, prevY);
//	
////	this.gotoElementWithID(returnLocation, wv);
//}