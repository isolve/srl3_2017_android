package se.isolve.srl3.reader;

import java.util.ArrayList;

import se.isolve.srl3.R;
import se.isolve.srl3.database.BookDB;
import se.isolve.srl3.database.BookElement;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ListView;

public class PagesTOCHandler {

	private ListView toc;
	private Context context;
	private TOCEventListener tocListener;
	private BookDB bookDB;
	private PagesTOCHandlerAdapter adapter;
	private ArrayList<BookElement> tocItems;
	
	PagesTOCHandler (Context context, ListView list, BookDB bdb) {
		this.toc = list;
		this.context = context;
		this.bookDB = bdb;
		
		toc.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		
		// Initialize adapter and set it to the ListView.
		adapter = new PagesTOCHandlerAdapter();
		tocItems = new ArrayList<BookElement>();
		toc.setAdapter(adapter);
	}
	
	public void showTOCForArticle (int article, int tocID) {
		toc.setVisibility(View.VISIBLE);
		
		// Get the items to be shown in the TOC.
		tocItems = bookDB.getLawsChaptersForArticle(article);
		
		adapter.notifyDataSetChanged();
		
		this.scrollToTocId(tocID);
		
		handleTOCItemSelected();
	}
	
	private void scrollToTocId (final int tocId) {
		int i = 0;
	    for (BookElement elem : tocItems) {
	        if (elem.ID >= tocId)
	            break;
	        i++;
	    }
	    if (i > 0)
	        i--;
	    
	    //Log.d("TOCHandler", "Scroll to position - "+i+". With no of toc items - "+tocItems.size());
	    
	    final int position = i;
	    
	    toc.post(new Runnable() {

	        @Override
	        public void run() {
	        	
	            toc.setSelection(position);
	            View rowView = toc.getChildAt(position - toc.getFirstVisiblePosition());
	            if (rowView != null) {
	            	rowView.setBackgroundResource(R.color.light_gray);
	            }
	        }
	    });
//	    
//	    toc.setSelection(i);
	}
	
	private void handleTOCItemSelected () {
		toc.setOnItemClickListener(new OnItemClickListener () {

			@Override
			public void onItemClick(AdapterView<?> view, View convertView, int pos,
					long arg3) {
				// TODO Auto-generated method stub
				BookElement elem = tocItems.get(pos);
				tocListener.OnTOCItemSelected(elem.ID);
			}});
	}
	
	public void hideTOC () {
		// Clear off the TOC when the list hides.
		clearTOCItems();
		toc.setVisibility(View.GONE);
	}
	
	public boolean tocShown () {
		return (toc.getVisibility() == View.VISIBLE);
	}
	
	public void clearTOCItems () {
		if (tocItems != null) {
			tocItems.clear();
		}
		if (adapter != null) {
			adapter.notifyDataSetChanged();
		}
	}
	
	public void setTOCEventListener (TOCEventListener listener) {
		this.tocListener = listener;
	}
	
	public interface TOCEventListener {
		public void OnTOCItemSelected(int ID);
	}
	
	private class PagesTOCHandlerAdapter extends BaseAdapter {

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return tocItems.size();
		}

		@Override
		public BookElement getItem(int pos) {
			// TODO Auto-generated method stub
			return tocItems.get(pos);
		}

		@Override
		public long getItemId(int pos) {
			// TODO Auto-generated method stub
			return tocItems.get(pos).getId();
		}

		@Override
		public View getView(int pos, View convertView, ViewGroup viewGroup) {
			// TODO Auto-generated method stub
			//if (convertView == null) {
				LayoutInflater inflater = ((Activity) context).getLayoutInflater();
				convertView = inflater.inflate(R.layout.toc_list_item, null);
				// Get the book element at the position and set the text.
//				convertView.setTag(Integer.toString(pos));
				viewGroup.setTag(Integer.toString(pos));
				BookElement elem = getItem(pos);
				AutoFitTextView tv = (AutoFitTextView) convertView.findViewById(R.id.tocText);
				if (tv != null) {
					tv.setMinTextSize(context.getResources().getDimensionPixelSize(R.dimen.sizetextlistguide));
					tv.setMaxTextSize(context.getResources().getDimensionPixelSize(R.dimen.sizetextlistguide));
					tv.applyFont(context);
					tv.setText(elem.text);
				}
			//}
			
			return convertView;
		}
	} 
}
