package se.isolve.srl3.reader;

import java.util.ArrayList;

import se.isolve.lib.pagebased.CsWebView;
import se.isolve.lib.pagebased.CsWebView.AnchorIDCallBack;
import se.isolve.lib.pagebased.CsWebView.ContextMenuListener;
import se.isolve.lib.pagebased.CsWebView.JavaScriptCallback;
import se.isolve.lib.pagebased.CsWebView.OnScrollChangedCallback;
import se.isolve.lib.pagebased.CsWebView.SurfaceViewCallback;
import se.isolve.srl3.R;
import se.isolve.srl3.SRL3ApplicationClass;
import se.isolve.srl3.database.BookDB;
import se.isolve.srl3.database.BookElement;
import utils.NotesDialogFragment;
import utils.SRLUtils;
import android.annotation.SuppressLint;
import android.app.FragmentManager;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

@SuppressLint("ValidFragment")
public class PageFragment extends Fragment implements SurfaceViewCallback, OnTouchListener, OnScrollChangedCallback, ContextMenuListener, JavaScriptCallback, NotesDialogFragment.NotesDialogListener, AnchorIDCallBack {


	public CsWebView webView;
	public AutoFitTextView title;
	public boolean loaded, shouldLoadFirstID = true;
	public int article, ID, position, startID, endID, tocID;
	
	private float lastScrollPosition;

	private Context context;
	private FragmentActivity activity;
	private BookDB bookDB;
	private RelativeLayout parentView;
	private String gotIDfromMarkedText;
	private boolean isNotes = false;

	private PageFragmentListener pageFragmentListener;
	
	public interface PageFragmentListener {
		public void pageLoaded(int position);
		public void titleClicked(int article, int tocId);
	}

	public PageFragment (Context c, FragmentActivity a, BookDB bdb, int pos) {
		this.context = c;
		this.bookDB = bdb;
		this.activity = a;
		this.position = pos;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		ViewGroup rootView = (ViewGroup) inflater.inflate(
				R.layout.page_fragment_view, container, false);
		//Log.d("PFr", "Creating page fragment");

		return rootView;
	}

	@Override    
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		View rootView = getView();

		webView = (CsWebView) rootView.findViewById(R.id.web1);
		title = (AutoFitTextView) rootView.findViewById(R.id.txtToc1);
		parentView = (RelativeLayout) rootView.findViewById(R.id.frmWeb1);
		title.setTypeface(SRL3ApplicationClass.getMyriadPro());

		this.handleTitleView(title);

		webView.setCallback(this);
		webView.setOnScrollChangedCallback(this);
		webView.setJavaScriptCallback(this);

		webView.setVisibility(View.INVISIBLE);
		this.registerForContextMenu(webView);

		if (!loaded) {
			if (shouldLoadFirstID) {
				//Log.d("PFr", "should start loading on top");
				this.displayArticleAtIndex(position);
			} else {
				//Log.d("PFr", "should start loading with given ID");
				this.displayArticleAtIndex(position, ID);
			}
		}

		webView.setContextMenuListener(this);

	}

	public void setPageFragmentListener (PageFragmentListener pll) {
		this.pageFragmentListener = pll;
	}

	public void displayArticleAtIndex (int pos) {
		article = pos+1;
		ArrayList<BookElement> elems = bookDB.elementsWithArticle(article);
		if (elems.size() == 0) return; 
		this.displayArticleAtIndex(pos, elems.get(0).ID);
	}

	public void displayArticleAtIndex (int pos, int id) {
		article = pos + 1;
		ID = id;
		if (webView != null) {
			updateTitleForWebView(id, webView);
            lastScrollPosition = 0.0f;
			LoadDataTask load = new LoadDataTask(webView, article, id);
			load.execute("");
		}
	}

	private void updateTitleForWebView (int topElementID, CsWebView web) {
		BookElement elem = bookDB.elementWithID(topElementID);
		String title = "";
		if (elem != null) {
			tocID = topElementID;
			title = bookDB.articleAndLawNameForElement(elem) + " ";
		} else {
			tocID = 0;
		}
		/*
    	if (elem != null) {
    		tocID = topElementID;

    		if (elem.weight == 1 || elem.weight == 2 || elem.weight == 5) { // top level
    			//self.tocLabel.font = [UIFont fontWithName:@"MyriadPro-Regular" size:24];
    			//self.tocLabel.numberOfLines = 1;
    			title = bookDB.articleNameForElement(elem) + " ";
    		} else {
    			//self.tocLabel.font = [UIFont fontWithName:@"MyriadPro-Regular" size:16];
    			//self.tocLabel.numberOfLines = 2;
    			title = bookDB.articleNameForElement(elem, false, false) + " \n" + bookDB.lawNameForElement(elem) + " ";                    
    		}
    	} else {
    		tocID = 0;
    	} */  

		if (web != null) {
			updateArticleTitle(web, title);
		}
	}

	private void updateArticleTitle (final CsWebView web,final String text) {
		activity.runOnUiThread(new Runnable() {
			public void run() {
				if (title != null) {
					title.setText(text);
				}
			}
		});
	}

	private void handleTitleView (AutoFitTextView tv) {
		//		for (final AutoFitTextView tv: tvs) {
		tv.setMinTextSize(16);			
		tv.setMaxTextSize(45);			
		tv.applyFont(context);
		tv.setOnTouchListener(this);
		//		}
	}

	/*@Override
	public boolean onLongClick(View arg0) {
		// TODO Auto-generated method stub
		return false;
	}*/

	@Override
	public boolean onTouch(View view, MotionEvent ev) {
		// TODO Auto-generated method stub

		if ((ev.getAction() == MotionEvent.ACTION_DOWN) && view.equals(this.title)) {
			webView.loadUrl("about:blank");
			pageFragmentListener.titleClicked(article, tocID);
		}

		return false;
	}

	@Override
	public void showOnTop(boolean isTop, float width) {
		// TODO Auto-generated method stub

	}

	@Override
	public void sendEvent(MotionEvent me) {
		// TODO Auto-generated method stub

	}

	@Override
	public void hideOnTop(boolean isHide) {
		// TODO Auto-generated method stub

	}

	@Override
	public void realPage(WebView wv) {
		// TODO Auto-generated method stub
	}

	public void runScript(WebView wv) {
		// TODO Auto-generated method stub

		CsWebView web = (CsWebView)wv;

		// When the webview is populated using the displayArticle() 
		// function, the webview position needs to be set at the position
		// of the selected element.
		this.gotoElementWithID(ID, web, 0);
		
		// Retrieve markings
		this.addMarkingsFromStartToEndID(web);
		
		// Enable webview scrolling and its scroll callbacks. 
		// This is in case of "prepending" content where there 
		// is risk of loading more than necessary and it ends up 
		// setting the webview position to an wrong one.
		enableWebViewScroll();
	}

	private void gotoElementWithID(int id, CsWebView web, int offset) {

		// String url2 = "javascript:var p=$('#e"+id+"').position().top - "+offset+"; $(window).scrollTop(p); Android.jsRequestCompleted()";
		String js = "javascript:var p=$('#e"+id+"').position().top - "+offset+"; $(window).scrollTop(p);";
		// Log.d("Pages", "Scroll to position - "+ id);
		web.loadUrl(js);

		final Handler handler = new Handler();
		handler.postDelayed(new Runnable() {
			@Override
			public void run() {
				// Do something after 0.5s = 500ms
				activity.runOnUiThread(new Runnable() {
					public void run() {
						webView.setVisibility(View.VISIBLE);
					}
				});
			}
		}, 500);
	}

	@Override
	public void updateTOCTitle(String id) {
		// TODO Auto-generated method stub

	}

	@Override
	public void touchedPoint(CsWebView web, float x, float y) {
		// TODO Auto-generated method stub

	}
	
	private String serialForMarkingWithID (int ID) {
		BookmarkHistoryItem item = BookmarkHistoryNotesManager.getInstance().getMarkingWithID(ID);
	    if ((item != null) && item.text.length() > 0)
	        return item.text;
	    return null;
	}

	private void addMarkingsFromStartToEndID (CsWebView web) {
	    String serial;
	    for (int ID = this.startID; ID <= this.endID; ID++) {
	    	serial = serialForMarkingWithID(ID);
	    	if (serial != null) {
	    		String js = "javascript:deserializeMarksInElementWithID('e"+ ID +"','" + serial + "');";
	            web.loadUrl(js);
	    	}
	    }
	}
	
	private class LoadDataTask extends AsyncTask <String, Void, String> {

		CsWebView wv;
		int tempArticle;
		int id;

		LoadDataTask (CsWebView web, int article, int ID) {
			this.wv = web;
			this.id = ID;
			this.tempArticle = article;
		}

		@Override
		protected String doInBackground(String[] arg0) {
			StringBuilder sb = new StringBuilder();
			sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>");
			sb.append("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.1//EN\" \"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">");
			sb.append("<html xmlns=\"http://www.w3.org/1999/xhtml\">");
			sb.append("<head>");
			sb.append("<title>SRL3</title>");
			sb.append("<meta http-equiv=\"Content-Type\" content=\"text/html;charset=utf-8\" />");
			sb.append("<link href=\"css/idGeneratedStyles.css\" rel=\"stylesheet\" type=\"text/css\" />");
			sb.append("<link href=\"css/android_srl_styles.css\" rel=\"stylesheet\" type=\"text/css\" />");
			sb.append("<script src=\"js/jquery.min.js\"></script>");
			sb.append("<script src=\"js/rangy-core.js\"></script>");
			sb.append("<script src=\"js/rangy-cssclassapplier.js\"></script>");
			sb.append("<script src=\"js/srl.js\"></script>");
			sb.append("</head>");
			sb.append("<body xml:lang=\"sv-SE\">");
			sb.append("<div id=\"content\" class=\"Basic-Text-Frame\">");

			if (bookDB == null) return null; // Database object needs to be set before calling this

			// Add initial page content
			ArrayList<BookElement> elemsArray;
			BookElement elem;
			String noteHtml = null;
			String markHtml = null;
			if (id > 0) {
				////Log.d("Pages", "Getting elements for id - "+id+" and article - "+tempArticle);
				elemsArray = bookDB.elementsWithArticleContainingID(tempArticle, id);
				//TODO: FMS 13-1-2016, IndexOutOfBoundsExcpetion: Invalid index 0, size is 0
				elem = elemsArray.get(0);
				startID = elem.ID;
				// Beginning of tempArticle - no more above
				if (startID == id) {
					startID = 0;
				}
			} else {
				elemsArray = bookDB.firstElementsOfArticle(tempArticle);
				//		        if (isFirstOpenedPage) {
				startID = 0;
				//		        }
			}
			//String html = article_head;
			String html = "";
			for (BookElement elm : elemsArray) {
				//////Log.d("Pages", "Should print - \n"+elm.html);
				html += elm.html;

				//updateArticleHash(wv, elm);

				noteHtml = htmlForNoteWithID(elm.ID);
				if (noteHtml != null)
					html += noteHtml;
			}
			elem = elemsArray.get(elemsArray.size()-1);
			//		    if (isFirstOpenedPage) {
			////Log.e("Pages", "Setting end ID to - "+elem.ID);
			endID = elem.ID;
			//	        }

			sb.append(html);
			sb.append("<script>rangy.init();cssApplier=rangy.createCssClassApplier('mark', {normalize:true});</script>");		    
			sb.append("</div>");
			sb.append("</body>");
			sb.append("</html>");

			String finalHtml = sb.toString();

			return finalHtml;
		}

		public String htmlForNoteWithID (int ID) {
			String html = null;
			BookmarkHistoryItem item = BookmarkHistoryNotesManager.getInstance().getNoteWithID(ID);
			if (item != null && item.text.length() > 0) {
				html = "<p id=\"e" + ID + "\" class=\"NJNote\">" + item.text + "</p>";
			}

			return html;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

			pageFragmentListener.pageLoaded(position);

			wv.setTag("initialPage");

			wv.loadDataWithBaseURL("file:///android_asset/html/", result,
					"text/html", "UTF-8", null);
		}
	}

	/* PWA: 2014-09-02: Not used anymore by onScroll
	private float calculateProgress(WebView content) {
		float contentHeight = (float) Math.floor(content.getContentHeight() * +content.getScaleY());
		float currentScrollPosition = content.getScrollY();
		float displayedContentHeight = content.getMeasuredHeight();
		float effectiveHeight = (contentHeight - displayedContentHeight);
		float percentWebview = currentScrollPosition / effectiveHeight;
		return percentWebview;
	}
	*/

	// PWA: 2014-09-02: deprecated getScale still seems to be the best way to get WebView scale
	@Override
	public void onScroll(CsWebView wv, int l, int t) {
		final float wvMarginBottom = 50.0f;
		float actualContentHeight = wv.getContentHeight() * wv.getScale();
		float currentScrollPosition = wv.getScrollY();
		float viewHeight = wv.getHeight();
		
		Log.d("Pages", "getHeight:"+wv.getHeight()+" actualContentHeight:"+actualContentHeight+" getScale:"+wv.getScale()+" getScrollY:"+wv.getScrollY());		

		if (currentScrollPosition <= 0.5 && lastScrollPosition > 0.5) {
			Log.d("Pages", " scrolling - Reached top");
			if (startID > 0) {
				if (!wv.loadingContent) {
					disableWebViewScroll(); 
					ID = startID;
					this.displayArticleAtIndex(position, ID);
				}
			}
		} else if (currentScrollPosition >= actualContentHeight - viewHeight - wvMarginBottom) {
			Log.d("Pages", " scrolling - Reached bottom");
			appendElementsToPage(wv);
		}
		lastScrollPosition = currentScrollPosition;
	}

	/* PWA: 2014-09-02: Old version 
	@Override
	public void onScroll(CsWebView wv, int l, int t) {
		float progress = calculateProgress(wv);
		
		Log.d("Pages", "getHeight:"+wv.getHeight()+"getContentHeight:"+wv.getContentHeight()+" getScale:"+wv.getScale()+" getScrollY:"+wv.getScrollY()+" getMeasuredHeight:"+wv.getMeasuredHeight());		
		Log.d("Pages", "scrolling with progress - "+progress);

		boolean reachedTop = (progress < 0);

		if (SRLUtils.isTablet(context)){
			reachedTop = (progress >= 0.01 && progress <= 0.03); 
		}

		if (reachedTop) {
			Log.d("Pages", " scrolling - Reached top");
			// appendContent(wv, true);
			if (startID > 0) {
				if (!wv.loadingContent) {
					disableWebViewScroll(); 
					ID = startID;
					this.displayArticleAtIndex(position, ID);
				}
			}
		} else if (progress >= 2) {
			Log.d("Pages", " scrolling - Reached bottom");
			// appendContent(wv, false);
			appendElementsToPage(wv);
		}
	}
	*/

	private void appendElementsToPage (final CsWebView wv) {
		if (endID > 0) {
			ArrayList<BookElement> elementArray = bookDB.elementsWithArticleStartingWithID(article, endID + 1);
			if (elementArray == null || elementArray.size() == 0) {
				wv.endID = 0;
				// this.endID = 0;
				return;
			}
			BookElement element;
			String js = "";
			String noteHtml;
			String html = "";
			String serial;

			for (BookElement elem: elementArray) {
		        js = "javascript:$(\"#content\").append(\"" + elem.html.replace("\"", "\\\"") + "\");";
		        js = js.replace("\n", ""); // This is to handle newlines in tables
		        wv.loadUrl(js);
		        
		        noteHtml = this.htmlForNoteWithID(elem.ID);
				if (noteHtml != null) {
					js = "javascript:$(\"#content\").append(\"" + noteHtml.replace("\"", "\\\"") + "\");";
					wv.loadUrl(js);
				}
				
				serial = this.serialForMarkingWithID(elem.ID);
		        if (serial != null) {
		        	js = "javascript:deserializeMarksInElementWithID('e"+ elem.ID +"','" + serial + "');";
		            wv.loadUrl(js);
		        }
			}
			element = elementArray.get(elementArray.size()-1);
			endID = element.ID;
		}
	}
	
	private void appendContent (final CsWebView wv, boolean toTop) {
		if (toTop) {
			if (startID > 0) {
				// Append elements to the top.
				if (!wv.loadingContent) {
					//Log.d("Pages", "Loading top content for article - "+ article);

					// Disable scrolling on the web view by overriding touch events (and also the scroll events callback to be certain)
					// and then load the top content. Enable the scroll callbacks and touch events again in the runScript() callback function.
					disableWebViewScroll(); 

					ID = startID;
					// Clear all previously loaded ID's.
					//				this.getArticleHash(wv).clear();
					//				this.displayArticle(wv.article, wv);
					this.displayArticleAtIndex(position, ID);
				}
			} else {
				////Log.d("Pages", "No elements to load.");
			}							
		} else {
			if (endID > 0) {
				//			int a = mCurlView.getCurrentIndex()+1;
				ArrayList<BookElement> elementArray = bookDB.elementsWithArticleStartingWithID(article, endID + 1);
				if (elementArray == null || elementArray.size() == 0) {
					//Log.d("Pages", "Not loading bottom content for article - "+ wv.article + " and endID - "+ wv.endID);
					wv.endID = 0;
					return;
				} else {
					////Log.d("Pages", "endID < 0");
				}
				//Log.d("Pages", "Loading bottom content for article - "+ article);
				//			loadingContent = true;
				BookElement element;
				String js = "";
				String noteHtml;

				String html = "";

				for (BookElement elem: elementArray) {						    	
					// Check if the article has already been loaded in the webview. If not, add it and avoid repetitions.
					//		    	if (!this.getArticleHash(wv).contains(elem.ID)) {
					noteHtml = this.htmlForNoteWithID(elem.ID);
					if (noteHtml != null) {
						js = "$(\"#content\").append(\"" + noteHtml.replace("\"", "\\\"") + "\");";
						//"$(\"#content\").append(\"%@\");"
						//[self.webview stringByEvaluatingJavaScriptFromString:js];
					}
					html += (elem.html.replace("\"", "\\\"")).replace("\n", "");
					//		    	} else {
					////Log.d("Article Hash", "Already loaded element with ID - "+elem.ID);
					//		    	}

					//		        js = "javascript:$(\"#content\").append(\""+html+"\");";

					//		        noteHtml = [self htmlForNoteWithID:elem.ID];
					//		        if (noteHtml) {
					//		            js = [NSString stringWithFormat:@"$(\"#content\").append(\"%@\");", [noteHtml stringByReplacingOccurrencesOfString:@"\"" withString:@"\\\""]];
					//		            [self.webview stringByEvaluatingJavaScriptFromString:js];
					//		        }
				}

				js = "javascript:var FunctionOne = function () {" +			        					    
						"document.getElementsByTagName('div')[0].innerHTML = document.getElementsByTagName('div')[0].innerHTML + '"+html+"';" +
						"return 'blah';" +	
						"};" +
						"var FunctionTwo = function () {" +
						//"Android.setJSDone();" +
						"};" +
						"FunctionOne().done(FunctionTwo());";

				wv.loadUrl(js);
				element = elementArray.get(elementArray.size()-1);
				endID = element.ID;
			}
		}
	}

	private void removeScrollCallbacks () {
		webView.setOnScrollChangedCallback(null);
	}

	private void setScrollCallbacks () {
		webView.setOnScrollChangedCallback(this);
	}

	private void disableWebViewScroll () {
		this.removeScrollCallbacks();
		// Disable the webView scroll by overlaying a view which would override the webView's touch events.
		LinearLayout linearLayout = new LinearLayout(context);
		linearLayout.setTag("overlay_view");
		//linearLayout.setBackgroundResource(R.color.white_transparent);
		RelativeLayout.LayoutParams relativeParams = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		relativeParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);

		linearLayout.setOnTouchListener(new View.OnTouchListener() {

			@Override
			public boolean onTouch(View arg0, MotionEvent arg1) {
				// TODO Auto-generated method stub
				return true;
			}
		});

		parentView.addView(linearLayout, relativeParams);
	}

	private void enableWebViewScroll () {
		this.setScrollCallbacks();
		for (int i = 0; i < parentView.getChildCount(); i++) {
			View child = parentView.getChildAt(i);
			if (child.getTag() != null) {
				if (child.getTag().equals("overlay_view")) {
					// Remove the overlay view.
					////Log.d("Pages", "Removing overlay view and enabling webview scroll.");
					parentView.removeView(child);
				}
			}
		}
	}
	@Override
	public void onContextItemSelected(ActionMode mode, MenuItem item) {
		// TODO Auto-generated method stub

		switch(item.getItemId()){

		case R.id.pam_mark: 
			//	        Toast.makeText(context, "Selected Markera", Toast.LENGTH_LONG).show();
			webView.addMarkText();
			mode.finish();
			break; 

		case R.id.pam_bookmark:           
			//	    	Toast.makeText(context, "Selected Bokmärk", Toast.LENGTH_LONG).show(); 
			// CJA 2015.4.28
			//webView.addBookmark()
			isNotes = false;
			webView.setAnchorIDCallbackListener(this);
			webView.getSelectionID();
			mode.finish();  
			break;        

		case R.id.pam_note:               
			//	    	Toast.makeText(context, "Selected Anteckna ", Toast.LENGTH_LONG).show();
			// PWA: TODO: need to restore any existing notes for ID
			isNotes = true;
			webView.setAnchorIDCallbackListener(this);
			webView.getSelectionID();
			mode.finish();  
			break; 
		}
	}

	@Override
	public void updateTitle(String idString, int webViewID) {
		// TODO Auto-generated method stub
		int topElementID = Integer.parseInt(idString.replace("e", ""));
		updateTitleForWebView(topElementID, webView);
	}

	//@Override
	//public void onJSRequestCompleted(final CsWebView webView) {
	//	// TODO Auto-generated method stub
	//	activity.runOnUiThread(new Runnable() {
	//        public void run() {
	//        	webView.setVisibility(View.VISIBLE);
	//        }
	//    });
	//}
	@Override
	public void onNotesReadyClick(String note) {

		Log.d(note,"added note");
		
		//webView.addNote(note);
		if (isNotes) {
			BookmarkHistoryItem item = BookmarkHistoryItem.getItem(Integer.parseInt(gotIDfromMarkedText), 0, SRLUtils.getCurrentTimeStamp(), null, note, true);
			BookmarkHistoryNotesManager.getInstance().insertNote(item);
			this.displayArticleAtIndex(position, Integer.parseInt(gotIDfromMarkedText));
		} else {
			// PWA: 2017-01-16: updated with time stamp
			// BookmarkHistoryItem item = BookmarkHistoryItem.getItem(Integer.parseInt(gotIDfromMarkedText), 0, null, null, note, true);
			BookmarkHistoryItem item = BookmarkHistoryItem.getItem(Integer.parseInt(gotIDfromMarkedText), 0, SRLUtils.getCurrentTimeStamp(), null, note, true);
	        BookmarkHistoryNotesManager.getInstance().insertBookmark(item);
			//Toast.makeText(mContext, "Bokmärke skapat!", Toast.LENGTH_LONG).show();
		}
		
	}

	private String htmlForNoteWithID(int ID) {
		String html = null;
		BookmarkHistoryItem item = BookmarkHistoryNotesManager.getInstance().getNoteWithID(ID);
		if (item != null && item.text.length() > 0) {
			html = "<p id=\"e" + ID + "\" class=\"NJNote\">" + item.text + "</p>";
		}
		return html;
	}

	@Override
	public void onAnchorIDFetchCompleted(String arg) {
		if (arg != null && isNotes) {
			
			String tempStr = arg.substring(1);
			gotIDfromMarkedText = tempStr;
			BookmarkHistoryItem foundNote = BookmarkHistoryNotesManager.getInstance().getNoteWithID(Integer.parseInt(tempStr));
			String tempNote = null;
			if(foundNote != null) {
				tempNote = foundNote.text;
			}

			// PWA: 2016-08-19: Changed to use default empty contructor
			NotesDialogFragment notesDialog = new NotesDialogFragment();
			notesDialog.SetNotesDialogFragmentArgs(tempNote, "Lägg till anteckning", false);
			notesDialog.mListener = PageFragment.this;
			FragmentManager ft = getActivity().getFragmentManager();
			notesDialog.show(ft, "notes");
			
			} else {
				String tempStr = arg.substring(1);
				gotIDfromMarkedText = tempStr;
				BookmarkHistoryItem foundNote = BookmarkHistoryNotesManager.getInstance().getNoteWithID(Integer.parseInt(tempStr));
				String tempNote = null;
				if(foundNote != null) {
					tempNote = foundNote.text;
				}
				
				BookElement element = bookDB.elementWithID(Integer.parseInt(tempStr));

				// PWA: 2016-08-19: Changed to use default empty contructor
				NotesDialogFragment notesDialog = new NotesDialogFragment();
				// notesDialog.SetNotesDialogFragmentArgs(bookDB.articleNameForElement(element), "Lägg till bokmärke", true);
				notesDialog.SetNotesDialogFragmentArgs(labelForElement(element), "Lägg till bokmärke", true);

				notesDialog.mListener = PageFragment.this;
				FragmentManager ft = getActivity().getFragmentManager();
				notesDialog.show(ft, "notes"); 
			}
		
	}

	public String labelForElement(BookElement element) {
		String retStr = "";
		if (element.weight == 1 || element.weight == 2 || element.weight == 5) {
			retStr = bookDB.articleNameForElement(element);
		} else {
			retStr = bookDB.articleNameForElement(element,false,false) + " " + bookDB.lawNameForElement(element);
		}
		return retStr;
	}
}
