package se.isolve.srl3.reader;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.concurrent.CopyOnWriteArrayList;

import se.isolve.srl3.SRL3ApplicationClass;
import se.isolve.srl3.database.BookDB;
import utils.SRLSynchronize;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

public class BookmarkHistoryNotesManager {

	private static final String TAG = "BHNManager";
	
    private static BookmarkHistoryNotesManager instance;
    private static final int BOOKMARKHISTORY_MAX_NO_OF_ITEMS = 200;
	private static final int BOOKMARKHISTORY_NO_LIMIT_ITEMS = 9999;
	private static final String FILENAME = "bookmarkHistory";
	private static final String BOOK_YEAR = "18";
	private static final String OLD_BOOK_YEAR = "17-2";
	
	private ArrayList<BookmarkHistoryItem> markingArray;
	private ArrayList<BookmarkHistoryItem> bookmarkArray;
	private ArrayList<BookmarkHistoryItem> noteArray;
	private ArrayList<BookmarkHistoryItem> historyArray;
	
	//private BookmarkHistory bookmarkHistory;

    //Singleton = private constructor
    private BookmarkHistoryNotesManager() {
        load();
    }

    public static void initInstance() {
        if (instance == null) {
            Log.d("BHNManager", "Singleton initialized");
            instance = new BookmarkHistoryNotesManager();
        }
    }

    public static BookmarkHistoryNotesManager getInstance() {
        return instance;
    }
    
    public static void destroyInstance () {
    	instance = null;
    }

    private void load() {
		markingArray = new ArrayList<BookmarkHistoryItem>();
		bookmarkArray = new ArrayList<BookmarkHistoryItem>();
		noteArray = new ArrayList<BookmarkHistoryItem>();
		historyArray = new ArrayList<BookmarkHistoryItem>();

		ArrayList<ArrayList<BookmarkHistoryItem>> bookmarkHistory = new ArrayList<ArrayList<BookmarkHistoryItem>>();
		bookmarkHistory = getFromFile(SRL3ApplicationClass.getContext());
		
		if (bookmarkHistory != null && bookmarkHistory.size() == 4) {
			markingArray = bookmarkHistory.get(0);
			bookmarkArray = bookmarkHistory.get(1);
			noteArray = bookmarkHistory.get(2);
			historyArray = bookmarkHistory.get(3);
		}

		// Migrate old bookmarks and notes
		ArrayList<ArrayList<BookmarkHistoryItem>> oldBookmarkHistory = new ArrayList<ArrayList<BookmarkHistoryItem>>();
		oldBookmarkHistory = getOldFromFile(SRL3ApplicationClass.getContext());
		if (oldBookmarkHistory != null && oldBookmarkHistory.size() == 4) {
			BookDB bookDB = new BookDB(SRL3ApplicationClass.getContext());
			bookDB.createDatabase();
			ArrayList<BookmarkHistoryItem> oldNoteArray = new ArrayList<BookmarkHistoryItem>();
			oldNoteArray = oldBookmarkHistory.get(2);
			ArrayList<BookmarkHistoryItem> oldBookmarkArray = new ArrayList<BookmarkHistoryItem>();
			oldBookmarkArray = oldBookmarkHistory.get(1);
			for (BookmarkHistoryItem item: oldNoteArray) {
				int newId = bookDB.idForOldLawbookId(item.getID());
				// PWA: 2017-01-16: Added check
				if (newId > 0) {
					item.setID(newId);
					this.addNoteToArray(item, false);
				}
			}
			for (BookmarkHistoryItem item: oldBookmarkArray) {
				int newId = bookDB.idForOldLawbookId(item.getID());
				// PWA: 2017-01-16: Added check
				if (newId > 0) {
					item.setID(newId);
					this.addBookmarkToArray(item, false);
				}
			}
			deleteOldFile(SRL3ApplicationClass.getContext());
		}
    }
    
    //public void getFromFile (String path, Context c) throws ClassNotFoundException, IOException {
	//	ArrayList<ArrayList<BookmarkHistoryItem>> array = (ArrayList<ArrayList<BookmarkHistoryItem>>) SerializationUtil.deserialize(path);
	//	
	//	if (array != null && array.size() == 4) {
	//		markingArray = array.get(0);
	//		bookmarkArray = array.get(1);
	//		noteArray = array.get(2);
	//		historyArray = array.get(3);
	//	}
		
		//return history;
	//}
	
	//public void writeToFile (String path) throws IOException {
	//	ArrayList<ArrayList<BookmarkHistoryItem>> array = new ArrayList<ArrayList<BookmarkHistoryItem>>();
	//	array.add(markingArray);
	//	array.add(bookmarkArray);
	//	array.add(noteArray);
	//	array.add(historyArray);
	//    SerializationUtil.serialize(array, path);
	//}

	public ArrayList<BookmarkHistoryItem> getMarkings () {
	    return this.markingArray;
	}

	public ArrayList<BookmarkHistoryItem> getBookmarks () {
	    return this.bookmarkArray;
	}

	public ArrayList<BookmarkHistoryItem> getNotes () {
	    return this.noteArray;
	}

	public ArrayList<BookmarkHistoryItem> getHistoryItems () {
	    return this.historyArray;
	}
	
	private void deleteNoteWithID(int ID) {
		CopyOnWriteArrayList<BookmarkHistoryItem> deleteNotesArray = new CopyOnWriteArrayList<BookmarkHistoryItem>(getNotes());
		BookmarkHistoryItem deleteItem = null;
		for (BookmarkHistoryItem it : deleteNotesArray) {
			if	(it.ID == ID) {
				deleteItem = it;
			}
		}
		deleteNotesArray.remove(deleteItem);
		noteArray = new ArrayList<BookmarkHistoryItem>(deleteNotesArray);
	}
	
	public void insertItemIntoNotesArray(BookmarkHistoryItem item) {
		if (item != null) {
	    	if	(getNoteWithID(item.ID) != null) {
	    		deleteNoteWithID(item.ID);
	    	}
	    	else {
	    	this.delete(item, noteArray);
	    	}
	    	noteArray.add(0, item);
	        while (noteArray.size() > BOOKMARKHISTORY_MAX_NO_OF_ITEMS) {
	        	noteArray.remove(noteArray.size() - 1);
	        }
	        Log.d("BMHNM", "Added bookmark history item - "+item.toString());
	        
	        writeToFile(SRL3ApplicationClass.getContext());
	        
	}
	}

	public void insertItemIntoArray (BookmarkHistoryItem item, ArrayList<BookmarkHistoryItem> array, int limit) {
	    if (item != null) {
	    	
	    	this.delete(item, array);
	    	array.add(0, item);
	        while (array.size() > limit) {
	        	array.remove(array.size() - 1);
	        }
	        Log.d("BMHNM", "Added bookmark history item - "+item.toString());
	        
	        writeToFile(SRL3ApplicationClass.getContext());
	        
	        /*File file = new File(SRL3ApplicationClass.getContext().getFilesDir(), "bookmarkItems");
	        file.getPath();
	        
	        try {
				this.writeToFile(file.getPath());
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				BookmarkHistory hist = null;
				try {
					 hist = getFromFile(file.getPath(), context);
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				Log.d("BMH", "Got stored bmh instances - "+hist.bookmarkArray.size());
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/
	    }
	}

	public void insertItemIntoArray (BookmarkHistoryItem item, ArrayList<BookmarkHistoryItem> array) {
	    this.insertItemIntoArray(item, array, BOOKMARKHISTORY_NO_LIMIT_ITEMS);
	}

	public boolean itemExistsInArray (BookmarkHistoryItem item, ArrayList<BookmarkHistoryItem> array) {
	    return array.contains(item);
	}

	public void delete (BookmarkHistoryItem item , ArrayList<BookmarkHistoryItem> array) {
	    array.remove(item);
	    writeToFile(SRL3ApplicationClass.getContext());
	}

	public void deleteAll (ArrayList<BookmarkHistoryItem> array) {
	    array.clear();
	}

	public void insertMarking (BookmarkHistoryItem item) {
	    this.insertItemIntoArray(item, this.markingArray);
	}

	public boolean markingExists (BookmarkHistoryItem item) {
	    return this.itemExistsInArray(item, this.markingArray);
	}

	public void deleteMarking (BookmarkHistoryItem item) {
		for (int i = 0; i < bookmarkArray.size(); i++) {
			BookmarkHistoryItem itItem = markingArray.get(i);
			if(itItem.ID == item.ID) {
				markingArray.remove(i);
				break;
			}
		}
		//this.delete(item, bookmarkArray);
		writeToFile(SRL3ApplicationClass.getContext());
	}

	public void deleteAllMarkings () {
		this.markingArray.clear();
	}

	public void insertBookmark (BookmarkHistoryItem item) {
		this.insertItemIntoArray(item, bookmarkArray, BOOKMARKHISTORY_MAX_NO_OF_ITEMS);
		Log.d(TAG, "Inserting new bookmark - "+item.toString());
		SRLSynchronize.getInstance().sendNewNotesAndBookmarks(SRL3ApplicationClass.getContext());
	}

	public void addBookmarkToArray (BookmarkHistoryItem bookmarkItem, boolean isWritten) {
		ArrayList<BookmarkHistoryItem> tempArray = new ArrayList<BookmarkHistoryItem> (bookmarkArray);
		for (BookmarkHistoryItem item : tempArray) {
			if (item.ID == bookmarkItem.ID) {
				bookmarkArray.remove(item);
			}
		}
		bookmarkArray.add(bookmarkItem);
		if (isWritten) {
			Log.d(TAG, "Added bookmark to array and sending new bookmarks");
			SRLSynchronize.getInstance().sendNewNotesAndBookmarks(SRL3ApplicationClass.getContext());
		}
	}

	public void setBookmarksToSynced (ArrayList<BookmarkHistoryItem> syncedArray) {
		for (BookmarkHistoryItem item : syncedArray) {
			if (item.text.equals("") || (item.text == null)) {
				deleteItemFromArray(item, bookmarkArray);
			}
			else {
				this.addBookmarkToArray(item, false);
			}
		}
	}

	public void setAllBookmarksToNotSynced () {
		for (BookmarkHistoryItem item : bookmarkArray) {
			item.notSynced = true;
		}
	}

	public boolean bookmarkExists (BookmarkHistoryItem item) {
	    return this.itemExistsInArray(item, bookmarkArray);
	}

	public void deleteBookmark (BookmarkHistoryItem item) {
		for (int i = 0; i < bookmarkArray.size(); i++) {
			BookmarkHistoryItem itItem = bookmarkArray.get(i);
			if(itItem.ID == item.ID) {
				bookmarkArray.remove(i);
				break;
			}
		}
		//this.delete(item, bookmarkArray);
		writeToFile(SRL3ApplicationClass.getContext());
	}

	public void deleteAllBookmarks () {
		this.bookmarkArray.clear();
		writeToFile(SRL3ApplicationClass.getContext());
	}

	public void insertNote (BookmarkHistoryItem item) {
	    this.insertItemIntoArray(item, noteArray, BOOKMARKHISTORY_MAX_NO_OF_ITEMS);
	    Log.d(TAG, "Inserting new note - "+item.toString());
	    SRLSynchronize.getInstance().sendNewNotesAndBookmarks(SRL3ApplicationClass.getContext());
	}

	public void addNoteToArray (BookmarkHistoryItem noteItem, boolean isWritten) {
		ArrayList<BookmarkHistoryItem> tempArray = new ArrayList<BookmarkHistoryItem> (noteArray);
	    for (BookmarkHistoryItem item : tempArray) {
	        if (item.ID == noteItem.ID) {
	            noteArray.remove(item);
	        }
	    }
	    noteArray.add(noteItem);
	    if (isWritten) {
	    	Log.d(TAG, "Added note to array and sending new notes");
	        SRLSynchronize.getInstance().sendNewNotesAndBookmarks(SRL3ApplicationClass.getContext());
	    }
	}
	
	public void setNotesToSynced (ArrayList<BookmarkHistoryItem> syncedArray) {
		for (BookmarkHistoryItem item : syncedArray) {
			if (item.text.equals("") || (item.text == null)) {
				deleteItemFromArray(item, noteArray);
			}
			else {
				this.addNoteToArray(item, false);
			}
		}
	}

	public void deleteItemFromArray (BookmarkHistoryItem item, ArrayList<BookmarkHistoryItem> array) {
		array.remove(item);
	}
	
	public boolean noteExists (BookmarkHistoryItem item) {
	    return this.itemExistsInArray(item, this.noteArray);
	}

	public BookmarkHistoryItem getNoteWithID (int ID) {
		ArrayList<BookmarkHistoryItem> temp = new ArrayList<BookmarkHistoryItem>(this.noteArray);
	    for (BookmarkHistoryItem item : temp) {
	        if (item.ID == ID)
	            return item;
	    }
	    return null;
	}
	
	public BookmarkHistoryItem getMarkingWithID (int ID) {
		for (BookmarkHistoryItem item : this.markingArray) {
			if (item.ID == ID) {
				return item;
			}
		}
		return null;
	}

	public void deleteNote (BookmarkHistoryItem item) {
		for (int i = 0; i < noteArray.size(); i++) {
			BookmarkHistoryItem itItem = noteArray.get(i);
			if(itItem.ID == item.ID) {
				//noteArray.remove(i);
				// For syncing purposes, we need to keep track of all notes created. Therefore, only the text must be cleared from each note.
				// Its notSyced flag must be set to true.
				itItem.setText("");
				itItem.setNotSynced(true);
				Log.d(TAG, "Deleting note - "+itItem.toString());
				break;
			}
		}
		writeToFile(SRL3ApplicationClass.getContext());
	}

	public void deleteAllNotes () {
		for (int i = 0; i < noteArray.size(); i++) {
			BookmarkHistoryItem itItem = noteArray.get(i);
			//noteArray.remove(i);
			// For syncing purposes, we need to keep track of all notes created. Therefore, only the text must be cleared from each note.
			// Its notSyced flag must be set to true.
			itItem.setText("");
			itItem.setNotSynced(true);
			Log.d(TAG, "Deleting all notes");
		}
		writeToFile(SRL3ApplicationClass.getContext());
//		this.noteArray.clear();
	}

	public void insertHistoryItem (BookmarkHistoryItem item) { 
		this.insertItemIntoArray(item, this.historyArray, BOOKMARKHISTORY_MAX_NO_OF_ITEMS);
	}

	public boolean historyItemExists (BookmarkHistoryItem item) {
		return this.itemExistsInArray(item, this.historyArray);
	}

	public void deleteHistoryItem (BookmarkHistoryItem item) {
		this.delete(item, this.historyArray);
	}

	public void deleteAllHistoryItems () {
	    this.historyArray.clear();
	}

	public boolean writeToFile(Context context) {
		
		SharedPreferences settings = context.getSharedPreferences("LOGIN",Context.MODE_PRIVATE);
		String loggedInUser = settings.getString("username", null);
		
		if (loggedInUser != null) {
			try {
		    	ArrayList<ArrayList<BookmarkHistoryItem>> bookmarkHistory = new ArrayList<ArrayList<BookmarkHistoryItem>>();			
		    	bookmarkHistory.add(markingArray);
		    	bookmarkHistory.add(bookmarkArray);
		    	bookmarkHistory.add(noteArray);
		    	bookmarkHistory.add(historyArray);
		    	
		    	Log.d(TAG, "Writing arrays  - "+bookmarkHistory.toString()+" \nTo file - "+loggedInUser+"_"+FILENAME);
		    	
		    	// CJA 2015.7.3
		        //FileOutputStream fos = context.openFileOutput(loggedInUser+"_"+FILENAME, Context.MODE_PRIVATE);
		    	FileOutputStream fos = context.openFileOutput(loggedInUser + "_" + BOOK_YEAR + "_" + FILENAME, Context.MODE_PRIVATE); 
		        ObjectOutputStream oos = new ObjectOutputStream(fos);
		        oos.writeObject(bookmarkHistory);
		        oos.close();
		        
		        // Update notes.
		        // Log.d(TAG, "Deleted note and sending new notes");
		        //SRLSynchronize.getInstance().sendNewNotes(context);
//            	if (BookmarkHistoryNotesManager.getInstance() != null) {
//            		BookmarkHistoryNotesManager.getInstance().writeToFile(context);
//            	}
		        
		    } catch (IOException e) {
		        e.printStackTrace();
		        return false;
		    }
		} else {
			return false;
		}

	    return true;
	}

	public ArrayList<ArrayList<BookmarkHistoryItem>> getFromFile(Context context) {
		
		SharedPreferences settings = context.getSharedPreferences("LOGIN",Context.MODE_PRIVATE);
		String loggedInUser = settings.getString("username", null);
		
		if (loggedInUser != null) {
			
			Object readObject = null;
			
		    try {
		    	// CJA 2015.7.3
		        //FileInputStream fis = context.openFileInput(loggedInUser+"_"+FILENAME);
		    	FileInputStream fis = context.openFileInput(loggedInUser + "_" + BOOK_YEAR + "_" + FILENAME);
		        ObjectInputStream is = new ObjectInputStream(fis);
		        readObject = is.readObject();
		        is.close();

		        
		    } catch (IOException e) {
		        e.printStackTrace();
		    } catch (ClassNotFoundException e) {
		        e.printStackTrace();
		    }
			//}
		    //return null;
		    if(readObject != null) {
		    	Log.d(TAG, "Got arrays - "+((ArrayList<ArrayList<BookmarkHistoryItem>>) readObject).toString()+" \nFrom file - "+loggedInUser+"_"+FILENAME);
	            return (ArrayList<ArrayList<BookmarkHistoryItem>>) readObject;
	        }
	        else 
	        	return null;
		} else {
			return null;
		}
		
	}

	public ArrayList<ArrayList<BookmarkHistoryItem>> getOldFromFile(Context context) {
		SharedPreferences settings = context.getSharedPreferences("LOGIN",Context.MODE_PRIVATE);
		String loggedInUser = settings.getString("username", null);
		if (loggedInUser != null) {
			Object readObject = null;
			try {
				FileInputStream fis = context.openFileInput(loggedInUser + "_" + OLD_BOOK_YEAR + "_" + FILENAME);
				ObjectInputStream is = new ObjectInputStream(fis);
				readObject = is.readObject();
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
			if (readObject != null) {
				return (ArrayList<ArrayList<BookmarkHistoryItem>>) readObject;
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	private void deleteOldFile(Context context) {
		SharedPreferences settings = context.getSharedPreferences("LOGIN",Context.MODE_PRIVATE);
		String loggedInUser = settings.getString("username", null);
		if (loggedInUser != null) {
			context.deleteFile(loggedInUser + "_" + OLD_BOOK_YEAR + "_" + FILENAME);
		}
	}
	
//	public String bookmarkHistoryFileName (Context context) {
//		SharedPreferences settings = context.getSharedPreferences("LOGIN",Context.MODE_PRIVATE);
//		String loggedInUser = settings.getString("username", null);
//		return loggedInUser+"_"+FILENAME;
//	}
}