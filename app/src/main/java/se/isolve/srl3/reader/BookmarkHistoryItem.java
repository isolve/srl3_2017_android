package se.isolve.srl3.reader;

import java.io.Serializable;

import se.isolve.srl3.database.BookElement;
import android.util.Log;

public class BookmarkHistoryItem implements Serializable {

	/**
	 * 
	 */
//	private static final long serialVersionUID = 1L;
	public int ID;
	int type;
	String date;
	String location;
	public String text;
	
	// Variables used for syncing functionality.
	boolean notSynced;
	
	public BookmarkHistoryItem () {
		ID = 0;
		type = 0;
		date = null;
		location = null;
		text = null;
	}
	
	@Override
	  public String toString() {
	    return "BookmarkHistoryItem [ID=" + ID + ", type=" + type
	        + ", date: " + ((date != null) ? date.toString() : "null") + ", " +
	        "location - " + ((location != null) ? location : "null") + ", " +
	        "text - " + ((text != null) ? text : "null") + ", " +
	        "notSynced - " + ((this.notSynced) ? "Yes" : "No") + ", " +
	        "]";
	  }
	
	public void setID (int id) {
		this.ID = id;
	}
	
	public int getID () {
		return this.ID;
	}
	
	public void setNotSynced (boolean ns) {
		this.notSynced = ns;
	}
	
	public boolean getNotSynced () {
		return this.notSynced;
	}
	
	public void setType (int t) {
		this.type = t;
	}
	
	public int getType () {
		return this.type;
	}
	
	public void setDate (String d) {
		this.date = d;
	}
	
	public String getDate () {
		return this.date;
	}
	
	public void setLocation (String loc) {
		this.location = loc;
	}
	
	public String getLocation () {
		return this.location;
	}
	
	public void setText (String txt) {
		this.text = txt;
	}
	
	public String getText () {
		return this.text;
	}
	
	public static BookmarkHistoryItem getItem (int ID, int type, String date, String location, String text, boolean notSynced) {
		Log.d("BMHI", "getting a BookmarkHistoryItem from ID - "+ID);
		BookmarkHistoryItem elem = new BookmarkHistoryItem();
	    elem.setID(ID);
	    elem.setType(type);
	    elem.setDate(date);
	    elem.setLocation(location);
	    elem.setText(text);
	    elem.setNotSynced(notSynced);
	    
	    Log.d("BMHI", "Getting bookmarkHistory item - "+elem.toString());
	    return elem;
	}

	public void copyBookElement (BookElement bookElem) {
		if (bookElem != null) {
	        this.ID = bookElem.ID;
	        this.location = (int)bookElem.law + bookElem.chapter + bookElem.paragraph; // Needs to be revised
	        this.text = bookElem.text;
	    }
	}
}
