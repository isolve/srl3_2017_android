package se.isolve.srl3;

import se.isolve.srl3.adapter.ItemAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;

public class ListGuide extends SRLMenuActivity {
	private String NameFromList = "list";
	int extraInt;
	LinearLayout llOption;
	Button options;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.listguide);
		extraInt = getIntent().getIntExtra(NameFromList, 0);
		llOption = (LinearLayout)findViewById(R.id.lloption2);
		options = (Button)findViewById(R.id.btnCloseList);
		ListView listguide = (ListView) findViewById(R.id.lvguide);
		int[] prgmImages = { R.drawable.item1, R.drawable.item2,
				R.drawable.item3, R.drawable.item5, R.drawable.item6,
				R.drawable.item7, R.drawable.item8, R.drawable.item9 };
		String[] prgmNameList = {  "Bläddring", "Gå till", "Register",
				"Bokmärken", "Anteckningar",  "Historik","Inställningar",
				"Om appen" };
		 ItemAdapter adapter = new ItemAdapter(this, prgmNameList, prgmImages);
		 listguide.setAdapter(adapter);
		 listguide.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub
				
				boolean fromPages = getIntent().getBooleanExtra("from_pages", false);
				
				if (arg2 == 1) {
					Intent intent = new Intent(ListGuide.this, GaTillActivity.class);
					intent.putExtra(NameFromList, extraInt);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
					startActivity(intent);
					overridePendingTransition(0, 0);
					ListGuide.this.finish();
				} else if (arg2 == 0 ) {
					Intent intent = new Intent(ListGuide.this, ChooserPageScreen.class);
					intent.putExtra(NameFromList, extraInt);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
					startActivity(intent);
					overridePendingTransition(0, 0);
					ListGuide.this.finish();
				} else if (arg2 == 2 ) {
					Intent intent = new Intent(ListGuide.this, RegisterActivity.class);
					intent.putExtra(NameFromList, extraInt);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
					startActivity(intent);
					overridePendingTransition(0, 0);
					ListGuide.this.finish();
				} else if (arg2 == 3) {
					Intent intent = new Intent(ListGuide.this, BookmarkActivity.class);
					intent.putExtra(NameFromList, extraInt);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
					startActivity(intent);
					overridePendingTransition(0, 0);
					ListGuide.this.finish();
				} else if (arg2 == 4) {
					Intent intent = new Intent(ListGuide.this, NotesActivity.class);
					intent.putExtra(NameFromList, extraInt);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
					startActivity(intent);
					overridePendingTransition(0, 0);
					ListGuide.this.finish();
				} else if (arg2 == 5) {
					Intent intent = new Intent(ListGuide.this, HistorikActivity.class);
					intent.putExtra(NameFromList, extraInt);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
					startActivity(intent);
					overridePendingTransition(0, 0);
					ListGuide.this.finish();
				} else if (arg2 == 6) {
					Intent intent = new Intent(ListGuide.this, InstallningarActivity.class);
//					intent.putExtra(NameFromList, extraInt);
					intent.putExtra(NameFromList, 0);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
					startActivity(intent);
					overridePendingTransition(0, 0);
					ListGuide.this.finish();
				} else if (arg2 == 7) {
					Intent intent = new Intent(ListGuide.this, AboutActivity.class);
					intent.putExtra(NameFromList, 0);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
					startActivity(intent);
					overridePendingTransition(0, 0);
					ListGuide.this.finish();
				}
			}
		});
		 
		 llOption.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				handleNavBack();
			}
		});
		 
		 options.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					handleNavBack();
				}
			});
	}
	
	public void onClose(View view){
//		Intent intent = new Intent(ListGuide.this, ChooserPageScreen.class);
//		startActivity(intent);
//		this.finish();
	}
}
