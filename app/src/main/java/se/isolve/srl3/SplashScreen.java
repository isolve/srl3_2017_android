package se.isolve.srl3;

import se.isolve.introview.IntroViewActivity;
import se.isolve.srl3.database.BookDB;
import se.isolve.srl3.database.BookElement;
import se.isolve.srl3.reader.AutoFitTextView;
import se.isolve.srl3.reader.BookmarkHistoryNotesManager;
import se.isolve.srl3.reader.Pages;
import utils.LoginHandler;
import utils.LoginHandler.LoginCallBack;
import utils.SRLSynchronize;
import utils.SRLUtils;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

/**
 * 
 * @author
 * 
 */
public class SplashScreen extends Activity implements OnCompletionListener,
		OnPreparedListener, View.OnTouchListener, PlayVideoHandler , LoginCallBack{

	MySurfaceView ourSurfaceView;
	private LinearLayout buttonsContainer;
	private ImageView blIcon;
	private AutoFitTextView textDisplay;
	private Button previous, next, openPage;
	private BookDB bookDB;
	private String labelText = "";
	public static final int SIDE_MARGIN = 60;
	int ID;
	float x, y;

	private RelativeLayout main;
	private VideoView mVV;
	private View loginView;
	private TextView loginHeader, forgotPassword;
	private EditText epost;
	private EditText losenord;
	private Button loginButton;
	
	private static int VIDEO_STATE = 0;
	private static final int VIDEO_P1_FINISH_PLAYING = 1;
	private static final int VIDEO_P1_IS_PLAYING = 2;
	private static final int VIDEO_P2_FINISH_PLAYING = 3;
	private static final int VIDEO_P2_IS_PLAYING = 4;
	int width = 0;
	int height = 0;
	float activeWidth;
	float marginLeft;
	float marginRight;
	float yMin;
	float yMax;
	
	String epostS;
	private String passwS, udid;
	private String loggedInUser, loggedInPass, loggedInUdid;

	public static boolean isBackFromReader = false;
	
	private boolean init = false;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		setContentView(R.layout.activity_main);
		//RelativeLayout vi = (RelativeLayout) findViewById(R.id.log_layout);
		//TextView vie = (TextView) vi.findViewById(R.id.login_header);

		Log.d("SplashScreen", "onCreate");
				
		init = true;
		
		Display display = getWindowManager().getDefaultDisplay();
    	Point size = new Point();
    	display.getSize(size);
    	width = size.x;
    	height = size.y;
    	activeWidth = width * 0.86f;
    	marginLeft = width * 0.07f;
    	marginRight = width * 0.93f;
    	yMin = height * 0.33f;
    	yMax = height * 0.8f;
    	
		initViews();
		if (ourSurfaceView != null && !ourSurfaceView.isRunning) {
			ourSurfaceView.start();
		}
		initSplashVideo("join");		

		// openFile();
		handleOpenPage();
		
		// Open database.
		bookDB = new BookDB(this);
		bookDB.createDatabase();
		
    	handleNextAndPreviousButtons();
    	
 	}
	
	private void handleNextAndPreviousButtons () {
		next.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				BookElement elem = bookDB.nextFlipElementForID(ID);
				if (elem != null) {
					updateViewForElement(elem, true);
				}
			}
		});
		
		previous.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				BookElement elem = bookDB.previousFlipElementForID(ID);
				if (elem != null) {
					updateViewForElement(elem, false);
				}
			}
		});
	}

	private void updateViewForElement (BookElement elem, boolean next) {
		
		ID = elem.ID;
		setLabelForElement(elem);
			
		float pos = (float)bookDB.positionForID(ID);

		x = marginLeft + pos * activeWidth;
		
		// int projectedX = (int) ((double) x * ((double) iconBgr.getWidth() / (double) mVV.getWidth()));
		// int projectedY = (int) y;
		//int colorCodeArgb = iconBgr.getPixel(projectedX, projectedY);
		//Log.i("colorCodeArgb", "" + colorCodeArgb);
		
		//if (colorCodeArgb <= -9666134 && colorCodeArgb >= -16184046 || x > marright || x < redunceW || y < ytranparent) {
		if (x < marginLeft || x > marginRight || y < yMin || y > yMax) {
			ourSurfaceView.setIsTransparent(true);
			hideControls();
		} else {
			// Update the position of the crack.
			//Log.d("SS", "next x position - "+x);
			//ourSurfaceView.drawBitmap(x);
			ourSurfaceView.updateXY(x, y);
			if (textDisplay.getVisibility() == View.INVISIBLE || textDisplay.getVisibility() == View.GONE) {
				textDisplay.setVisibility(View.VISIBLE);
			}
		}
		
//		float x = bookDB.positionForElement(elem) * ,
//				y = this.ourSurfaceView.y;
	}
	
	Bitmap iconBgr;
	ImageView imgButton;
	// Button btnClick;
	private void initViews() {
		loginView = (View)findViewById(R.id.banner);
		loginHeader = (TextView)findViewById(R.id.login_header);
		epost = (EditText)findViewById(R.id.epost);
		losenord = (EditText)findViewById(R.id.losenord);
		loginButton = (Button)findViewById(R.id.login_button);
		forgotPassword = (TextView)findViewById(R.id.forgot_password);
		Typeface tfMyriadPro = SRL3ApplicationClass.getMyriadPro();
		
		forgotPassword.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				String url = "http://www.nj.se/cms/pub/glomt-losenord";
		        Intent i = new Intent(Intent.ACTION_VIEW);
		        i.setData(Uri.parse(url));
		        startActivity(i);
			}
		});
		
		loginHeader.setTypeface(tfMyriadPro);
		epost.setTypeface(tfMyriadPro);
		losenord.setTypeface(tfMyriadPro);
		loginButton.setTypeface(tfMyriadPro);
		forgotPassword.setTypeface(tfMyriadPro);
		
		int h = (int) (height * 0.2);
		int xMargin = (int)(width * 0.07);
		
		buttonsContainer = (LinearLayout)findViewById(R.id.chooser_buttons_container);

		RelativeLayout.LayoutParams layout1 = (RelativeLayout.LayoutParams)buttonsContainer.getLayoutParams();
		layout1.leftMargin = xMargin;
		layout1.rightMargin = xMargin;
		buttonsContainer.setLayoutParams(layout1);

		previous = (Button)findViewById(R.id.chooser_previous_article);
		next = (Button)findViewById(R.id.chooser_next_article);
		openPage = (Button)findViewById(R.id.chooser_open_article);
		openPage.setTypeface(tfMyriadPro);
		blIcon = (ImageView)findViewById(R.id.img_icon_bladdring);
		textDisplay = (AutoFitTextView)findViewById(R.id.chooser_text_displayer);
		textDisplay.applyFont(this);
		textDisplay.setMinTextSize(24);
		textDisplay.setMaxTextSize(35);
		textDisplay.setMinLines(2);
		textDisplay.setTypeface(tfMyriadPro);
		RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams)textDisplay.getLayoutParams();
		layoutParams.topMargin = h;
		layoutParams.leftMargin = xMargin;
		layoutParams.rightMargin = xMargin;
		textDisplay.setLayoutParams(layoutParams);
		
		// Hide the buttons container and the icon.
		blIcon.setVisibility(View.GONE);
		buttonsContainer.setVisibility(View.GONE);
		textDisplay.setVisibility(View.GONE);
		
		 imgButton = (ImageView)findViewById(R.id.imageView1);
		 imgButton.setBackgroundResource(R.drawable.tranparenimg2);
//		 imgButton.setBackgroundDrawable(getResources().getDrawable(R.drawable.tranparenimg2));
		iconBgr = BitmapFactory.decodeResource(getBaseContext().getResources(), R.drawable.brg);
		mVV = (VideoView) findViewById(R.id.myvideoview);
		mVV.setOnCompletionListener(this);
		mVV.setOnPreparedListener(this);
		// mVV.setOnTouchListener(this);

		ourSurfaceView = (MySurfaceView) findViewById(R.id.myBringBack1);
		// ourSurfaceView.setOnTouchListener(this);
		ourSurfaceView.playHandler = this;
		x = 0;
		y = 0;
	}

	protected void initSplashVideo(String name) {
		int res = this.getResources().getIdentifier(name, "raw",
				getPackageName());
		if (!playFileRes(res))
			return;
	}

	@Override
	protected void onPause() {
		super.onPause();
		
		init = false;
		
		if (ourSurfaceView != null && ourSurfaceView.isRunning) {
						
			ourSurfaceView.pause();
		}
		// Hide page crack and option buttons when the activity pauses.
		hideControls();
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub	
		currentPosition = mVV.getCurrentPosition();
		super.onStop();

		VIDEO_STATE = 0;
	}

	@Override
	protected void onResume() {
		Log.d("SplashScreen", "onResume");
		super.onResume();
		if (ourSurfaceView != null && !ourSurfaceView.isRunning) {
			Log.d("SplashScreen", "resume SurfaceView");
			
			if (mVV == null)
				Log.d("mVV null", "resume SurfaceView");
			
			
			ourSurfaceView.resume();
		}
		
		mVV.seekTo(currentPosition);

		// if(isBackFromReader){
		// initViews();
		// initSplashVideo("join3");
		// isBackFromReader = false;
		// }

	}

	private void setLabelForElement (BookElement elem) {
	    if (elem.weight == 1 || elem.weight == 2 || elem.weight == 5) { // top level
	        labelText = bookDB.articleNameForElement(elem, true, false);
	    } else {
	    	labelText = bookDB.articleNameForElement(elem, false, false) + "\n" + bookDB.lawNameForElement(elem, true, false);
	    }
	    textDisplay.setText(labelText);
	    
	    if (textDisplay.getVisibility() == View.INVISIBLE || textDisplay.getVisibility() == View.GONE) {
			textDisplay.setVisibility(View.VISIBLE);
		} 
	}
	
	@Override
	public boolean onTouch(View view, MotionEvent motionEvent) {

		x = motionEvent.getX();
		y = motionEvent.getY();
		
		switch (motionEvent.getAction()) {
		case MotionEvent.ACTION_MOVE:
			if (VIDEO_STATE == VIDEO_P1_FINISH_PLAYING) {
				
				// Hide the option buttons when the paging is in progress.
				buttonsContainer.setVisibility(View.GONE);
				if (!labelText.isEmpty()) {
					textDisplay.setVisibility(View.VISIBLE);
				}
				
				ourSurfaceView.updateXY(x, y);
				// int projectedX = (int) ((double) x * ((double) iconBgr.getWidth() / (double) mVV.getWidth()));
				// int projectedY = (int) ((double) y * ((double) iconBgr.getHeight() / (double) mVV.getHeight()));
				//int colorCodeArgb = iconBgr.//(projectedX, projectedY);
				//Log.i("colorCodeArgb", "" + colorCodeArgb);
				//if (colorCodeArgb <= -9666134 && colorCodeArgb >= -16184046 || x > marright || x < redunceW || y < ytranparent) {
				if (x < marginLeft || x > marginRight || y < yMin || y > yMax) {
					ourSurfaceView.setIsTransparent(true);
					//textDisplay.setVisibility(View.INVISIBLE);
					return true;
				} else {
					ourSurfaceView.setIsTransparent(false);
					
					//SplashScreen.this.textDisplay.setVisibility(View.VISIBLE);
						
					// float pos = (motionEvent.getX() - redunceW) / relativeW;
					float pos = (x - marginLeft) / activeWidth;
					BookElement book = bookDB.elementForBookPosition(pos);
					setLabelForElement(book);
					ID = book.getId();
					//SplashScreen.this.book = book;
				}
			} else if (VIDEO_STATE == VIDEO_P2_IS_PLAYING) {
				x = -1;
				y = -1;
				ourSurfaceView.updateXY(x, y);
			}
			break;
		case MotionEvent.ACTION_DOWN:
			if (VIDEO_STATE == VIDEO_P1_FINISH_PLAYING) {
				
				// Hide the option buttons when the paging is in progress.
				buttonsContainer.setVisibility(View.GONE);
				if (!labelText.isEmpty()) {
					textDisplay.setVisibility(View.VISIBLE);
				}
				
				ourSurfaceView.updateXY(x, y);
				// int projectedX = (int) ((double) x * ((double) iconBgr.getWidth() / (double) mVV.getWidth()));
				// int projectedY = (int) ((double) y * ((double) iconBgr.getHeight() / (double) mVV.getHeight()));
				//int colorCodeArgb = iconBgr.getPixel(projectedX, projectedY);
				//Log.i("colorCodeArgb", "" + colorCodeArgb);
				//if (colorCodeArgb <= -9666134 && colorCodeArgb >= -16184046 || x > marright || x < redunceW || y < ytranparent) {
				if (x < marginLeft || x > marginRight || y < yMin || y > yMax) {	
					ourSurfaceView.setIsTransparent(true);
					//textDisplay.setVisibility(View.INVISIBLE);
					return true;
				} else {
					ourSurfaceView.setIsTransparent(false);
					
					//SplashScreen.this.textDisplay.setVisibility(View.VISIBLE);
					
					// float pos = (motionEvent.getX() - redunceW) / relativeW;
					float pos = (x - marginLeft) / activeWidth;
					BookElement book = bookDB.elementForBookPosition(pos);
					setLabelForElement(book);
					ID = book.getId();
					//SplashScreen.this.book = book;
				}
			} else if (VIDEO_STATE == VIDEO_P2_IS_PLAYING) {
				x = -1;
				y = -1;
				ourSurfaceView.updateXY(x, y);
			}
			break;
		case MotionEvent.ACTION_UP:
			if (VIDEO_STATE == VIDEO_P1_FINISH_PLAYING) {
				ourSurfaceView.updateXY(x, y);
				// int projectedX = (int) ((double) x * ((double) iconBgr.getWidth() / (double) mVV.getWidth()));
				// int projectedY = (int) ((double) y * ((double) iconBgr.getHeight() / (double) mVV.getHeight()));
				//int colorCodeArgb = iconBgr.getPixel(projectedX, projectedY);

				//if (colorCodeArgb <= -9666134 && colorCodeArgb >= -16184046) {
				if (x < marginLeft || x > marginRight || y < yMin || y > yMax) {
					//Log.d("Thumbing", "should hide surface view");
					ourSurfaceView.setIsTransparent(true);
					buttonsContainer.setVisibility(View.GONE);
					textDisplay.setVisibility(View.GONE);
					labelText = "";
					return true;
				} else {
					ourSurfaceView.setIsTransparent(false);
					
				}
				
				// Show option buttons when the thumbing is complete.
				buttonsContainer.setVisibility(View.VISIBLE);
//				initSplashVideo("join2");
//				mVV.start();
//
//				x = -1;
//				y = -1;
//				ourSurfaceView.updateXY(x, y);
//				VIDEO_STATE = VIDEO_P2_IS_PLAYING;
//				imgButton.setVisibility(View.INVISIBLE);
				
		    	  
			} else if (VIDEO_STATE == VIDEO_P2_IS_PLAYING) {
				
				x = -1;
				y = -1;
				ourSurfaceView.updateXY(x, y);
			}
			break;
		}
		//Log.i("MotionEvent x y", x + "  " + y);
		ourSurfaceView.updateXY(x, y);

		return true;
	}

	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		setIntent(intent);
		int fileRes = 0;
		Bundle e = getIntent().getExtras();
		if (e != null) {
			fileRes = e.getInt("fileRes");
		}
		playFileRes(fileRes);
		Log.d("SplashScreen", "onNewIntent");
	}

	private boolean playFileRes(int fileRes) {
		if (fileRes == 0) {
			stopPlaying();
			return false;
		}
		else {
			mVV.setVideoURI(Uri.parse("android.resource://" + getPackageName()
					+ "/" + fileRes));

			return true;
		}
	}

	public void stopPlaying() {
		mVV.stopPlayback();
	}

	@Override
	public void onCompletion(MediaPlayer mp) {
		videoPlayCompleted();
	}
	
	private void videoPlayCompleted () {
		Intent intent;
		Log.d("***video_state", "state: " + VIDEO_STATE);
		switch (VIDEO_STATE) {
		case VIDEO_P1_IS_PLAYING:
			if (SRLUtils.isUserLoggedIn(SplashScreen.this)) {
				VIDEO_STATE = VIDEO_P1_FINISH_PLAYING;
				blIcon.setVisibility(View.VISIBLE);
				imgButton.setBackgroundResource(R.drawable.ic_drawer);
//				imgButton.setBackground(getResources().getDrawable(R.drawable.ic_drawer));
				imgButton.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						Intent intent = new Intent(SplashScreen.this, ListGuide.class);
						startActivity(intent);
						overridePendingTransition(R.anim.your_left_to_right, R.anim.your_right_to_left);
						SplashScreen.this.finish();
					}
				});
				mVV.setOnTouchListener(this);
				ourSurfaceView.setOnTouchListener(this);
			}
						
			break;
		case VIDEO_P2_IS_PLAYING:
			
			intent = new Intent(getApplicationContext(), Pages.class);
			intent.putExtra(BookElement.BOOK_ID, ID);
			intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(intent);
			overridePendingTransition (0, 0);
//			startActivity(new Intent(SplashScreen.this, ChangeView.class));
			finish();
			break;

		default:
			break;
		}
	}
	

	@Override
	public void onPrepared(MediaPlayer mp) {
		mp.setLooping(false);
	}

	public int currentPosition = 0;
	
	public void pause() {
		if (mVV.isPlaying()) {
			mVV.pause();
			currentPosition = mVV.getCurrentPosition();
			Log.d("Isolve currentPosition", "currentPosition " + currentPosition);

		} else {
			mVV.seekTo(100);
			mVV.pause();
		}
		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				continuousPlay();
			}
		}, 1500);

	}

	public void continuousPlay() {
		// CJA 2015.4.27 Refactoring for bug searching
		//mVV.seekTo(currentPosition);
		Throwable stack = new Throwable().fillInStackTrace();
		StackTraceElement[] trace = stack.getStackTrace();
		//Log.d("SS", "mvv.start "+ trace[0].getMethodName());
		// CJA 2015.4.27 Refactoring for bug searching
		//mVV.start();
		if (LoginHandler.getInstance().hasInternetConnection()) {
			if(!LoginHandler.getInstance().isUserLoggedIn()) {
				mVV.start();
				
				mVV.postDelayed(new Runnable(){
					@Override
					public void run() {

						loginView.setVisibility(View.VISIBLE);
						loginHeader.setVisibility(View.VISIBLE);
						epost.setVisibility(View.VISIBLE);
						losenord.setVisibility(View.VISIBLE);
						loginButton.setVisibility(View.VISIBLE);
						forgotPassword.setVisibility(View.VISIBLE);
						mVV.pause();

						epost.setOnFocusChangeListener(new OnFocusChangeListener() {
							@Override
							public void onFocusChange(View v, boolean hasFocus) {
								epost.post(new Runnable() {
									@Override
									public void run() {
										getBaseContext();
										InputMethodManager imm = (InputMethodManager) SplashScreen.this.getSystemService(Context.INPUT_METHOD_SERVICE);
										imm.showSoftInput(epost, InputMethodManager.SHOW_IMPLICIT);
									}
								});
							}
						});
						epost.requestFocus();

						LinearLayout linearLayout = new LinearLayout(getApplicationContext());

						//RelativeLayout.LayoutParams relativeParams = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
						//relativeParams.topMargin = -100;

						//(relativeParams);
					}
				},400);
			} else {
				LoginHandler.getInstance().setLoginCallbackListener(this);
				LoginHandler.getInstance().checkLogin(null,null);
			}
		}
		else {
			
			if (LoginHandler.getInstance().checkSessionTimestamp()) {
				loginOK();
			} else {
				loginView.setVisibility(View.VISIBLE);
				loginHeader.setVisibility(View.VISIBLE);
				epost.setVisibility(View.VISIBLE);
				losenord.setVisibility(View.VISIBLE);
				loginButton.setVisibility(View.VISIBLE);
				forgotPassword.setVisibility(View.VISIBLE);
				mVV.pause();

				epost.setOnFocusChangeListener(new OnFocusChangeListener() {
					@Override
					public void onFocusChange(View v, boolean hasFocus) {
						epost.post(new Runnable() {
							@Override
							public void run() {
								getBaseContext();
								InputMethodManager imm = (InputMethodManager) SplashScreen.this.getSystemService(Context.INPUT_METHOD_SERVICE);
								imm.showSoftInput(epost, InputMethodManager.SHOW_IMPLICIT);
							}
						});	
					}
				});
				Toast.makeText(getApplicationContext(), "Problem med nätverksuppkopplingen. Vänligen prova igen senare.", Toast.LENGTH_LONG).show();
				epost.requestFocus();
			}

		}
	}

	@Override
	public void playVideo(final int mode) {

		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				if (mode == 0) {
					Throwable stack = new Throwable().fillInStackTrace();
		            StackTraceElement[] trace = stack.getStackTrace();
					//Log.d("SS", "mvv.start "+ trace[0].getMethodName());
					mVV.start();
					VIDEO_STATE = VIDEO_P1_IS_PLAYING;
					new Handler().postDelayed(new Runnable() {

						@Override
						public void run() {
							// TODO Auto-generated method stub
							//Log.i("Isolve", "pause");
							pause();
						}
					}, 100);
				}
			}
		});

	}

	@Override
	public void handlerResumed() {
		// TODO Auto-generated method stub
		Log.i("Splash", "Has resumed");
		
//		VIDEO_STATE = VIDEO_P1_IS_PLAYING;
		
		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				
				int seekTo = 1;
				
				if (SRLUtils.isUserLoggedIn(getApplicationContext())) {
					initSplashVideo("join2");
				}
				
				Throwable stack = new Throwable().fillInStackTrace();
		        StackTraceElement[] trace = stack.getStackTrace();
				//Log.d("SS", "mvv.start "+ trace[0].getMethodName());
				mVV.seekTo(seekTo);
				VIDEO_STATE = VIDEO_P1_IS_PLAYING;
				
				videoPlayCompleted();
//				new Handler().postDelayed(new Runnable() {
//
//					@Override
//					public void run() {
//						// TODO Auto-generated method stub
//						//Log.i("Isolve", "pause");
//						pause();
//					}
//				}, 1);
			}
		});
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		this.finish();
	}
	
	private void hideControls () {
		// Hide the page crack and option buttons.
		x = 0;
		y = 0;
		ourSurfaceView.updateXY(x, y);
		buttonsContainer.setVisibility(View.GONE);
		textDisplay.setVisibility(View.GONE);
		labelText = "";
	}
	
	private void handleOpenPage () {
		openPage.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				initSplashVideo("join2");
				Throwable stack = new Throwable().fillInStackTrace();
	            StackTraceElement[] trace = stack.getStackTrace();
				//Log.d("SS", "mvv.start "+ trace[0].getMethodName());
				mVV.start();

				x = -1;
				y = -1;
				ourSurfaceView.updateXY(x, y);
				VIDEO_STATE = VIDEO_P2_IS_PLAYING;
				imgButton.setVisibility(View.INVISIBLE);
				blIcon.setVisibility(View.GONE);
				hideControls();
			}
		});
	}
	
	/*private boolean isUserLoggedIn() {
		SharedPreferences settings = getSharedPreferences("LOGIN",Context.MODE_PRIVATE);
		loggedInUser = settings.getString("username", null);
		loggedInPass = settings.getString("password", null);
		loggedInUdid = settings.getString("udid", null);
		if (loggedInUser != null && loggedInPass != null && loggedInUdid != null) {
			return true;
		}
		else {
			return false;
		}
	}
	*/
	
	public void login(View v) {
		loginButton.setBackground(getResources().getDrawable(R.drawable.login_button));
		if	(epost.getText().toString().length() < 1 && losenord.getText().toString().length() > 0) {
			Toast.makeText(getApplicationContext(), "Vänligen ange e-post.", Toast.LENGTH_LONG).show();
			epost.requestFocus();
		}
		else if ( losenord.getText().toString().length() < 1 && epost.getText().toString().length() > 0) {
			Toast.makeText(getApplicationContext(), "Vänligen ange lösenord.", Toast.LENGTH_LONG).show();
			losenord.requestFocus();
		}
		else if (losenord.getText().toString().length() < 1 && epost.getText().toString().length() < 1) {
			Toast.makeText(getApplicationContext(), "Vänligen ange e-post och lösenord.", Toast.LENGTH_LONG).show();
			epost.requestFocus();
		}
		else {
			if (LoginHandler.getInstance().hasInternetConnection()) {
				LoginHandler.getInstance().setLoginCallbackListener(this);
				String epostClean = epost.getText().toString().replace(" ", "");
				String passwClea = losenord.getText().toString().replace(" ", "");
				epost.setText(epostClean);
				LoginHandler.getInstance().checkLogin(epost.getText().toString(), losenord.getText().toString());
				//checkLogin();
			}
			else {
				Toast.makeText(getApplicationContext(), "Problem med nätverksuppkopplingen. Vänligen prova igen senare.", Toast.LENGTH_LONG).show();			        		      			
			}		
		}
	}
	
	/*private void checkLogin() {
		
		AsyncHttpClient client = new AsyncHttpClient();
		if (!isUserLoggedIn()) {
			loggedInUser = epost.getText().toString();
			loggedInPass = losenord.getText().toString();
			loggedInUdid = Secure.getString(SRL3ApplicationClass.getContext().getContentResolver(),
	                Secure.ANDROID_ID); 
		}
		
		String urlString = "https://isolve-srl2-service.appspot.com/login?username=" + loggedInUser + "&pw=" + loggedInPass + "&year=14" + "&udid=" + loggedInUdid;
		client.get(urlString, new AsyncHttpResponseHandler() {

		    @Override
		    public void onStart() {
		        // called before request is started
		    }

		    @Override
		    public void onRetry(int retryNo) {
		        // called when request is retried
			}

			@Override
			public void onFailure(int arg0, Header[] arg1, byte[] arg2,
					Throwable arg3) {
				// Toast.makeText(getApplicationContext(), arg3.getMessage(), Toast.LENGTH_LONG).show();
				Toast.makeText(getApplicationContext(), R.string.connection_to_server_error, Toast.LENGTH_LONG).show();
			}

			@Override
			public void onSuccess(int arg0, Header[] arg1, byte[] arg2) {
				
				String response = null;
				String responseCode = null;
				try {
					response = new String(arg2, "UTF-8");
					responseCode = Character.toString(response.charAt(0)) + Character.toString(response.charAt(1));
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				Log.d("SplashScreen", "Login response - "+response);
				
				if (Character.toString(response.charAt(0)).equalsIgnoreCase("1")) {
					loginOK();
				}
				else if (responseCode.equalsIgnoreCase("-1")) {
					Toast.makeText(getApplicationContext(), "Felaktig e-post eller lösenord", Toast.LENGTH_LONG).show();
				}
				else if (responseCode.equalsIgnoreCase("-2")) {
					Toast.makeText(getApplicationContext(), "Kontot saknar behörighet till denna lagbok", Toast.LENGTH_LONG).show();
				}
				else if (responseCode.equalsIgnoreCase("-3")) {
					Toast.makeText(getApplicationContext(), "Problem med inloggningstjänsten. Vänligen prova igen senare.", Toast.LENGTH_LONG).show();
				}
				else if (responseCode.equalsIgnoreCase("-4")) {
					Toast.makeText(getApplicationContext(), "För många öppna sessioner. Vänligen logga ut från en aktiv session.", Toast.LENGTH_LONG).show();
				}
				
			}
		});
		
	}*/
	
	private void loginOK() {
		
		LoginHandler.getInstance().setIsLoggedIn();
		LoginHandler.getInstance().setTimeStamp();
				
		//RelativeLayout.LayoutParams relativeParams = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		//relativeParams.topMargin = 0;

		//mVV.setLayoutParams(relativeParams);		
		loginView.setVisibility(View.INVISIBLE);
		loginHeader.setVisibility(View.INVISIBLE);
		epost.setVisibility(View.INVISIBLE);
		InputMethodManager imm = (InputMethodManager)getSystemService(
		      Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(epost.getWindowToken(), 0);
		losenord.setVisibility(View.INVISIBLE);
		loginButton.setVisibility(View.INVISIBLE);
		forgotPassword.setVisibility(View.INVISIBLE);
	    
		// Initialize BookmarkHistoryNotesManager on each login.
		BookmarkHistoryNotesManager.initInstance();
		
		// Initialize the object to syncronize notes to the cloud.
		SRLSynchronize.initSyncSession(getApplicationContext());

		// PWA: 2017-01-16: Migrate old bookmarks and notes from web service (once)
		SharedPreferences prefs2 = SRL3ApplicationClass.getContext().getSharedPreferences("SRLPref", Context.MODE_PRIVATE);
		int migrated2016NotesAndBookmarks = prefs2.getInt("migrated2016NotesAndBookmarks", 0);
		if (migrated2016NotesAndBookmarks == 0) {
			SRLSynchronize.getInstance().get2017NotesAndBookmarks(SRL3ApplicationClass.getContext());
			SharedPreferences.Editor editor = prefs2.edit();
			editor.putInt("migrated2016NotesAndBookmarks", 1);
			editor.commit();
		}

		SRLSynchronize.getInstance().sendNewNotesAndBookmarks(getApplicationContext());
		
		mVV.start();
		// Check/start Intro
		SharedPreferences prefs = this.getSharedPreferences(IntroViewActivity.INTRO_KEY, Context.MODE_PRIVATE);
		if (!prefs.getString(IntroViewActivity.INTRO_VERSION_KEY, "").equals(IntroViewActivity.INTRO_VERSION)) {
			Intent intent = new Intent(getApplicationContext(), IntroViewActivity.class);
			startActivity(intent);
			finish();
		}
		
		// mVV.start();
	}
	
	/*private void setTimeStamp() {
		Date date = new Date();
		long timestamp = date.getTime();
		SharedPreferences setTS = getSharedPreferences("LOGIN",Context.MODE_PRIVATE);
		Editor edit = setTS.edit();
		edit.putLong("timestamp", timestamp);
		edit.commit();
	}*/
	
	/*private long getTimestamp() {
		SharedPreferences getTS = getSharedPreferences("LOGIN", Context.MODE_PRIVATE);
		return getTS.getLong("timestamp", 0);
	}*/
	
	/*private void setIsLoggedIn() {
		SharedPreferences settings = getSharedPreferences("LOGIN",Context.MODE_PRIVATE);
		Editor editor = settings.edit();
		editor.putString("username", loggedInUser);
		editor.putString("password", loggedInPass);
		editor.putString("udid", loggedInUdid);
		editor.commit();
	}*/
	
	/*boolean hasInternetConnection() {
    	final ConnectivityManager connMgr = (ConnectivityManager)
    	this.getSystemService(Context.CONNECTIVITY_SERVICE);

    	final android.net.NetworkInfo wifi = connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
    	final android.net.NetworkInfo mobile = connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

    	if (wifi.isAvailable()) {
    		return true;
    	} 
    	else if(mobile != null) {
    		return mobile.isAvailable();
    	}
    	else {
    		return false;
    	}
    }*/

	//Callback from LoginHandler
	@Override
	public void onLoginQueryCompleted(String arg) {
		if (Character.toString(arg.charAt(0)).equalsIgnoreCase("1")) {
			loginOK();
		}
		else if (arg.equalsIgnoreCase("-1")) {
			Toast.makeText(SRL3ApplicationClass.getContext(), "Felaktig e-post eller lösenord", Toast.LENGTH_LONG).show();
			if(SRLUtils.isUserLoggedIn(SplashScreen.this)) {
				LoginHandler.getInstance().logout();
			}
		}
		else if (arg.equalsIgnoreCase("-2")) {
			Toast.makeText(SRL3ApplicationClass.getContext(), "Kontot saknar behörighet till denna lagbok", Toast.LENGTH_LONG).show();
			if(SRLUtils.isUserLoggedIn(SplashScreen.this)) {
				LoginHandler.getInstance().logout();
			}
		}
		else if (arg.equalsIgnoreCase("-3")) {
			Toast.makeText(SRL3ApplicationClass.getContext(), "Problem med inloggningstjänsten. Vänligen prova igen senare.", Toast.LENGTH_LONG).show();
			if(SRLUtils.isUserLoggedIn(SplashScreen.this)) {
				LoginHandler.getInstance().logout();
			}
		}
		else if (arg.equalsIgnoreCase("-4")) {
			Toast.makeText(SRL3ApplicationClass.getContext(), "För många öppna sessioner. Vänligen logga ut från en aktiv session.", Toast.LENGTH_LONG).show();
			if(SRLUtils.isUserLoggedIn(SplashScreen.this)) {
				LoginHandler.getInstance().logout();
			}
		}
	}
	
}
