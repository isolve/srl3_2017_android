package se.isolve.srl3;

public interface PlayVideoHandler {
	void playVideo(int mode);
	void handlerResumed();
}
