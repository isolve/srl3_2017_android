package se.isolve.srl3;

import se.isolve.srl3.database.BookDB;
import se.isolve.srl3.database.BookElement;
import se.isolve.srl3.reader.AutoFitTextView;
import se.isolve.srl3.reader.Pages;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
//import android.util.Log;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.VideoView;

/**
 * 
 * @author
 * 
 */
public class ChooserPageScreen extends Activity implements
OnCompletionListener, OnPreparedListener, View.OnTouchListener,
PlayVideoHandler {

	MySurfaceView ourSurfaceView;
	float x, y;
	private Bitmap iconBgr;
	private VideoView mVV;
	private ImageView blIcon;
	private LinearLayout buttonsContainer;
	private RelativeLayout contentFrame;
	private AutoFitTextView textDisplay;
	private Button previous, next, openPage;
	private BookDB bookDB;
	private String labelText = "";
	private int ID = 0;

	private String NameFromList = "list";
	private static int VIDEO_STATE = 0;
	private static final int VIDEO_P3_FINISH_PLAYING = 1;
	private static final int VIDEO_P3_IS_PLAYING = 2;
	private static final int VIDEO_P2_FINISH_PLAYING = 3;
	private static final int VIDEO_P2_IS_PLAYING = 4;

	int width = 0;
	int height = 0;
	float activeWidth;
	float marginLeft;
	float marginRight;
	float yMin;
	float yMax;

	int extraInt;

	public static boolean isBackFromReader = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		requestWindowFeature(Window.FEATURE_NO_TITLE);

		setContentView(R.layout.activity_main);
		initViews();

		handleOpenPage();

		// Open database.
		bookDB = new BookDB(this);
		bookDB.createDatabase();

		Display display = getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		width = size.x;
		height = size.y;
		activeWidth = width * 0.86f;
		marginLeft = width * 0.07f;
		marginRight = width * 0.93f;
		yMin = height * 0.33f;
		yMax = height * 0.8f;

		handleNextAndPreviousButtons();

		extraInt = getIntent().getIntExtra(NameFromList, 0);
		initViews();
		if (extraInt == 1) {
			initSplashVideo("join3");
		} else {
			mVV.setVisibility(View.INVISIBLE);
			contentFrame.setBackgroundResource(R.drawable.b1);
			this.setupControls();
		}    	    	
	}

	private void handleNextAndPreviousButtons () {
		next.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				BookElement elem = bookDB.nextFlipElementForID(ID);
				if (elem != null) {
					updateViewForElement(elem, true);
				}
			}
		});

		previous.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				BookElement elem = bookDB.previousFlipElementForID(ID);
				if (elem != null) {
					updateViewForElement(elem, false);
				}
			}
		});
	}

	private void updateViewForElement (BookElement elem, boolean next) {

		ID = elem.ID;
		setLabelForElement(elem);

		float pos = (float)bookDB.positionForID(ID);

		x = marginLeft + pos * activeWidth;

		// int projectedX = (int) ((double) x * ((double) iconBgr.getWidth() / (double) mVV.getWidth()));
		// int projectedY = (int) y;
		//int colorCodeArgb = iconBgr.getPixel(projectedX, projectedY);
		//Log.i("colorCodeArgb", "" + colorCodeArgb);

		//if (colorCodeArgb <= -9666134 && colorCodeArgb >= -16184046 || x > marright || x < redunceW || y < ytranparent) {
		if (x < marginLeft || x > marginRight || y < yMin || y > yMax) {
			ourSurfaceView.setIsTransparent(true);
			hideControls();
		} else {
			// Update the position of the crack.
			//Log.d("SS", "next x position - "+x);
			//ourSurfaceView.drawBitmap(x);
			ourSurfaceView.updateXY(x, y);
			if (textDisplay.getVisibility() == View.INVISIBLE || textDisplay.getVisibility() == View.GONE) {
				textDisplay.setVisibility(View.VISIBLE);
			}
		}

		//    		float x = bookDB.positionForElement(elem) * ,
		//    				y = this.ourSurfaceView.y;
	}
	
	ImageView imgButton;

	private void initViews() {
		int h = (int) (height * 0.2);
		int xMargin = (int)(width * 0.07);
		// Initialize the button views.		
		contentFrame = (RelativeLayout)findViewById(R.id.content_frame);
		buttonsContainer = (LinearLayout)findViewById(R.id.chooser_buttons_container);

		RelativeLayout.LayoutParams layout1 = (RelativeLayout.LayoutParams)buttonsContainer.getLayoutParams();
		layout1.leftMargin = xMargin;
		layout1.rightMargin = xMargin;
		buttonsContainer.setLayoutParams(layout1);

		Typeface tfMyriadPro = SRL3ApplicationClass.getMyriadPro();

		previous = (Button)findViewById(R.id.chooser_previous_article);
		next = (Button)findViewById(R.id.chooser_next_article);
		openPage = (Button)findViewById(R.id.chooser_open_article);
		openPage.setTypeface(tfMyriadPro);
		blIcon = (ImageView)findViewById(R.id.img_icon_bladdring);
		textDisplay = (AutoFitTextView)findViewById(R.id.chooser_text_displayer);
		textDisplay.applyFont(this);
		textDisplay.setMinTextSize(16);
		textDisplay.setMaxTextSize(35);
		textDisplay.setTypeface(tfMyriadPro);

		RelativeLayout.LayoutParams layout2 = (RelativeLayout.LayoutParams)textDisplay.getLayoutParams();
		layout2.topMargin = h;
		layout2.leftMargin = xMargin;
		layout2.rightMargin = xMargin;
		textDisplay.setLayoutParams(layout2);

		// Hide the buttons container and icon.
		blIcon.setVisibility(View.GONE);
		buttonsContainer.setVisibility(View.GONE);
		textDisplay.setVisibility(View.GONE);

		imgButton = (ImageView)findViewById(R.id.imageView1);
		imgButton.setBackgroundResource(R.drawable.tranparenimg2);
		mVV = (VideoView) findViewById(R.id.myvideoview);
		if (extraInt == 1) {
			mVV.setOnCompletionListener(this);
			mVV.setOnPreparedListener(this);
			// mVV.setOnTouchListener(this);
		}

		ourSurfaceView = (MySurfaceView) findViewById(R.id.myBringBack1);
		// ourSurfaceView.setOnTouchListener(this);
		ourSurfaceView.playHandler = this;

		Display display = getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		width = size.x;
		height = size.y;
		//		iconBgr = decodeSampledBitmapFromResource(getBaseContext().getResources(),R.drawable.brg,width,height);
		//		iconBgr = BitmapFactory.decodeResource(getBaseContext().getResources(),
		//				R.drawable.bgr);

		new GetIconBgr() { 
			protected void onPostExecute(Bitmap result) {
				iconBgr = result;
			}
		}.execute();

		x = 0;
		y = 0;
	}

	class GetIconBgr extends AsyncTask<Void, Integer, Bitmap> {
		@Override
		protected Bitmap doInBackground(Void... arg0) {

			Bitmap b =decodeSampledBitmapFromResource(getBaseContext().getResources(),R.drawable.brg,width,height);

			return b;
		}	
	}

	protected void initSplashVideo(String name) {
		int res = this.getResources().getIdentifier(name, "raw",
				getPackageName());
		if (!playFileRes(res))
			return;
	}

	@Override
	protected void onPause() {
		super.onPause();
		if (ourSurfaceView != null && ourSurfaceView.isRunning) {
			ourSurfaceView.pause();
		}
		// Hide page crack and option buttons when the activity pauses.
		hideControls();
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();

		VIDEO_STATE = 0;
	}

	@Override
	protected void onResume() {
		super.onResume();

		if(!mVV.isPlaying()) {
			VIDEO_STATE = VIDEO_P3_FINISH_PLAYING;
		}

		if (ourSurfaceView != null && !ourSurfaceView.isRunning) {
			ourSurfaceView.start();
		}

		// if(isBackFromReader){
		// initViews();
		// initSplashVideo("join3");
		// isBackFromReader = false;
		// }

	}

	private void setLabelForElement (BookElement elem) {
		if (elem.weight == 1 || elem.weight == 2 || elem.weight == 5) { // top level
			labelText = bookDB.articleNameForElement(elem, true, false);
		} else {
			labelText = bookDB.articleNameForElement(elem, false, false) + "\n" + bookDB.lawNameForElement(elem, true, false);
		}
		textDisplay.setText(labelText);
	}

	@Override
	public boolean onTouch(View view, MotionEvent motionEvent) {

		x = motionEvent.getX();
		y = motionEvent.getY();

		switch (motionEvent.getAction()) {

		case MotionEvent.ACTION_MOVE:
			////Log.d("CPS", "moving video state - "+VIDEO_STATE);
			if (VIDEO_STATE == VIDEO_P3_FINISH_PLAYING) {
				////Log.d("CPS", "moving 1 ");	
				// Hide the option buttons when the paging is in progress.
				buttonsContainer.setVisibility(View.GONE);


				ourSurfaceView.updateXY(x, y);
	
				if (x < marginLeft || x > marginRight || y < yMin || y > yMax) {
					//OLD CODE
					//ourSurfaceView.setIsTransparent(true);
					//textDisplay.setVisibility(View.INVISIBLE);
					ourSurfaceView.setIsTransparent(true);
					buttonsContainer.setVisibility(View.GONE);
					textDisplay.setVisibility(View.GONE);
					labelText = "";
					return true;
				} else {
					ourSurfaceView.setIsTransparent(false);

					//SplashScreen.this.textDisplay.setVisibility(View.VISIBLE);

					// float pos = (motionEvent.getX() - redunceW) / relativeW;
					float pos = (x - marginLeft) / activeWidth;
					BookElement book = bookDB.elementForBookPosition(pos);
					setLabelForElement(book);
					ID = book.getId();
					//SplashScreen.this.book = book;
				}

				if (labelText.length() > 0) {
					textDisplay.setVisibility(View.VISIBLE);
				}

			} else if (VIDEO_STATE == VIDEO_P2_IS_PLAYING) {
				////Log.d("CPS", "moving 2");
				x = -1;
				y = -1;
				ourSurfaceView.updateXY(x, y);
			}
			break;
		case MotionEvent.ACTION_DOWN:
			////Log.d("CPS", "moving video state - "+VIDEO_STATE);
			if (VIDEO_STATE == VIDEO_P3_FINISH_PLAYING) {
				////Log.d("CPS", "moving 1 ");	
				// Hide the option buttons when the paging is in progress.
				buttonsContainer.setVisibility(View.GONE);

				ourSurfaceView.updateXY(x, y);
				
				if (x < marginLeft || x > marginRight || y < yMin || y > yMax) {
					//OLD CODE
					//ourSurfaceView.setIsTransparent(true);
					//textDisplay.setVisibility(View.INVISIBLE);
					ourSurfaceView.setIsTransparent(true);
					buttonsContainer.setVisibility(View.GONE);
					textDisplay.setVisibility(View.GONE);
					labelText = "";
					return true;
				} else {
					ourSurfaceView.setIsTransparent(false);

					//SplashScreen.this.textDisplay.setVisibility(View.VISIBLE);

					// float pos = (motionEvent.getX() - redunceW) / relativeW;
					float pos = (x - marginLeft) / activeWidth;
					BookElement book = bookDB.elementForBookPosition(pos);
					setLabelForElement(book);
					ID = book.getId();
					//SplashScreen.this.book = book;
				}
				if (labelText.length() > 0) {
					textDisplay.setVisibility(View.VISIBLE);
				}

			} else if (VIDEO_STATE == VIDEO_P2_IS_PLAYING) {
				////Log.d("CPS", "moving 2");
				x = -1;
				y = -1;
				ourSurfaceView.updateXY(x, y);
			}
			break;
		case MotionEvent.ACTION_UP:
			if (VIDEO_STATE == VIDEO_P3_FINISH_PLAYING) {

				if (x < marginLeft || x > marginRight || y < yMin || y > yMax) {
					ourSurfaceView.setIsTransparent(true);
					buttonsContainer.setVisibility(View.GONE);
					textDisplay.setVisibility(View.GONE);
					labelText = "";
					return true;
				} else {
					ourSurfaceView.setIsTransparent(false);
				}
				// Show option buttons when the paging is complete.
				buttonsContainer.setVisibility(View.VISIBLE);

			} else if (VIDEO_STATE == VIDEO_P2_IS_PLAYING) {
				ourSurfaceView.clearSurface();
			}
			break;
		}
		ourSurfaceView.updateXY(x, y);

		return true;
	}

	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		setIntent(intent);
		int fileRes = 0;
		Bundle e = getIntent().getExtras();
		if (e != null) {
			fileRes = e.getInt("fileRes");
		}
		playFileRes(fileRes);
	}

	private boolean playFileRes(int fileRes) {
		if (fileRes == 0) {
			stopPlaying();
			return false;
		} else {
			mVV.setVideoURI(Uri.parse("android.resource://" + getPackageName()
					+ "/" + fileRes));
			return true;
		}
	}

	public void stopPlaying() {
		mVV.stopPlayback();
	}

	private void setupControls () {
		VIDEO_STATE = VIDEO_P3_FINISH_PLAYING;

		blIcon.setVisibility(View.VISIBLE);

		imgButton.setBackgroundResource(R.drawable.ic_drawer);
		//		imgButton.setBackground(getResources().getDrawable(R.drawable.ic_drawer));
		imgButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(ChooserPageScreen.this, ListGuide.class);
				startActivity(intent);
				overridePendingTransition(R.anim.your_left_to_right, R.anim.your_right_to_left);
				ChooserPageScreen.this.finish(); 
			}
		});
		mVV.setOnTouchListener(this);
		ourSurfaceView.setOnTouchListener(this);
	}

	@Override
	public void onCompletion(MediaPlayer mp) {
		videoPlayCompleted();
	}
	
	private void videoPlayCompleted () {
		switch (VIDEO_STATE) {
		case VIDEO_P3_IS_PLAYING:
			this.setupControls();
			break;
		case VIDEO_P2_IS_PLAYING:

			//buttonsContainer.setVisibility(View.VISIBLE);
			Intent intent = new Intent(getApplicationContext(), Pages.class);
			intent.putExtra(BookElement.BOOK_ID, ID);
			intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(intent);
			overridePendingTransition (0, 0);
			finish();
			break;

		default:
			break;
		}
	}
	
	@Override
	public void onPrepared(MediaPlayer mp) {
		mp.setLooping(false);
	}

	public int currentPosition = 0;

	public void pause() {
		if (mVV.isPlaying()) {
			mVV.pause();
			currentPosition = mVV.getCurrentPosition();
			////Log.i("Isolve", currentPosition + "");

		}
		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				continuousPlay();
			}
		}, 1500);

	}

	public void continuousPlay() {
		mVV.seekTo(currentPosition);
		mVV.start();
	}

	@Override
	public void playVideo(final int mode) {
		// runOnUiThread(new Runnable() {
		//
		// @Override
		// public void run() {
		// // TODO Auto-generated method stub
		// if (mode == 0) {
		//
		// mVV.start();
		// // new Handler().postDelayed(new Runnable() {
		// //
		// // @Override
		// // public void run() {
		// // // TODO Auto-generated method stub
		// // ////Log.i("Isolve", "pause");
		// // pause();
		// // }
		// // }, 100);
		// }
		// }
		// });
		if (mode == 0 && extraInt == 1) {
			mVV.start();
			////Log.e("CPS", "Setting video p3 is playing.");
			VIDEO_STATE = VIDEO_P3_IS_PLAYING;
		}
	}
	
	@Override
	public void handlerResumed() {
		// TODO Auto-generated method stub
		videoPlayCompleted();
	}

	public static int calculateInSampleSize(
			BitmapFactory.Options options, int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;
		////Log.i("size raw: ", "" + width + " " + height);
		if (height > reqHeight || width > reqWidth) {

			final int halfHeight = height;
			final int halfWidth = width;
			////Log.i("size raw: ", "" + halfHeight + " " + halfWidth);
			// Calculate the largest inSampleSize value that is a power of 2 and keeps both
			// height and width larger than the requested height and width.
			while ((halfHeight / inSampleSize) > reqHeight
					&& (halfWidth / inSampleSize) > reqWidth) {
				inSampleSize *= 2;
			}
		}

		return inSampleSize;
	}

	public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
			int reqWidth, int reqHeight) {

		// First decode with inJustDecodeBounds=true to check dimensions
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeResource(res, resId, options);
		////Log.i("inSampleSize", "" + calculateInSampleSize(options, reqWidth, reqHeight));
		// Calculate inSampleSize
		options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

		// Decode bitmap with inSampleSize set
		options.inJustDecodeBounds = false;
		return BitmapFactory.decodeResource(res, resId, options);
	}

	private void hideControls () {
		// Hide the page crack and option buttons.
		x = 0;
		y = 0;
		ourSurfaceView.updateXY(x, y);
		buttonsContainer.setVisibility(View.GONE);
		textDisplay.setVisibility(View.GONE);
		labelText = "";
	}

	private void handleOpenPage () {
		openPage.setOnClickListener(new View.OnClickListener() {

			@SuppressLint("NewApi") @Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				// Reset this value so the book-open video can play.
				extraInt = 0;

				mVV.setOnCompletionListener(ChooserPageScreen.this);
				mVV.setOnPreparedListener(ChooserPageScreen.this);
				// mVV.setOnTouchListener(this);

				contentFrame.setBackground(null);
				mVV.setVisibility(View.VISIBLE);
				//				mVV.setBackgroundResource(0);

				initSplashVideo("join2");
				mVV.start();

				x = -1;
				y = -1;
				ourSurfaceView.updateXY(x, y);
				VIDEO_STATE = VIDEO_P2_IS_PLAYING;
				imgButton.setVisibility(View.INVISIBLE);
				blIcon.setVisibility(View.GONE);
				hideControls();
			}
		});
	}
}