package se.isolve.srl3;

import se.isolve.srl3.database.BookDB;
import se.isolve.srl3.database.BookElement;
import se.isolve.srl3.reader.Pages;
import utils.LoginHandler;
import utils.SRLSynchronize;
import utils.SRLSynchronize.SRLNotesSyncListener;
import utils.SRLUtils;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ActivityInfo;
import android.graphics.Point;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

public class InstallningarActivity extends SRLMenuActivity  implements OnCompletionListener, SRLNotesSyncListener{
	private BookDB mDbHelper;
	private LinearLayout rl1;
	private ImageView imgGaTill;
	private int width = 0;
	private int height = 0;
	private SeekBar fontSizeSeekBar;
	private TextView loggedInText;
	
	private LinearLayout llgatil2;
	private String NameFromList = "list";
	private int extraInt;
	private int VIDEO_STATE = 0;
	private static final int VIDEO_P2_IS_PLAYING = 4;
	private TextView lastSyncedTime;
	private Button manualSync;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_installningar);
		Display display = getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		width = size.x;
		height = size.y;
		rl1 = (LinearLayout)findViewById(R.id.rlh1);
		double f = 0.2; // 0.189;
		int h = (int)(height * f);
		rl1.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, h));
		LinearLayout layout_settings = (LinearLayout) findViewById(R.id.layout_settings);
		double widthMarginRatio = 0.14;
		int w = (int) (width * widthMarginRatio); // getResources().getDimension(R.dimen.marginbook);
		LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(width - w, LayoutParams.MATCH_PARENT);
		layoutParams.gravity = Gravity.CENTER;
		layout_settings.setLayoutParams(layoutParams);
		
		fontSizeSeekBar = (SeekBar) layout_settings.findViewById(R.id.font_size_seekbar);
		fontSizeSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				setFontSize(seekBar.getProgress());
			}
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				// TODO Auto-generated method stub
				
			}
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
				
			}
		});
		
		
		loggedInText = (TextView)findViewById(R.id.logged_in);
		String tex = getUser();
		loggedInText.setText(getUser());
		
		imgGaTill = (ImageView)findViewById(R.id.imgGaTill);
		imgGaTill.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// SeekBar seekBar = (SeekBar)findViewById(R.id.font_size_seekbar);
				// setFontSize(seekBar.getProgress());
				// Log.d("fontSize" + fontSizeSeekBar.getProgress(), "");
				Intent intent = new Intent(InstallningarActivity.this, ListGuide.class);
				startActivity(intent);
				overridePendingTransition(R.anim.your_left_to_right, R.anim.your_right_to_left);
//				InstallningarActivity.this.finish();
			}
		});
		
		llgatil2 = (LinearLayout)findViewById(R.id.llgatill2);
//		Resources resources = this.getResources();
//	    DisplayMetrics metrics = resources.getDisplayMetrics();
//	    float dp = 142 / (metrics.densityDpi / 160f);
		extraInt = getIntent().getIntExtra(NameFromList, 0);
		if(extraInt == 1){
		}else{
			llgatil2.setVisibility(View.VISIBLE);
		}
		
		lastSyncedTime = (TextView) findViewById(R.id.last_sync_time);
		
		updateSyncDate();
		
		// Set up the button to force sync.
		manualSync = (Button) findViewById(R.id.manual_sync_button);
		manualSync.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (SRLSynchronize.getInstance().sessionIsInitialized()) {
					SRLSynchronize.sendLatestNotes(InstallningarActivity.this);
				}
			}
		});
	}
	
	@Override
	public void onStop() {
		super.onStop();
		if (SRLSynchronize.getInstance() != null) {
			SRLSynchronize.getInstance().removeSRLNotesSyncListener();
		}
	}
	@Override
	public void onResume() {
		super.onResume();
		if	(SRLSynchronize.getInstance() != null && SRLSynchronize.getInstance().getSyncListener() == null) {
			SRLSynchronize.getInstance().setSRLNotesSyncListener(this);
			}
	}
	
	@Override
	public void onStart() {
		super.onStart();
		int fontSize = 10 + (getFontSize()/10);
		fontSizeSeekBar.setProgress(getFontSize());
		if	(SRLSynchronize.getInstance() != null) {
		SRLSynchronize.getInstance().setSRLNotesSyncListener(this);
		}
	}
	
		private BookElement book;
	@Override	
	public void onCompletion(MediaPlayer arg0) {
		// TODO Auto-generated method stub
		if(VIDEO_STATE == VIDEO_P2_IS_PLAYING){
			Intent intent = new Intent(InstallningarActivity.this, Pages.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			intent.putExtra(BookElement.BOOK_ID, book.ID);
			startActivity(intent);
			overridePendingTransition (0, 0);
			finish();
			return;
		}
		llgatil2.setVisibility(View.VISIBLE);
//		mVV.setAlpha(0f);
//		mVV.setVisibility(View.GONE);
	}
	
	private void updateSyncDate () {
		if (SRLSynchronize.getInstance() != null) {
		long lastSyncTs = SRLSynchronize.getInstance().getSyncTimestamp(InstallningarActivity.this);
		String dateString = SRLUtils.getDateStringFromTimeStamp(lastSyncTs);
		Log.d("Inst", "SYNC. lastSyncTs - "+lastSyncTs+", dateString - "+dateString);
		lastSyncedTime.setText(dateString);
		}
	}
	
	private String getUser() {
		SharedPreferences pref = getApplicationContext().getSharedPreferences("LOGIN", 0);
		return pref.getString("username", null);
	}
	
	private void setFontSize(int fontSize) {
		SharedPreferences pref = getApplicationContext().getSharedPreferences("SRLPref", 0);
		Editor editor = pref.edit();
		
		editor.putInt("fontSize", fontSize);
		editor.commit();
		Toast.makeText(this, "Teckenstorlek ändrad!", Toast.LENGTH_LONG).show();
	}
	
	private int getFontSize() {
		SharedPreferences pref = getApplicationContext().getSharedPreferences("SRLPref", 0);
		int fontS = pref.getInt("fontSize", 30);
		return fontS;
	}

	@Override
	public boolean onKeyUp(int keyCode, KeyEvent msg) {

	     switch(keyCode) {
	     case(KeyEvent.KEYCODE_BACK):
	    	 	Intent intent = new Intent(this, ChooserPageScreen.class);
	          	intent.putExtra(NameFromList, 0);
				startActivity(intent);
				overridePendingTransition(0, 0);
				finish();
				return true;    
	     }
	     return false;
	}
	
	public void logout(View v) {
		LoginHandler.getInstance().logout();
		Intent intent  = new Intent(SRL3ApplicationClass.getContext(), SplashScreen.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
		finish();
	}

	@Override
	public void syncDone() {
		// TODO Auto-generated method stub
		updateSyncDate();
	}
}
